
Explanation of directories in this repo
---------------------------------------

_EPTO2021_model0/

	* Most recent model *
	Implementation & technical reports of the PTO in E.coli model, Ben Shirt-Ediss, February 2021


_master2018

	Masters student Zac Rubin wrote this code in 2018

	In principle, its an implementation of circadian clock model in "Robust Circadian Oscillations in Growing Cyanobacteria Require Transcriptional Feedback", Teng et al, Science 2013

	Many models here!

	Of most interest will be:
		Teng2013_Python_OOP/
		Teng2013_COPASI/


_rust2007

	Implementation of model in "Ordered Phosphorylation Governs Oscillation of a Three-Protein Circadian Clock", Rust et al, Science 2007
	one of the first models of the PPC mechanism, and still widely used

		* Run the partial model using $ python3 linear_model.py

		* Run the full model using $ python3 full_model.py

_vanZon2007

	COPASI implementation of the model that underlies the Zwicker 2010 PTO model.
	Implemented by Ben Shirt-Ediss in Feb 2021. I could not get it to oscillate.

	Other more basic differential affinity oscillator models here also.

_zwicker2010

	Successful COPASI implementation of the Zwicker 2010 circadian PTO oscillator and tech report.


Giorgio - Correspondence/

	Email correspondence I had with Giorgio, asking him questions to get model parameters


two_component_system_models/
	
	* Core mathematical model presented by Gao 2017 about the shut-off kinetics of the PhoR/PhoB two component system
	* I have extracted kinetic in-vivo parameters and concentrations, which can be used as estimates for our two component system models


CIRCADIAN_annual_review_talk_2019.pdf

	Portabolomics 2019 talk on circadian clock model. 
	This is about the whole model with output pathway, I developed with master student Zac in 2018.
	It is not about the new PTO in E.coli (EPTO) 2021 model.



# # ODE model of the full circadian rhythm model, Rust et al. 2007 Science paper.
# ## Ben Shirt-Ediss & Zac Rubin

import pandas as pd

from DeterministicODEModelBase import DeterministicODEModelBase

class Bunch(object):
    """Helper class to access dictionary items as object properties for simplified code!"""
    def __init__(self, adict):
        self.__dict__.update(adict)

class Goulian2003Model(DeterministicODEModelBase):
    required_params = ["k1", "k_1", "k2", "k_2", "kt", "kp", "kk", "k_k"]

    def _model_ode_set(self, time, state, ode_params):
        pr = Bunch(ode_params)
        # unpack state variables
        HKP_RR, HK_RRP, HKP, HK, RR, RRP = state

        # Differential equations
        ode_HKP_RR = (pr.k1 * HKP * RR) - ((pr.k_1 + pr.kt) * HKP_RR)

        ode_HK_RRP = - ((pr.kp + pr.k_2) * HK_RRP) + (pr.k_2 * HK * RRP)

        ode_HKP = (pr.kk * HK) - (pr.k_k * HKP) + (pr.k_1 * HKP_RR) - (pr.k1 * HKP * RR)

        ode_HK = (pr.k_k * HKP) - (pr.k_k * HK) + ((pr.kp + pr.k_2) * HK_RRP) + (pr.kt * HKP_RR) - (pr.k_2 * HK * RRP)

        ode_RR = (pr.k_1 * HKP_RR) - (pr.k1 * HKP * RR) + (pr.kp * HK_RRP)

        ode_RRP = (pr.kt * HKP_RR) + (pr.k_2 * HK_RRP) - (pr.k2 * HK * RRP)

        return [ode_HKP_RR, ode_HK_RRP, ode_HKP, ode_HK, ode_RR, ode_RRP]


# # ODE model of the full circadian rhythm model, Rust et al. 2007 Science paper.
# ## Ben Shirt-Ediss & Zac Rubin

import pandas as pd

from DeterministicODEModelBase import DeterministicODEModelBase


class Rust2007ModelNormal(DeterministicODEModelBase):
    required_params = ['C_tot', 'A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0',
                       'k_DS_0', 'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A',
                       'k_SU_A']

    def _model_ode_set(self, time, state, ode_params):
        # unpack state variables
        T, D, S = state

        pr = self.ode_params

        # calc U conc
        U = pr['C_tot'] - S - D - T

        # calc KaiA conc
        A = max([0, pr['A_tot'] - (2 * S)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [S, A, pr['K_half']]

        # 3 derivatives
        dT__dt = (self._k(pr['k_UT_0'], pr['k_UT_A'], p) * U) + (self._k(pr['k_DT_0'], pr['k_DT_A'], p) * D) \
                 - (self._k(pr['k_TU_0'], pr['k_TU_A'], p) * T) - (self._k(pr['k_TD_0'], pr['k_TD_A'], p) * T)

        dD__dt = (self._k(pr['k_TD_0'], pr['k_TD_A'], p) * T) + (self._k(pr['k_SD_0'], pr['k_SD_A'], p) * S) \
                 - (self._k(pr['k_DT_0'], pr['k_DT_A'], p) * D) - (self._k(pr['k_DS_0'], pr['k_DS_A'], p) * D)

        dS__dt = (self._k(pr['k_DS_0'], pr['k_DS_A'], p) * D) + (self._k(pr['k_US_0'], pr['k_US_A'], p) * U) \
                 - (self._k(pr['k_SD_0'], pr['k_SD_A'], p) * S) - (self._k(pr['k_SU_0'], pr['k_SU_A'], p) * S)

        return [dT__dt, dD__dt, dS__dt]

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [S, A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))


def get_phosphoforms(trajectories, C_tot):
    # the overall conc of PHOSPHORYLATED FORMS of KaiC (e.g. T, D and S)

    T = trajectories['T']
    D = trajectories['D']
    S = trajectories['S']

    percentages = pd.DataFrame(index=trajectories.index)

    totTDS = list(map(lambda n: T[n] + D[n] + S[n], trajectories.index))
    # to get percentages of each phosphoform, rather than absolute concs,
    # T, D, S and totTDS need dividing by the TOTAL amount of KaiC including the U form, C_tot
    percentages['%T'] = [(t / C_tot) * 100 for t in T]
    percentages['%D'] = [(d / C_tot) * 100 for d in D]
    percentages['%S'] = [(s / C_tot) * 100 for s in S]
    percentages['%TDS'] = [(tds / C_tot) * 100 for tds in totTDS]
    return percentages

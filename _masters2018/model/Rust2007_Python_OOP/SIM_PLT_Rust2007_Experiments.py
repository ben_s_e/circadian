from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import json

from MODEL_Rust2007_OOP import Rust2007ModelNormal
from ModelHelpers import dataframe_to_percentages_constant
from TOOL_PY_FourierRhythmDetect import fourier_period


if __name__ == "__main__":
    T0 = 0.68
    D0 = 1.36
    S0 = 0.34
    label = ['T', 'D', 'S']

    with open("PARAMS_Rust2007_Default.json") as f:
        ode_params_original = json.load(f)

    # ### Simulation #1: LONG SIMULATION!
    p = deepcopy(ode_params_original)

    hrs = 24 * 90

    t_start, t_end, t_steps = 0, hrs, hrs * 10000 / hrs
    state0 = [T0, D0, S0]
    model = Rust2007ModelNormal(p, state0, t_start, t_end, t_steps)
    trajectories = model.run(['T', 'D', 'S'])
    perc = dataframe_to_percentages_constant(trajectories, p['C_tot'], label, sumtargets=label)
    time = perc.index

    # ## PLOTTING
    fig, (ax1, ax2) = plt.subplots(2, 1)
    fig.tight_layout()

    # Time course
    ax1.plot(time, perc['TDS'], 'b', linewidth=2, label="Rust PPC (in vitro)")
    ax1.set_xlabel("Time (hr)")
    ax1.set_ylabel("% phosphorylated KaiC")
    ax1.set_xlim([t_start, t_end])
    ax1.legend(loc="upper right", title="Model")

    sf = 1 / time[1]

    PSD2, P2 = fourier_period(perc['TDS'], time, sf)

    ax2.set_title("FT of PPC trajectory")
    ax2.plot(P2, PSD2, 'g')

    m = 50
    ax2.set_xlabel("Period (hr)")
    ax2.set_ylabel("Normalised PSD")
    ax2.set_xlim([0, m])
    ax2.xaxis.set_major_locator(plt.MaxNLocator(m)) # change axis ticks to every 24hr 

    plt.show()

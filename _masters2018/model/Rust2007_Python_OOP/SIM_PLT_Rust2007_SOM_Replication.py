from copy import deepcopy
import matplotlib.pyplot as plt
import json

from MODEL_Rust2007_OOP import Rust2007ModelSOM
from ModelHelpers import dataframe_to_percentages_constant


if __name__ == "__main__":
    # ------------- Initial Concentrations -----------

    # Unit: uM

    # But unit does not matter, since all reactions are unimolecular with no
    # concentration unit -- so conc can be in any unit.
    # All we want is the percentage concs
    T0 = 0.68
    D0 = 1.36
    S0 = 0.34
    label = ['T', 'D', 'S']

    with open("PARAMS_Rust2007_SOM.json") as f:
        ode_params_original = json.load(f)

    # ### Simulation #1: Show that model works as described in Science paper, Fig 4A
    p = deepcopy(ode_params_original)

    t_start, t_end, t_steps = 0, 90, 10000
    state0 = [T0, D0, S0]
    model = Rust2007ModelSOM(p, state0, t_start, t_end, t_steps)
    trajectories = model.run(['T', 'D', 'S'])
    percentages = dataframe_to_percentages_constant(trajectories, p['C_tot'], label, sumtargets=label)
    time = percentages.index

    plt.figure(1)

    # Plot percentages
    plt.plot(time, percentages['TDS'], 'k--', linewidth=3, label="Total")
    plt.plot(time, percentages['T'], 'g', linewidth=3, label="T")
    plt.plot(time, percentages['D'], 'b', linewidth=3, label="D")
    plt.plot(time, percentages['S'], 'r', linewidth=3, label="S")

    # Prep plot
    plt.xlabel("Time, hours")
    plt.ylabel("% [phosphoforms of KaiC]")
    plt.xlim([t_start, t_end])
    plt.title("Fig 4A Rust 2007: Circadian Oscillator")
    plt.legend(loc="upper right", title="Phosphoform")
    plt.show()

    # ### Simulation #2: Show that a five-fold excess injection of KaiA at about 33 hours leads to the behaviour
    #     reported in Figure 5B of Rust 2007.
    p = deepcopy(ode_params_original)

    # PHASE 1
    t_start, t_end, t_steps = 0, 30, 10000
    state0 = [T0, D0, S0]
    model = Rust2007ModelSOM(p, state0, t_start, t_end, t_steps)
    trajectories0 = model.run(['T', 'D', 'S'])
    # PHASE 2
    t_start, t_end, t_steps = 30, 40, 10000
    state1 = [trajectories0['T'].iloc[-1], trajectories0['D'].iloc[-1], trajectories0['S'].iloc[-1]]
    p['A_tot'] = p['A_tot'] * 5
    model = Rust2007ModelSOM(p, state1, t_start, t_end, t_steps)
    trajectories1 = model.run(['T', 'D', 'S'])

    # # PLOT
    plt.figure(2)

    pp0 = dataframe_to_percentages_constant(trajectories0, p['C_tot'], label, sumtargets=label)
    plt.plot(trajectories0.index, pp0['TDS'], 'k--', linewidth=3, label="Total phosphorylated KaiC")

    pp1 = dataframe_to_percentages_constant(trajectories1, p['C_tot'], label, sumtargets=label)
    plt.plot(trajectories1.index, pp1['TDS'], 'r--', linewidth=3, label="Total phosphorylated KaiC")

    # Prepare plot
    plt.xlim([0, 40])

    plt.xlabel("Time, hours")
    plt.ylabel("% [total phosphoforms of KaiC]")

    n = pp1['TDS'].iloc[0]
    plt.annotate('5x KaiA added',
                 xy=(30, n),
                 xycoords='data',
                 xytext=(32, n + 5),
                 arrowprops=dict(arrowstyle="->", color="r"), color="r");
    plt.show()

    # ### Simulation #3: Show that model this model has a problem: its sensitive to concentration scaling
    #     (Fig S8B of Rust 2007 supplementary material)
    colours = ['k', 'b', 'g', 'r']
    conc_mults = [0.5, 1.0, 2.0, 4.0]
    for colour, multiplier in zip(colours, conc_mults):
        p = deepcopy(ode_params_original)
        p['C_tot'], p['A_tot'] = p['C_tot'] * multiplier, p['A_tot'] * multiplier
        state0 = [T0 * multiplier, D0 * multiplier, S0 * multiplier]

        t_start, t_end, t_steps = 0, 90, 10000
        model = Rust2007ModelSOM(p, state0, t_start, t_end, t_steps)
        trajectories = model.run(['T', 'D', 'S'])
        pp = dataframe_to_percentages_constant(trajectories, p['C_tot'], label, sumtargets=label)
        plt.plot(trajectories.index, pp['TDS'], colour, linewidth=3, label=str(multiplier))

    plt.xlabel("Time, hours")
    plt.ylabel("% [total phosphoforms of KaiC]")
    plt.xlim([t_start, t_end])
    plt.title("Fig S8B Rust 2007: ...but oscillator is sensitive to initial absolute protein concentrations")
    plt.legend(loc="upper right", title="Initial conc. multiplier");
    plt.show()

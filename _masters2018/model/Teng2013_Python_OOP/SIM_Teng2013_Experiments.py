from copy import deepcopy
import matplotlib.pyplot as plt
import math
import numpy as np
import json

from MODELS_DET_Teng2013_OOP import Teng2013PTROnlyModel, Teng2013WTModel
from ModelHelpers import dataframe_to_percentages_susbset_total
from TOOL_PY_FourierRhythmDetect import fourier_period


if __name__ == "__main__":
    with open("PARAMS_Teng2013WT_Default.json") as f:
        params_original_wt = json.load(f)

    with open("PARAMS_Teng2013PTR_Default.json") as f:
        params_original_ptr = json.load(f)


    # Get initial conditions from parameters (same for both wt and ptr-only
    # models)
    ini = ['T0', 'D0', 'S0', 'U0', 'M0']
    state0 = []
    for species in ini:
        state0.append(params_original_wt[species])

    # ### Simulation #1 Long simulation length

    pwt= deepcopy(params_original_wt)
    pptr = deepcopy(params_original_ptr)


    hrs = 24 * 90

    t_start, t_end, t_steps = 0, hrs, hrs* 10000 / hrs

    # Simulate WT model
    model_wt = Teng2013WTModel(pwt, state0, t_start, t_end, t_steps)
    traj_wt = model_wt.run(['T', 'D', 'S', 'U', 'M'])
    per_wt = dataframe_to_percentages_susbset_total(traj_wt, ['T', 'D', 'S', 'U'], ['T', 'D', 'S'])
    time = per_wt.index

    # Simulate PTR model
    model_ptr = Teng2013PTROnlyModel(pptr, state0, t_start, t_end, t_steps)
    traj_ptr = model_ptr.run(['T', 'D', 'S', 'U', 'M'])
    per_ptr = dataframe_to_percentages_susbset_total(traj_ptr, ['T', 'D', 'S', 'U'], ['T', 'D', 'S'])

    # ## PLOTTING
    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    fig.tight_layout()

    # Time course
    ax1.plot(time, per_wt['TDS'], 'g', linewidth=3, label="WT (in vivo)")
    ax1.plot(time, per_ptr['TDS'], 'b', linewidth=3, label="PTR-only (in vivo)")
    ax1.set_xlabel("Time (hr)")
    ax1.set_ylabel("% phosphorylated KaiC")
    ax1.set_xlim([t_start, t_end])
    ax1.set_title("Fig 4A Teng 2013: Deterministic simulation of WT vs PTR-only models")
    ax1.legend(loc="upper right", title="Model")

    sf = 1 / time[1]

    PSD2, P2 = fourier_period(per_wt['TDS'], time, sf)
    ax2.set_title("FT of WT trajectory")
    ax2.plot(P2, PSD2, 'g')

    PSD2, P2 = fourier_period(per_ptr['TDS'], time, sf)
    ax3.set_title("FT of PTR-only trajectory")
    ax3.plot(P2, PSD2, 'b')

    for ax in [ax2, ax3]:
        m = 50
        ax.set_xlabel("Period (hr)")
        ax.set_ylabel("Normalised PSD")
        ax.set_xlim([0, m])
        ax.xaxis.set_major_locator(plt.MaxNLocator(m)) # change axis ticks to every 24hr 

    plt.show()

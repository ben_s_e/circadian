from DeterministicODEModelBase import DeterministicODEModelBase

import pandas as pd


class Bunch(object):
    """Helper class to access dictionary items as object properties for simplified code!"""

    def __init__(self, adict):
        self.__dict__.update(adict)


class Teng2013PTROnlyModel(DeterministicODEModelBase):
    """The PTR-only (in vivo) version of the model"""
    required_params = ['A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0', 'k_DS_0',
                       'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A', 'k_SU_A',
                       'V', 'Vd', 'Vm', 'Vsptr', 'K', 'Ks', 'Km']

    def _model_ode_set(self, time, state, ode_params):
        # unpack state variables
        T, D, S, U, M = state

        pr = Bunch(ode_params)

        # unpack parameters

        # calc KaiA conc
        A = max([0, pr.A_tot - (2 * S)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [S, A, pr.K_half]

        # 5 derivatives
        dT__dt = (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) \
                 - (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) - (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) \
                 - self._deg(T, pr.V, pr.K) - (pr.Vd * T)

        dD__dt = (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) \
                 - (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) \
                 - self._deg(D, pr.V, pr.K) - (pr.Vd * D)

        dS__dt = (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) + (self._k(pr.k_US_0, pr.k_US_A, p) * U) \
                 - (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                 - self._deg(S, pr.V, pr.K) - (pr.Vd * S)

        dU__dt = (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) + (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                 - (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) - (self._k(pr.k_US_0, pr.k_US_A, p) * U) \
                 - self._deg(U, pr.V, pr.K) - (pr.Vd * U) + (pr.Ks * M)

        dM__dt = pr.Vsptr - (pr.Vm * (M / (pr.Km + M)))

        return [dT__dt, dD__dt, dS__dt, dU__dt, dM__dt]

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [S, A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))

    @staticmethod
    def _deg(phosphoform, V, K):
        """Calculate rate of degradation"""
        return V * (phosphoform / (K + phosphoform))


class Teng2013WTModel(DeterministicODEModelBase):
    """The wild-type version of the model featuring the TTR and PTR"""
    required_params = ['A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0', 'k_DS_0',
                       'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A', 'k_SU_A', 'V',
                       'Vs', 'Vd', 'Vm', 'Vsptr', 'K', 'Ks', 'Km', 'cU', 'cT', 'cD', 'cS', 'KI', 'n']

    def _model_ode_set(self, time, state, ode_params):
        T, D, S, U, M = state
        pr = Bunch(self.ode_params)

        # calc KaiA conc
        A = max([0, pr.A_tot - (2 * S)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [S, A, pr.K_half]

        # Caculate Cr term
        Cr = (pr.cU * U) + (pr.cT * T) + (pr.cD * D) + (pr.cS * S)  # abstract coupling of PTR and TTR

        # 4 derivatives
        dT__dt = (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (
                self._k(pr.k_TU_0, pr.k_TU_A, p) * T) - (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) - self._deg(T, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * T)

        dD__dt = (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (
                self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) - self._deg(D, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * D)

        dS__dt = (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) + (self._k(pr.k_US_0, pr.k_US_A, p) * U) - (
                self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) - self._deg(S, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * S)

        dU__dt = (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) + (
                self._k(pr.k_SU_0, pr.k_SU_A, p) * S - self._k(pr.k_UT_0, pr.k_UT_A, p) * U) - (
                         self._k(pr.k_US_0, pr.k_US_A, p) * U) - self._deg(U, pr.V, pr.K) - (pr.Vd * U) + (
                         pr.Ks * M)

        dM__dt = (pr.Vs * ((pr.KI ** pr.n) / ((pr.KI ** pr.n) + (Cr ** pr.n)))) - (pr.Vm * (M / (pr.Km + M)))

        return [dT__dt, dD__dt, dS__dt, dU__dt, dM__dt]

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [S, A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))

    @staticmethod
    def _deg(phosphoform, V, K):
        """Calculate rate of degradation"""
        return V * (phosphoform / (K + phosphoform))


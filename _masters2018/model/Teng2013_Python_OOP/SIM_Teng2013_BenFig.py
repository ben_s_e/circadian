from copy import deepcopy
import matplotlib.pyplot as plt
import json

from MODELS_DET_Teng2013_OOP import Teng2013PTROnlyModel, Teng2013WTModel
from ModelHelpers import dataframe_to_percentages_susbset_total


if __name__ == "__main__":
    with open("PARAMS_Teng2013WT_Default.json") as f:
        params_original_wt = json.load(f)

    with open("PARAMS_Teng2013PTR_Default.json") as f:
        params_original_ppc = json.load(f)


    # Get initial conditions from parameters (same for both wt and ppc-only
    # models)
    ini = ['T0', 'D0', 'S0', 'U0', 'M0']
    state0 = []
    for species in ini:
        state0.append(params_original_wt[species])

    # ### Simulation #1: Reproduce Figure 4A
    # WARNING! The paper uses unknown initial conditions for the graphs in Figure 4.
    # As such, we cannot replicate their figures exactly.

    pwt= deepcopy(params_original_wt)
    pppc = deepcopy(params_original_ppc)

    t_start, t_end, t_steps = 0, 120, 1200000 / 120

    # Simulate WT model
    model_wt = Teng2013WTModel(pwt, state0, t_start, t_end, t_steps)
    traj_wt = model_wt.run(['T', 'D', 'S', 'U', 'M'])
    per_wt = dataframe_to_percentages_susbset_total(traj_wt, ['T', 'D', 'S', 'U'], ['T', 'D', 'S'])
    time = per_wt.index

    """
    # Simulate PTR model
    model_ppc = Teng2013PTROnlyModel(pwt, state0, t_start, t_end, t_steps)
    traj_ppc = model_ppc.run(['T', 'D', 'S', 'U', 'M'])
    per_ppc = dataframe_to_percentages_susbset_total(traj_ppc, ['T', 'D', 'S', 'U'], ['T', 'D', 'S'])
    """

    plt.figure(1)

    plt.plot(time, traj_wt['T'], label="T")
    plt.plot(time, traj_wt['D'], label="D")
    plt.plot(time, traj_wt['S'], label="S")
    plt.plot(time, traj_wt['U'], label="U")
    #plt.plot(time, per_ppc['TDS'], 'b', linewidth=3, label="PTR-only")


    # Prep plot
    plt.xlabel("Time (hr)")
    plt.ylabel("Concentrations of KaiC phosphoforms (uM)")
    plt.xlim([t_start, t_end])
    plt.legend(loc="upper right", title="KaiC phosphoform")
    plt.show()

    plt.figure(2)

    plt.plot(time, traj_wt['M'])
    #plt.plot(time, per_ppc['TDS'], 'b', linewidth=3, label="PTR-only")


    # Prep plot
    plt.xlabel("Time (hr)")
    plt.ylabel("Concentrationof kaiBC mRNA (M)")
    plt.xlim([t_start, t_end])
    plt.show()


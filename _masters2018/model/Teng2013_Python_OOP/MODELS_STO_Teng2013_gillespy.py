import json
import gillespy
import matplotlib.pyplot as plt


class Bunch(object):
    """Helper class to access dictionary items as object properties for simplified code!"""

    def __init__(self, adict):
        self.__dict__.update(adict)


class Teng2013PTROnlyModelStochastic(gillespy.Model):
    """The PTR-only (in vivo) version of the model"""
    required_params = ['A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0', 'k_DS_0',
                       'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A', 'k_SU_A',
                       'V', 'Vd', 'Vm', 'Vsptr', 'K', 'Ks', 'Km']

    def __init__(self, parameterset, volume):
        # ### PARAMETER IMPORT
        for p in self.required_params:
            if p not in parameterset.keys():
                raise KeyError("PARAMETER ERROR! Missing paramter {!r}.".format(p))


        gillespy.Model.__init__(self, name="Teng2013 PTR-only", volume=volume)

        params = dict()
        for name, value in parameterset.items():
            params[name] = gillespy.Parameter(name=name, expression=value)

        self.add_parameter(params.values())
        p = Bunch(params) # easy access
        pv = dict()
        for k, v in params.items():
            pv[k] = float(v.expression)
        pv = Bunch(pv)

        # ### SPECIES
        species = dict()

        #species['U'] = gillespy.Species(name='U', initial_value=int(pv.U0* volume))
        #species['T'] = gillespy.Species(name='T', initial_value=int(pv.T0 * volume))
        #species['D'] = gillespy.Species(name='D', initial_value=int(pv.D0 * volume))
        #species['S'] = gillespy.Species(name='S', initial_value=int(pv.S0 * volume))
        species['M'] = gillespy.Species(name='M', initial_value=int(pv.M0 * volume))

        self.add_species(species.values())
        s = Bunch(species)

        # ### REACTIONS
        reactions = dict()
        # ## MRNA
        reactions['M_SYN'] = gillespy.Reaction(name="M_SYN", reactants={}, products={s.M:1}, rate=p.Vsptr)
        reactions['M_DEG'] = gillespy.Reaction(name="M_DEG", reactants={s.M:1}, products={}, propensity_function='Vm * (M / (Km + M))')

        self.add_reaction(reactions.values())

        """
        # calc KaiA conc
        A = max([0, pr.A_tot - (2 * S)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [S, A, pr.K_half]

        # 5 derivatives
        dT__dt = (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) \
                 - (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) - (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) \
                 - self._deg(T, pr.V, pr.K) - (pr.Vd * T)

        dD__dt = (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) \
                 - (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) \
                 - self._deg(D, pr.V, pr.K) - (pr.Vd * D)

        dS__dt = (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) + (self._k(pr.k_US_0, pr.k_US_A, p) * U) \
                 - (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                 - self._deg(S, pr.V, pr.K) - (pr.Vd * S)

        dU__dt = (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) + (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                 - (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) - (self._k(pr.k_US_0, pr.k_US_A, p) * U) \
                 - self._deg(U, pr.V, pr.K) - (pr.Vd * U) + (pr.Ks * M)

        dM__dt = pr.Vsptr - (pr.Vm * (M / (pr.Km + M)))

        """

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [S, A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))

    @staticmethod
    def _deg(phosphoform, V, K):
        """Calculate rate of degradation"""
        return V * (phosphoform / (K + phosphoform))


class Teng2013WTModel(gillespy.Model):
    """The wild-type version of the model featuring the TTR and PTR"""
    required_params = ['A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0', 'k_DS_0',
                       'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A', 'k_SU_A', 'V',
                       'Vs', 'Vd', 'Vm', 'Vsptr', 'K', 'Ks', 'Km', 'cU', 'cT', 'cD', 'cS', 'KI', 'n']

    def _model_ode_set(self, time, state, ode_params):
        T, D, S, U, M = state
        pr = Bunch(self.ode_params)

        # calc KaiA conc
        A = max([0, pr.A_tot - (2 * S)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [S, A, pr.K_half]

        # Caculate Cr term
        Cr = (pr.cU * U) + (pr.cT * T) + (pr.cD * D) + (pr.cS * S)  # abstract coupling of PTR and TTR

        # 4 derivatives
        dT__dt = (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (
                self._k(pr.k_TU_0, pr.k_TU_A, p) * T) - (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) - self._deg(T, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * T)

        dD__dt = (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (
                self._k(pr.k_DT_0, pr.k_DT_A, p) * D) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) - self._deg(D, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * D)

        dS__dt = (self._k(pr.k_DS_0, pr.k_DS_A, p) * D) + (self._k(pr.k_US_0, pr.k_US_A, p) * U) - (
                self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) - self._deg(S, pr.V,
                                                                                                           pr.K) - (
                         pr.Vd * S)

        dU__dt = (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) + (
                self._k(pr.k_SU_0, pr.k_SU_A, p) * S - self._k(pr.k_UT_0, pr.k_UT_A, p) * U) - (
                         self._k(pr.k_US_0, pr.k_US_A, p) * U) - self._deg(U, pr.V, pr.K) - (pr.Vd * U) + (
                         pr.Ks * M)

        dM__dt = (pr.Vs * ((pr.KI ** pr.n) / ((pr.KI ** pr.n) + (Cr ** pr.n)))) - (pr.Vm * (M / (pr.Km + M)))

        return [dT__dt, dD__dt, dS__dt, dU__dt, dM__dt]

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [S, A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))

    @staticmethod
    def _deg(phosphoform, V, K):
        """Calculate rate of degradation"""
        return V * (phosphoform / (K + phosphoform))

if __name__ == "__main__":
    with open("PARAMS_Teng2013PTR_DEFAULT.json") as f:
        pa = json.load(f)
    v = 3*10**-15
    m = Teng2013PTROnlyModelStochastic(pa, v)


    traj = m.run(show_labels=True)
    plt.figure(1)
    traj = traj[0]
    plt.plot(traj['time'], traj['M'])
    plt.show()

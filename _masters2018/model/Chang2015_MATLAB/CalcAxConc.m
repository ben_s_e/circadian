function AxConc = CalcAxConc(mode, A_tot, m, n, Sx_Bfs, STx_Bfs, Bfs)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    mode = round(mode);
    %
    switch mode
        case 0
            AxConc = 0;
        case 1
            AxConc = A_tot-(m*Sx_Bfs)-(n*STx_Bfs)-(2*Bfs);
        otherwise
            error("Invalid Mode!")
    end
end


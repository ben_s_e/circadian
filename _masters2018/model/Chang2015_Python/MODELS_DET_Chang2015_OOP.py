from DeterministicODEModelBase import DeterministicODEModelBase

import pandas as pd


class Bunch(object):
    """Helper class to access dictionary items as object properties for simplified code!"""

    def __init__(self, adict):
        self.__dict__.update(adict)


class Chang2015Model(DeterministicODEModelBase):
    required_params = ['A_tot', 'K_half', 'k_UT_0', 'k_TD_0', 'k_SD_0', 'k_US_0', 'k_TU_0', 'k_DT_0', 'k_DS_0',
                       'k_SU_0', 'k_UT_A', 'k_TD_A', 'k_SD_A', 'k_US_A', 'k_TU_A', 'k_DT_A', 'k_DS_A', 'k_SU_A',
                       'V', 'Vd', 'Vm', 'Vsptr', 'K', 'Ks', 'Km', 'kCI', 'm', 'n', 'kB+', 'kB-', 'kon', 'S*0',
                      'ST*0', 'S.B0', 'ST.B0', 'B0', 'Bfs0']

    def _model_ode_set(self, time, state, ode_params):
        # unpack state variables
        U, S, T, ST, Sx, STx, SBfs, STBfs, B, Bfs = state

        pr = Bunch(ode_params)
        pp = ode_params # can't access certain paramters via the bunch due to special characters

        # calc active KaiA conc
        A = max([0, pr.A_tot - (pr.m * SBfs) - (pr.n * STBfs) - (2 * Bfs)])

        # package up constant variables to send into each rate function, to make derivatives below easier to read
        p = [A, pr.K_half]

        # 5 derivatives
        dU = (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) + (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                + (self._k(pr.k_SU_0, pr.k_SU_A, p) * SBfs) + (self._k(pr.k_SU_0, pr.k_SU_A, p) * Sx) \
                - (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) - (self._k(pr.k_US_0, pr.k_US_A, p) * U)

        dS = (self._k(pr.k_US_0, pr.k_US_A, p) * U) + (self._k(pr.k_DS_0, pr.k_DS_A, p) * ST) \
                - (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) - (self._k(pr.k_SU_0, pr.k_SU_A, p) * S) \
                - (pr.kCI * S)

        dT = (self._k(pr.k_UT_0, pr.k_UT_A, p) * U) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * ST) \
                + (self._k(pr.k_DT_0, pr.k_DT_A, p) * STBfs) + (self._k(pr.k_DT_0, pr.k_DT_A, p) * STx) \
                - (self._k(pr.k_TU_0, pr.k_TU_A, p) * T) - (self._k(pr.k_TD_0, pr.k_TD_A, p) * T)

        dST = (self._k(pr.k_TD_0, pr.k_TD_A, p) * T) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * S) \
                - (self._k(pr.k_DT_0, pr.k_DT_A, p) * ST) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * ST) \
                - (pr.kCI * ST)

        dSx = (self._k(pr.k_DS_0, pr.k_DS_A, p) * STx) + (pr.kCI * S) - (self._k(pr.k_SD_0, pr.k_SD_A, p) * Sx) \
                - (self._k(pr.k_SU_0, pr.k_SU_A, p) * Sx) - (pr.kon * Sx * Bfs)

        dSTx = (self._k(pr.k_SD_0, pr.k_SD_A, p) * Sx) + (pr.kCI * ST) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * STx) \
                - (self._k(pr.k_DT_0, pr.k_DT_A, p) * STx) - (pr.kon * STx * Bfs)

        dSBfs = (pr.kon * Sx * Bfs) + (self._k(pr.k_DS_0, pr.k_DS_A, p) * STBfs) - (self._k(pr.k_SD_0, pr.k_SD_A, p) * SBfs) \
                - (self._k(pr.k_SU_0, pr.k_SU_A, p) * SBfs)

        dSTBfs = (pr.kon * STx * Bfs) + (self._k(pr.k_SD_0, pr.k_SD_A, p) * SBfs) - (self._k(pr.k_DS_0, pr.k_DS_A, p) * STBfs) \
                - (self._k(pr.k_DT_0, pr.k_DT_A, p) * STBfs)

        dB = (pp['kB-']*Bfs) + ((pr.n - pr.m) * self._k(pr.k_DS_0, pr.k_DS_A, p) * STBfs) \
                + (pr.n * self._k(pr.k_DT_0, pr.k_DT_A, p) * STBfs) + ((pr.m - pr.n) * self._k(pr.k_SD_0, pr.k_SD_A, p) * SBfs) - (pp['kB+']*B)

        dBfs = (pp['kB+'] * B) - (pp['kB-'] * Bfs) - (pr.m * pr.kon * Sx * Bfs) - (pr.n * pr.kon * STx * Bfs)

        return [dU, dS, dT, dST, dSx, dSTx, dSBfs, dSTBfs, dB, dBfs]

    @staticmethod
    def _k(k_basal, k_maximal, p):
        [A, K_half] = p
        return k_basal + ((k_maximal * A) / (K_half + A))


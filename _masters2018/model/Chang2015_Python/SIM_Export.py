from copy import deepcopy
import matplotlib.pyplot as plt
import json

from MODELS_DET_Chang2015_OOP import Chang2015Model
from ModelHelpers import dataframe_to_percentages_susbset_total


def main():
    with open("PARAMS_Chang2015_DEFAULT.json") as f:
        params_original = json.load(f)


    # Get initial conditions from parameters (same for both wt and ppc-only
    # models)
    ini = ["U0", "S0", "T0", "ST0", "S*0", "ST*0", "S.B0", "ST.B0", "B0", "Bfs0"]
    state0 = []
    for species in ini:
        state0.append(params_original[species])

    t_start, t_end, t_steps = 0, 120, 1200000 / 120

    model = Chang2015Model(params_original, state0, t_start, t_end, t_steps)
    names = list(map(lambda x: x.rstrip("0"), ini))
    traj = model.run(names)
    traj.to_csv("chang-data-python.csv")

    time = str(list(traj.index))
    with open("chang-data-times.txt", "w") as f:
        f.write(time)


if __name__ == "__main__":
    main()

Installation
============

MacOS or Linux
--------------

1. Install the most recent build of COPASI_. 

.. _COPASI: http://copasi.org/

2. Change to a directory where you want to place the simulator code. For simplicity, we can choose the Desktop: ::

	cd Desktop

3. Enter the terminal.

4. Download the software repository: ::

	git clone https://bitbucket.org/ben_s_e/circadian.git

5. Change to the ``circadian`` directory: ::

	cd circadian

6. Install a Python3 virtual environment, and configure it: ::

	python3 -m venv venv
	source venv/bin/activate
	pip install --upgrade pip
	pip install matplotlib
	pip install python-libsbml
	pip install scipy

7. Change to the EPTO model directory. ::

	cd _EPTO2021_model0

8. Open file ``sim.py`` and change the ``COPASI_PATH`` variable at the top of the script to the path where the command line version of COPASI -- called CopasiSE -- is located. For example, on Mac, if COPASI is in the ``/Applications`` directory, then set: ::

	COPASI_PATH = "/Applications/COPASI/CopasiSE"

9. Run the model simulations and analysis following instructions on the :ref:`Usage` page.



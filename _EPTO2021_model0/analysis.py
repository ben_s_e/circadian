"""Analysis of simulation trajectories

Ben Shirt-Ediss, Feb 2021
"""


import heatmap

import csv
import pickle
import scipy.fft
import numpy
import pickle
import matplotlib.pyplot as plt







# --------- Analysis Parameters -----------

HAS_OSCILLATION = []
IS_DAMPED = []

"""
HAS_OSCILLATION = 	list(range(41,46+1)) + \
					list(range(80,87+1)) + \
					list(range(118,128+1)) + \
					list(range(157,169+1)) + \
					list(range(195,209+1)) + \
					list(range(234,248+1)) + \
					list(range(272,288+1)) + \
					list(range(311,324+1)) + \
					list(range(348,360+1)) + \
					list(range(387,396+1))

					
IS_DAMPED =			list(range(44,46+1)) + \
					list(range(84,87+1)) + \
					list(range(123,128+1)) + \
					list(range(163,169+1)) + \
					list(range(206,209+1)) + \
					list(range(243,248+1)) + \
					list(range(284,288+1)) + \
					list(range(311,312+1)) + \
					list(range(324,324+1)) + \
					list(range(348,349+1)) + \
					list(range(387,389+1))
"""

# --------- Analysis Parameters -----------




#
#
# Compute trajectory of p metric versus Time
# 			(p is the percent fraction of KaiC monomers phosphorylated)
#

def read_COPASI_timecourse_report_csv(traj_filename) :

	variables = {}
	values = {}

	with open(traj_filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter='\t') 	# TAB delimited columns
		
		header_line_found = False

		for row in csv_reader:

			# search for beginning of data
			if header_line_found == False :
				
				strrow = "".join(row)
				if strrow.startswith("# Time") :
					
					header_line_found = True
					
					for i, heading in enumerate(row) :
						if heading != "" :	# last heading is empty
							variables[i] = str(heading)		# time is called # Time
							values[i] = []
				continue

			# in the data - add it
			for i, val in enumerate(row) :
				if val != "" :	# last value is empty
					values[i].append(float(val))

	return variables, values





def make_KaiC_p_trajectory(traj_filename, ptraj_filename) :
	"""Converts raw trajectory output by COPASI into percent fraction of existing KaiC monomers phosphorylated over time
		Saves trajectory as pickle file (ready for signal analysis).
	"""

	variables, values = read_COPASI_timecourse_report_csv(traj_filename)

	# find all C1 .. C6 variable columns (phosphorylated forms of KaiC. C0 is not phosphorylated)
	C = {0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: []}
	for idx in variables :
		for i in range(0, 6+1) :
			if "C%d" % (i) in variables[idx] :
				C[i].append(idx)

	# XXX ALSO SMALL C POP IN DIMERS, TRIMER, 4MERS ETC.

	# step through trajectory data, line by line
		
	p_KaiC = {	"Time" 	: [],
				"p" 	: []}

	p_KaiC["Time"] = values[0]

	datapoints = len(p_KaiC["Time"])

	for d in range(0, datapoints) :
		C0 = sum([values[col][d] for col in C[0]])		# conc of all i=0 species
		C1 = sum([values[col][d] for col in C[1]])		# conc of all i=1 species...etc
		C2 = sum([values[col][d] for col in C[2]])
		C3 = sum([values[col][d] for col in C[3]])
		C4 = sum([values[col][d] for col in C[4]])
		C5 = sum([values[col][d] for col in C[5]])
		C6 = sum([values[col][d] for col in C[6]])

		numerator = (1 * C1) + (2 * C2) + (3 * C3) + (4 * C4) + (5 * C5) + (6 * C6)
		denominator = 6 * (C0 + C1 + C2 + C3 + C4 + C5 + C6)

		p = 0
		if denominator > 0 :
			p = numerator / denominator

		p_KaiC["p"].append(p)

	# dump p traj to pickle file
	pickle.dump( p_KaiC, open( ptraj_filename, "wb" ) )












#
#
# Signal Processing (Basic Fourier Transform)
#		
#


def remove_DC_component(timeseries) :

	return timeseries - numpy.mean(timeseries)




def discrete_fourier_transform(timeseries, sample_rate) :
	"""(from https://realpython.com/python-scipy-fft/)

	sample_rate is the number of data points per time unit
	e.g. if the timeseries is in seconds, sample_rate is the number of data points per second (Hz)
		 if the timeseries is in hours, sample_rate is the number of data points per hour
	"""

	# calc discrete Fourier transform, without negative x-axis in results (the "r" version of the transforn)
	# - the transform is the discrete version, as we have discrete time series data of a continuous function
	
	N = len(timeseries) 						# total number of data points

	yf = scipy.fft.rfft(timeseries)				# power (complex numbers)
	xf = scipy.fft.rfftfreq(N, 1 / sample_rate) # frequency (I guess in same unit of time as timeseries is in)

	yf_real = numpy.abs(yf)

	return xf, yf_real




def tallest_peak(xf, yf_real) :
	"""Find at what frequency tallest peak in power spectrum occurs, and its amplitude
	"""

	xf_list = list(xf)
	yf_real_list = list(yf_real)

	# find maximum value in yf_real
	max_power = max(yf_real_list)

	# if maximum power is 0, there is no frequency spectrum!
	if max_power == 0 :
		return 0, 0
	else :
		return xf_list[yf_real_list.index(max_power)], max_power
					# frequency 						power















#
#
# Parameter Scan
#		
#


def make_trajectory_graphs(ppi_start=None, ppi_end=None) :

	plt.figure()

	# --------- just does a batch... label trajectories while you wait for others to finish
	if ppi_start != None and ppi_end != None :
		
		for ppi in range(ppi_start, ppi_end+1) :

			print("- Calculating point %d" % (ppi))

			# open pickle file of p(t) trajectory
			p_KaiC = pickle.load( open( "results/%d.p" % (ppi), "rb" ) )

			# save traj plot
			plt.clf()
			plt.plot(p_KaiC["Time"], p_KaiC["p"], "k")
			plt.xlabel("Time (h)")
			plt.ylabel("p (fraction of KaiC monomers phosphorylated)")
			plt.savefig("results/_%d.pdf" % (ppi))

			ppi += 1

		return
	# ------------------------------------------------------------------------

	# default behaviour...

	# open results info
	info = pickle.load( open( "results/info.p", "rb" ) )
	kaiA_tr = info["kaiA_tr"]
	kaiBC_tr = info["kaiBC_tr"]
	#end_of_spike_time = info["end_of_spike_time"]

	end_of_spike_time = 8

	ppi = 1 		# index of parameter square
	for y in kaiA_tr :
		for x in kaiBC_tr :

			print("- Calculating point %d of %d" % (ppi, len(kaiA_tr) * len(kaiBC_tr)))

			# open pickle file of p(t) trajectory
			p_KaiC = pickle.load( open( "results/%d.p" % (ppi), "rb" ) )

			# save traj plot
			plt.clf()
			plt.plot(p_KaiC["Time"], p_KaiC["p"], "k")
			plt.axvline(x=end_of_spike_time) # draw vertical line at beginning of free running phase (where experimental observations begin)
			plt.xlabel("Time (h)")
			plt.ylabel("p (fraction of KaiC monomers phosphorylated)")
			plt.savefig("results/_%d.pdf" % (ppi))

			ppi += 1





def draw_heatmaps() :

	# open results info
	info = pickle.load( open( "results/info.p", "rb" ) )

	kaiA_tr = info["kaiA_tr"]
	kaiBC_tr = info["kaiBC_tr"]
	sampling_rate = info["data_points_per_hour"]		# points per HOUR, needed by Fourier Transform

	heatmap_period = []		# period of oscillating parameter points (both damped and not)
	heatmap_damped = []		# two colours denoting damped and undamped parameter points
	heatmap_ss = []			# parameter points settling to steady state

	ppi = 1 		# index of parameter square
	for y in kaiA_tr :

		row_period = []		# heatmap rows
		row_damped = []
		row_ss = []

		for x in kaiBC_tr :

			print("- Calculating point %d of %d" % (ppi, len(kaiA_tr) * len(kaiBC_tr)))

			# open pickle file of p(t) trajectory
			p_KaiC = pickle.load( open( "results/%d.p" % (ppi), "rb" ) )

			if ppi in HAS_OSCILLATION :

				row_ss.append(0.01010101010)	# rogue value that signals display white square, no data

				# -- this trajectory oscillates

				# remove DC component (signal average)
				p_KaiC["p"] = remove_DC_component(p_KaiC["p"])
				# compute fourier spectrum, and resolve frequency of tallest peak
				xf, yf_real = discrete_fourier_transform(p_KaiC["p"], sampling_rate)
				freq, power = tallest_peak(xf, yf_real)

				row_period.append(1.0/freq)
					# frequency will not be 0, as this trajectory definitely oscillates

				# for reference: to plot spectrum
				#plt.plot(xf, yf_real, 'k')
				#plt.xlabel("Frequency (oscillations per hour)")
				#plt.ylabel("Power")

				if ppi in IS_DAMPED :

					# -- this trajectory oscillates, but is damped
					row_damped.append(1)

				else :

					row_damped.append(0)

			else :

				row_period.append(25.3321778)	# rogue value that signals display white square, no data
				row_damped.append(0.5) 			# rogue value that signals display white square, no data

				# -- this trajectory settles to a steady state

				# find steady state fraction at end of trajectory (average of last 25 samples)
		
				last25 = p_KaiC["p"][-25:]
				p_ss = sum(last25) / 25.0

				row_ss.append(p_ss)

			ppi += 1

		# add row data to summary heatmaps (insert *at beginning* of list of lists)
		heatmap_period.insert(0,row_period)
		heatmap_damped.insert(0,row_damped)
		heatmap_ss.insert(0,row_ss)

	
	# save summary heatmaps
	heatmap.plot_heatmap("results/_HEATMAP_period.pdf", "KaiC p Oscillation Period (Hours)", 
		heatmap_period,  
		"gamma_A", len(kaiA_tr), ["%1.1f" % (val * 1e6) for val in kaiA_tr[::-1]], 	# gamma_A list reversed because of way heatmap is drawn
		"gamma_BC", len(kaiBC_tr), ["%1.1f" % (val * 1e6) for val in kaiBC_tr],
		colour_map="Paired",
		colourbar_ticks=[25,30,35,40,45,50,55,60,65,70],
		colourbar_shrink=0.5,
		maskval=25.3321778)	

	heatmap.plot_heatmap("results/_HEATMAP_damped.pdf", "Damped or Full Oscillations", 
		heatmap_damped,  
		"gamma_A", len(kaiA_tr), ["%1.1f" % (val * 1e6) for val in kaiA_tr[::-1]], 
		"gamma_BC", len(kaiBC_tr),["%1.1f" % (val * 1e6) for val in kaiBC_tr],
		colourbar_ticks=[0,1],	
		colourbar_shrink=0.5,
		maskval=0.5)

	heatmap.plot_heatmap("results/_HEATMAP_ss.pdf", "KaiC p Final Steady State Values", 
		heatmap_ss,  
		"gamma_A", len(kaiA_tr), ["%1.1f" % (val * 1e6) for val in kaiA_tr[::-1]], 
		"gamma_BC", len(kaiBC_tr),["%1.1f" % (val * 1e6) for val in kaiBC_tr],
		colourbar_ticks=[0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7],
		colourbar_shrink=0.5,
		maskval=0.01010101010)




def get_ppi(gamma_A_uM_h, gamma_BC_uM_h) :
	"""Given gammaA and gammaBC values, returns what the grid square index is
		(used for labelling plots)
	"""

	info = pickle.load( open( "results/info.p", "rb" ) )
	kaiA_tr = info["kaiA_tr"]
	kaiBC_tr = info["kaiBC_tr"]

	ppi = 1 		# index of parameter square
	for y in kaiA_tr :
		for x in kaiBC_tr :

			y1 = float("%1.2f" % (y*1e6))	# gets rid of inexact small float problem
			x1 = float("%1.2f" % (x*1e6))

			if y1 == gamma_A_uM_h and x1 == gamma_BC_uM_h :
				return ppi

			ppi += 1

	return None





if __name__ == "__main__" :

	print("")
	print("---------------------------------------")
	print("EPTO Model ANALYSIS PLOTS")
	print("Post Translational Oscillator in E.coli")
	print("---------------------------------------")
	print("Ben Shirt-Ediss, 2021")
	print("---------------------------------------")
	print("")

	"""
	# get parameter squares used to demo the model in different scenarios
	# in the technical report.
	# scenario 1
	print( get_ppi(0.2, 0.2) )		# = 38

	# scenario 2
	print( get_ppi(0.2, 2.2) )		# = 48

	# scenario 3
	print( get_ppi(0.2, 3.0) )		# = 52

	# scenario 4
	print( get_ppi(0.8, 3.0) )		# = 160

	quit()
	"""
	

	if len(HAS_OSCILLATION) == 0 :
		print("Creating trajectories...")
		make_trajectory_graphs()

	else :
		print("Drawing heatmaps...")
		draw_heatmaps()

	print("")
	print("Done.")
	print("")



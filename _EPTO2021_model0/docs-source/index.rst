A Model for the Post-Translational Oscillator in *E.coli*
=========================================================

A first implementation for a model for the plasmid-controlled PTO oscillator in *E.coli*.

The PTO itself is re-implemented from `Zwicker et al. 2010 <https://doi.org/10.1073/pnas.1007613107>`_, with gene expression reactions added following rate constants given in `Teng et al. 2013 <https://science.sciencemag.org/content/340/6133/737.abstract>`_. The simulation model is generated in SBML by the Python3 binding of ``libsbml`` and then simulated in COPASI_.

 .. _COPASI: http://copasi.org/

The model is described in technical report :download:`TR-EPTO-0<TR-EPTO-0.pdf>`, with the re-implementation of the PTO described in the separate technical report :download:`TR-ZWICKER<TR-ZWICKER.pdf>`. 

This guide describes how to install and run the model. Simulations are run on a single machine, which can take significant time (e.g. 1 day) if a large and/or high resolution parameter grid for circadian oscillations is to be computed.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   usage
   extending_model
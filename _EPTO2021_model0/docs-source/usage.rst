Usage
=====

Run Simulations
---------------

Open script ``sim.py`` in a text editor and specify the model and simulation parameters at the top of the file. These are described below.

**Arabinose Spike Properties**

- ``WAIT_TIME``. The time, in hours, before the arabinose spike	in the medium is applied. In this time, the external arabinose is at 0 concentration, and there is constitutive expression of KaiB and KaiC inside the *E.coli*, leading to some auto-phosphorylation steady state of KaiC. 
- ``ARABINOSE_SPIKE_DURATION``. The time, in hours, of the external arabinose spike where the transcription rate of the *kaiA* gene is considered maximal (at ``KAI_A_MAX_TRANSCRIPTION_RATE``, below).
- ``KAI_A_ARABINOSE_TRANSCRIPTION_RATE``. kaiA transcription rate when the external arabinose pulse is applied. This is assumed around the maximal transcription rate for the pBAD promoter.

**Parameter Grid Properties**

The simulator performs a **parameter grid** of simulations, with *kaiA* transcription rate (in the free running period, following the spike) on the y-axis, and with *kaiBC* constitutive transcription rate on the x-axis. 

- ``KAI_A_FR_MIN_TRANSCRIPTION_RATE`` and ``KAI_A_FR_MAX_TRANSCRIPTION_RATE``. The range of *kaiA* gene transcription rates (in the free running period) on the y-axis of the parameter grid.
- ``KAI_BC_MIN_TRANSCRIPTION_RATE`` and ``KAI_BC_MAX_TRANSCRIPTION_RATE``. The range of *kaiBC* constitutive gene transcription on the x-axis of the parameter grid.
- ``GRID_INTERVAL_EVERY``. The increase in transcription rate between points on the parameter grid - i.e. the resolution of the parameter grid. A *kaiA* transcription rate range of 0 to 2e-6, for example, will be divided into increments 0, 1e-7, 2e-7, 3e-7, ...., 2e-6, when ``GRID_INTERVAL_EVERY`` =1e-7. That is, into (2e-6/1e-7) + 1 = 21 divisions.

**Simulation Properties**

- ``SIM_DURATION``. The total duration of each simulation, including the initial ``WAIT_TIME``, the ``ARABINOSE_SPIKE_DURATION`` and then the final free running phase. 
- ``SIM_INTERVALS``. The number of data points to record from a simulation (*not* the simulation step size: this is determined automatically).

To run all the model simulations for computing the parameter grid, first activate the Python3 virtual environment (if not already) and change to the model directory: ::

	cd circadian
	source venv/bin/activate
	cd _EPTO2021_model0

Then run: ::

	python3 sim.py

.. note :: This can take a long time to complete all simulations, especially if stochastic simulations are used and ``SIM_DURATION`` is over 100 hours. I did a preliminary "scouting" simulation on a sparse parameter grid to see where the interesting oscillating region lay, narrowed the parameter ranges just to cover this range when the higher resolution parameter grid was computed.

Analyse/Visualise Results
-------------------------

Open script ``analysis.py`` in a text editor, and set ``HAS_OSCILLATION = []`` at the top of the file. Then run: :: 

	python3 analysis.py

This makes a PDF of the KaiC phosphorylation trajectory for each parameter point in the ``results/`` directory (for visual inspection).

.. note :: I manually went through all trajectory PDF files in the ``results/`` directory (beginning with underscore), and noted which ones had oscillations, and which ones had damped oscillations. It turned out to be **difficult to reliably detect** oscillations from the fourier transform of a trajectory, just based on thresholding peak height (as reported in `Teng et al. 2013 <https://science.sciencemag.org/content/340/6133/737.abstract>`_). Manual curation was possible in this case, as the parameter grid was not too large.

When the trajectories with oscillations have been noted, set the ``HAS_OSCILLATION`` list in ``analysis.py``. For example, if trajectories 38,39,40,41,56,57,58 have oscillations, set ``HAS_OSCILLATION = [38,39,40,41,56,57,58]``. 

Run the analysis script again: ::

	python3 analysis.py

This time it will produce the parameter grid heatmaps because the trajectories that produce oscillations have been specified.


Test PTO under *in vitro* Conditions
------------------------------------

The Zwicker PTO part of the model can be tested independently, under *in vitro* conditions, to verify that it works. 

The following reproduces Fig S1A from `Zwicker et al. 2010 <https://doi.org/10.1073/pnas.1007613107>`_:

1. Get a COPASI model file (``.cps`` extension) from the ``results`` directory (any will do).

2. Open the file in COPASI and make the following changes:

	- Model -> Biochemical -> Global Quantities. Set ``gamma_A`` and ``gamma_BC`` to 0, to kill all gene expression. Set ``alpha_prot`` to 0, to kill protein dilution.
	- Model -> Biochemical -> Events. Delete All events, to stop the arabinose spike.
	- Model -> Biochemical -> Species. Set the initial (conserved) Molar concentrations of the Kai protein complexes to ``A`` =0.58e-6, ``B`` =1.75e-6, ``C0`` =0.58e-6.
	- Tasks -> Timecourse -> Output Assistant. Select "Concentrations, Volumes, and Global Quantity Values", Create.
	- Tasks -> Timecourse -> Run.





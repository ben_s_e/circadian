Extending Model
===============

From Appendix 1 of technical report :download:`TR-EPTO-0<TR-EPTO-0.pdf>`:

- :download:`Experimental results excel (Feb 2021)<Data for modelling.xlsx>`
- :download:`KaiA maximal expression model in COPASI<KaiA_maximal_expression.cps>`
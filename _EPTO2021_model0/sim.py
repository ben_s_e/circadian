"""Executes a simulation of the circadian clock for each point in parameter space

This runs all simulations on a single machine (and so takes overnight)

Ben Shirt-Ediss
"""

import sbmlmodel
import cpsmodel
import analysis

import numpy
import pickle


# --------- Simulation Parameters -----------
COPASI_PATH 						= "/Applications/COPASI/CopasiSE"

WAIT_TIME	 						= 4			# h
ARABINOSE_SPIKE_DURATION 			= 4			# h
KAI_A_ARABINOSE_TRANSCRIPTION_RATE	= 2e-6		# M h^-1  

# model behaviour is scanned over this range of parameters
KAI_A_FR_MIN_TRANSCRIPTION_RATE 	= 0			# M h^-1  
KAI_A_FR_MAX_TRANSCRIPTION_RATE 	= 2e-6 		# M h^-1

KAI_BC_MIN_TRANSCRIPTION_RATE		= 0			# M h^-1
KAI_BC_MAX_TRANSCRIPTION_RATE		= 7e-6 		# M h^-1 

GRID_INTERVAL_EVERY					= 2e-7		# M h^-1

# simulation details
SIM_DURATION 						= 72		# h
SIM_INTERVALS						= 1000 		# each traj has 10k data points
# --------- Simulation Parameters -----------



if __name__ == "__main__" :

	# make parameter grid

	A_divisions = int((KAI_A_FR_MAX_TRANSCRIPTION_RATE - KAI_A_FR_MIN_TRANSCRIPTION_RATE) / GRID_INTERVAL_EVERY) + 1
	BC_divisions = int((KAI_BC_MAX_TRANSCRIPTION_RATE - KAI_BC_MIN_TRANSCRIPTION_RATE) / GRID_INTERVAL_EVERY) + 1

	kaiA_tr = numpy.linspace(KAI_A_FR_MIN_TRANSCRIPTION_RATE, KAI_A_FR_MAX_TRANSCRIPTION_RATE, A_divisions)
	kaiBC_tr = numpy.linspace(KAI_BC_MIN_TRANSCRIPTION_RATE, KAI_BC_MAX_TRANSCRIPTION_RATE, BC_divisions)

	#print(kaiA_tr)
	#print(kaiBC_tr)
	#quit()

	print("")
	print("---------------------------------------")
	print("EPTO Model SIMULATION")
	print("Post Translational Oscillator in E.coli")
	print("---------------------------------------")
	print("Ben Shirt-Ediss, 2021")
	print("---------------------------------------")
	print("")
	print("PARAMETERS")
	print("- Wait time before arabinose spike:\t\t\t%1.2f h" % (WAIT_TIME))
	print("- Arabinose spike duration:\t\t\t\t%1.2f h" % (ARABINOSE_SPIKE_DURATION))
	print("- kaiA transcription rate during spike:\t\t\t%.5e M s^-1" % (KAI_A_ARABINOSE_TRANSCRIPTION_RATE))
	print("- kaiA transcription rate in free running phase:\t%.5e to %.5e M s^-1 in %d divisions" % (KAI_A_FR_MIN_TRANSCRIPTION_RATE, KAI_A_FR_MAX_TRANSCRIPTION_RATE, A_divisions))
	print("- kaiBC transcription rate (constitutive):\t\t%.5e to %.5e M s^-1 in %d divisions" % (KAI_BC_MIN_TRANSCRIPTION_RATE, KAI_BC_MAX_TRANSCRIPTION_RATE, BC_divisions))
	print("")
	print("COPASI SIMULATION")
	print("%d x %d = %d simulations to scan parameter space" % (A_divisions, BC_divisions, A_divisions*BC_divisions))

	# save info about results, for analysis.py
	info = {	"kaiA_tr" : 				kaiA_tr,
				"kaiBC_tr" :				kaiBC_tr,
				"data_points_per_hour" :  	SIM_INTERVALS / SIM_DURATION,
				"end_of_spike_time" : 		WAIT_TIME + ARABINOSE_SPIKE_DURATION }	# time from which experimental data points collected, after the spike
	pickle.dump( info, open( "results/info.p", "wb" ) )

	# execute a simulation for each parameter point
	# (kaiBC transcription rate on x-axis of grid, kaiA transcription rate on y-axis of grid)
	
	# index of parameter point
	ppi = 1 	
	ppi_total = A_divisions*BC_divisions

	# 7  8  9
	# 4  5  6
	# 1  2  3

	for y in kaiA_tr :
		for x in kaiBC_tr :

			print("- Simulating parameter point %d of %d" % (ppi, ppi_total))

			# make simulation configuration, for this parameter point

			config = {	"COPASI_PATH" :				COPASI_PATH,
						"parameter_point_index" : 	ppi,

						"gamma_BC" : 				x,				
									# constant transcription rate of KaiBC mRNA
					  	"gamma_A_times" : 			[0,WAIT_TIME,WAIT_TIME+ARABINOSE_SPIKE_DURATION],		
					  				# times (hours) when KaiA transcription rate changes
					  	"gamma_A" : 				[0,KAI_A_ARABINOSE_TRANSCRIPTION_RATE,y],		
					  				# transcription rate of KaiA mRNA, at each above time

					  	"sim_duration" :			SIM_DURATION, 	# float
					  	"sim_intervals"  :			SIM_INTERVALS 	# integer
					  }

			# note that MULTIPLE arabinose spikes can be specified, and the model will include them all
			# its just in this application, only 1 arabinose spike happens at the beginning.

			sbml_filename = "results/%d.xml" % (config["parameter_point_index"])
			cps_filename = "results/%d.cps" % (config["parameter_point_index"])
			traj_filename = "results/%d.txt" % (config["parameter_point_index"])
			ptraj_filename = "results/%d.p" % (config["parameter_point_index"])

			# 1. generate SBML model for parameter point
			sbmlmodel.generate(config, sbml_filename=sbml_filename)

			# 2. turn SBML model in executable COPASI .cps file
			# 		Note: the name of the output traj file made from simulation is specified in this file as config["parameter_point_index"].txt
			cpsmodel.generate(config, sbml_filename=sbml_filename, cps_filename=cps_filename)
				# XXX simulation method (Gillespie SSA or variant)

			# 3. execute COPASI .cps file, to get concentration traj
			cpsmodel.execute(config, cps_filename=cps_filename)

			# 4. calculate fraction of phosphorylated KaiC monomers at each time in concentration traj
			analysis.make_KaiC_p_trajectory(traj_filename, ptraj_filename)

			ppi += 1

	print("")
	print("Done.")
	print("")



"""Generates SBML model of the EPTO v0 system, using libsbml for Python

This file follows (and extends) template:
http://model.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/libsbml-python-creating-model.html

Requirements: 
	pip install python-libsbml

Ben Shirt-Ediss, Feb 2021
"""

import sys
from libsbml import *



#
#
# PART 1: SBML model operations
#
#

def check(value, message) :

	if value == None:
		raise SystemExit('LibSBML returned a null value trying to ' + message + '.')
	elif type(value) is int:
		if value == LIBSBML_OPERATION_SUCCESS:
			return
		else:
			err_msg = 'Error encountered trying to ' + message + '.' \
					+ 'LibSBML returned error code ' + str(value) + ': "' \
					+ OperationReturnValue_toString(value).strip() + '"'
		raise SystemExit(err_msg)
	else:
		return



def new_base_model(document) :
	"""
	Returns base model, to which compartments, species, reactions, events are added
	"""
 
	# base units 
	model = document.createModel()
	check(model,                              				'create model')

	# (define the hour time unit)
	hour = model.createUnitDefinition()
	check(hour,                         					'create unit definition')
	check(hour.setId('hour'),     							'set unit definition id')
	unit = hour.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_SECOND),     				'set unit kind')
	check(unit.setExponent(1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')
	# (end: define the hour time unit)

	check(model.setTimeUnits("hour"),       				'set model-wide time units')

	# Notes on time units:
	#
	# model.setTimeUnits("second")     is the default
	# Looking at SBML 3.1 docs, apparently only 
	# 		"second, dimensionless, or units derived from these" can be used.
	#
	# You can change the base unit to hours, as above, by defining an hour unit, which
	# is 1 second * 3600 
	# Setting the base unit correctly is important when importing into COPASI
	# as COPASI checks rate constant units against it, and also draws trajectory with time in the base unit

	check(model.setExtentUnits("mole"),       				'set model units of extent')
	check(model.setSubstanceUnits('mole'),    				'set model substance units')
	check(model.setVolumeUnits('litre'),      				'set model volume units')
	check(model.setLengthUnits('dimensionless'),    		'set model length units')
	check(model.setAreaUnits('dimensionless'),				'set model area units')	

	# s^-1 unit definition (uni molecular reactions)
	per_second = model.createUnitDefinition()
	check(per_second,                         				'create unit definition')
	check(per_second.setId('per_second'),     				'set unit definition id')
	unit = per_second.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_SECOND),     				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# h^-1 unit definition (uni molecular reactions)
	per_hour = model.createUnitDefinition()
	check(per_hour,                         				'create unit definition')
	check(per_hour.setId('per_hour'),     					'set unit definition id')
	unit = per_hour.createUnit()
	check(unit,                               				'create unit on per_hour')
	check(unit.setKind(UNIT_KIND_SECOND),     				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')

	# M^-1 s^-1 unit definition (bi-molecular reactions)
	per_molar_per_second = model.createUnitDefinition()
	check(per_molar_per_second,               				'create unit definition')
	check(per_molar_per_second.setId('per_molar_per_second'), 'set unit definition id')
	unit = per_molar_per_second.createUnit()
	check(unit,                               				'create unit on per_molar_per_second')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_second.createUnit()
	check(unit,                              		 		'create unit on per_molar_per_second')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_second.createUnit()
	check(unit,                               				'create unit on per_molar_per_second')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# M^-1 hr^-1 unit definition (bi-molecular reactions)	
	per_molar_per_hour = model.createUnitDefinition()
	check(per_molar_per_hour,               						'create unit definition')
	check(per_molar_per_hour.setId('per_molar_per_hour'), 'set unit definition id')
	unit = per_molar_per_hour.createUnit()
	check(unit,                               				'create unit on per_molar_per_hour')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_hour.createUnit()
	check(unit,                              		 		'create unit on per_molar_per_hour')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_hour.createUnit()
	check(unit,                               				'create unit on per_molar_per_hour')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')

	# M (molar) unit definition for Michaelis rate constant (mol/litre)
	molar = model.createUnitDefinition()
	check(molar,               								'create unit definition')
	check(molar.setId('molar'),								 'set unit definition id')
	unit = molar.createUnit()
	check(unit,                               				'create unit on molar')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = molar.createUnit()
	check(unit,                              		 		'create unit on molar')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(-1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# M s^-1 unit definition, for Vmax of Michaelis-Menten reaction
	molar_per_second = model.createUnitDefinition()
	check(molar_per_second,               					'create unit definition')
	check(molar_per_second.setId('molar_per_second'),		'set unit definition id')
	unit = molar_per_second.createUnit()	
	check(unit,                               				'create unit on molar_per_second')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = molar_per_second.createUnit()
	check(unit,                              		 		'create unit on molar_per_second')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(-1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = molar_per_second.createUnit()
	check(unit,                               				'create unit on molar_per_second')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# M h^-1 unit definition, for Vmax of Michaelis-Menten reaction
	molar_per_hour = model.createUnitDefinition()
	check(molar_per_hour,               					'create unit definition')
	check(molar_per_hour.setId('molar_per_hour'),		'set unit definition id')
	unit = molar_per_hour.createUnit()	
	check(unit,                               				'create unit on molar_per_hour')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = molar_per_hour.createUnit()
	check(unit,                              		 		'create unit on molar_per_hour')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(-1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = molar_per_hour.createUnit()
	check(unit,                               				'create unit on molar_per_hour')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')	

	return model



def new_parameter(model, parameter_name, parameter_value, parameter_unit, has_constant_value=True) :
	"""
	Note, specify has_constant_value = False, if this parameter is to be assigned by an event
	over the course of the trajectory
	"""

	k = model.createParameter()
	check(k,                                  				'create parameter k')
	check(k.setId(parameter_name),                       	'set parameter k id')
	check(k.setConstant(has_constant_value),                'set parameter k constant')
	check(k.setValue(parameter_value),                     	'set parameter k value')
	check(k.setUnits(parameter_unit),          				'set parameter k units')




def new_compartment(model, compartment_name, compartment_vol_litres) :

	c = model.createCompartment()
	check(c,                                 				'create compartment')
	check(c.setId(compartment_name),                		'set compartment id')
	check(c.setConstant(False),              				'set compartment variable size') 	# this must be False if the volume changes -- i.e. if an event changes the volume.
	check(c.setSize(compartment_vol_litres),        		'set compartment size')
	check(c.setSpatialDimensions(3),         				'set compartment dimensions')
	check(c.setUnits('litre'),               				'set compartment size units')



def new_species(model, species_name, initial_conc_molar, compartment_name, compartment_vol_litres) :

	species_moles = initial_conc_molar * compartment_vol_litres

	s = model.createSpecies()
	check(s,                                 				'create species s')
	check(s.setId(species_name),             				'set species s id')
	check(s.setCompartment(compartment_name),				'set species s compartment')
	check(s.setConstant(False),              				'set "constant" attribute on s')
	check(s.setInitialAmount(species_moles),    			'set initial amount for s')
	check(s.setSubstanceUnits('mole'),       				'set substance units for s')
	check(s.setBoundaryCondition(False),     				'set "boundaryCondition" on s')
	check(s.setHasOnlySubstanceUnits(False), 				'set "hasOnlySubstanceUnits" on s')





def new_michaelis_menten_kinetic_function(model) :

	MM = model.createFunctionDefinition()

	MM.setIdAttribute("Michaelis_Menten_mRNA_Degradation")  # name of MM function, referenced in kineticLaw
	MM.setName("Michaelis-Menten (for mRNA Degradation)")	   # this is the name appearing in COPASI function list

	# note that on the page for the reaction, 
	# COPASI refuses to call the parameters the names you give here. 
	# It just sets the name to the global quantity the param is bound to.
	# no idea why! But the function definition still works.
	xml = """
		<math xmlns="http://www.w3.org/1998/Math/MathML">
		  <lambda>
			<bvar>
			  <ci> substrate </ci>
			</bvar>
			<bvar>
			  <ci> Kmichaelis </ci>
			</bvar>
			<bvar>
			  <ci> Vmax </ci>
			</bvar>
			<apply>
			  <divide/>
			  <apply>
				<times/>
				<ci> Vmax </ci>
				<ci> substrate </ci>
			  </apply>
			  <apply>
				<plus/>
				<ci> Kmichaelis </ci>
				<ci> substrate </ci>
			  </apply>
			</apply>
		  </lambda>
		</math>"""	

	math_ast = readMathMLFromString(xml)
	MM.setMath(math_ast)





def new_unimolecular_reaction_1product(model, reaction_name, reactant_name, product_name, rate_constant_name, compartment_name) :
	"""
	A --> B
	"""

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant = r.createReactant()
	check(reactant,                       					'create reactant')
	check(reactant.setSpecies(reactant_name),      			'assign reactant species')
	check(reactant.setConstant(True),     					'set "constant" on reactant')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')
 
	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI
	
	formula = '%s * %s * %s' % (rate_constant_name, reactant_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')

	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')




def new_unimolecular_reaction_2product(model, reaction_name, reactant_name, product1_name, product2_name, rate_constant_name, compartment_name) :
	"""
	A --> B + C

	(can be used as reverse of bimolecular reaction below)
	"""

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant = r.createReactant()
	check(reactant,                       					'create reactant')
	check(reactant.setSpecies(reactant_name),      			'assign reactant species')
	check(reactant.setConstant(True),     					'set "constant" on reactant')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product1_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product1')
 
	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product2_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product2')

	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI
	
	formula = '%s * %s * %s' % (rate_constant_name, reactant_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')
 
	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')



def new_bimolecular_reaction_1product(model, reaction_name, reactant1_name, reactant2_name, product_name, rate_constant_name, compartment_name) :
	"""
	A + B --> C
	"""	

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant1 = r.createReactant()
	check(reactant1,                       					'create reactant')
	check(reactant1.setSpecies(reactant1_name),      		'assign reactant species')
	check(reactant1.setConstant(True),     					'set "constant" on reactant 1')

	reactant2 = r.createReactant()
	check(reactant2,                       					'create reactant')
	check(reactant2.setSpecies(reactant2_name),      		'assign reactant species')
	check(reactant2.setConstant(True),     					'set "constant" on reactant 2')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')

	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI

	formula = '%s * %s * %s * %s' % (rate_constant_name, reactant1_name, reactant2_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')

	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')




def new_michaelis_menten_reaction(model, reaction_name, reactant_name, product_name, K_constant_name, V_constant_name, compartment_name) :
	"""
	A --> C, an approximation of A + E --> C + E

	Note: this requires the functionDefinition 
	"""	
	
	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant = r.createReactant()
	check(reactant,                       					'create reactant')
	check(reactant.setSpecies(reactant_name),      			'assign reactant species')
	check(reactant.setConstant(True),     					'set "constant" on reactant 1')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')

	# assign custom rate law, using the MM function, using MathML
	kinetic_law = r.createKineticLaw()

	xml = """
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <apply>
              <times/>
              <ci> %s </ci>
              <apply>
                <ci> Michaelis_Menten_mRNA_Degradation </ci>
                <ci> %s </ci>
                <ci> %s </ci>
                <ci> %s </ci>
              </apply>
            </apply>
          </math>""" % (compartment_name, reactant_name, K_constant_name, V_constant_name)

	math_ast = readMathMLFromString(xml)
	kinetic_law.setMath(math_ast)




def new_synthesis_reaction_1product(model, reaction_name, product_name, rate_constant_name, compartment_name) :
	"""
	--> A   (proceeding at a constant synthesis rate)
	"""

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')
 
	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI
	
	formula = '%s * %s' % (rate_constant_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')

	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')



def new_discontinuous_event(model, event_name, event_priority, trigger_equation, variable_affected, variable_update_formula) :

	# event_name is a string
	e = model.createEvent()
	check(e, 												'create event')
	check(e.setId(event_name), 								'add id to event')
	check(e.setUseValuesFromTriggerTime(True), 				'set use values from trigger time')

	# even_priority is an integer
	pri = e.createPriority()
	pri_ast = parseL3Formula(str(event_priority))
	check(pri.setMath(pri_ast), 							'add priority to event')

	# e.g. Time == 5
	tri = e.createTrigger()
	check(tri, 												'add trigger to event')
	check(tri.setPersistent(True),							'set persistence of trigger')
	check(tri.setInitialValue(False), 						'set initial value of trigger')  # i.e. it wont fire immediately

	tri_ast = parseL3Formula(trigger_equation)
	check(tri.setMath(tri_ast), 							'add formula to trigger')	

	# e.g. a species name, or a rate constant name
	assign = e.createEventAssignment()
	check(assign, 											'add event assignment to event')
	check(assign.setVariable(variable_affected), 			'add variable to event assignment')    

	# e.g. species_name+5, or k=53
	val_ast = parseL3Formula(variable_update_formula)
	check(assign.setMath(val_ast), 							'add value to event assignment')

































#
#
# PART 2: Create SBML model of PPC in-vitro model (full model)
#
#

def create_epto0_SBML(config, sbml_filename) :

	try:
		document = SBMLDocument(3, 1)
	except ValueError:
		raise SystemExit("Could not create SBMLDocument object")

	# ====================================
	# configure base model
	# ====================================

	vol0 = 1e-15 	# litres. Volume of a bacterium is approx 1 um^3, which is 1e-15 litres
					# (Not relevant for deterministic simulation)

	model = new_base_model(document)
	new_compartment(model, "ecoli", vol0)


	########################################################################################
	#
	#
	# Arabinose induction events
	#
	#
	########################################################################################

	# the value of parameter gamma_A (A_mRNA synthesis rate), is changed with events
	for i, event_time in enumerate(config["gamma_A_times"]) :

		gamma_A = config["gamma_A"][i]

		new_discontinuous_event(model, "spike%d" % (i+1), i+1, "Time == %1.2f" % (event_time), "gamma_A", str(gamma_A))

								# model, event_name, event_priority, 
								#							trigger_equation, variable_affected, variable_update_formula
	
	########################################################################################
	#
	#
	# Gene expression reactions
	#
	#
	########################################################################################

	# ====================================
	# add new function definition for a Michaelis-Menten reaction
	# ====================================

	# (or could just reference COPASIs built in one, in the kinetic law?)
	new_michaelis_menten_kinetic_function(model)

	# ====================================
	# add parameters (8)
	# ====================================

	gamma_A 		= 0 						# M h^-1 	-- initial value only. Events (above) set its value
	gamma_BC		= config["gamma_BC"]		# M h^-1

	new_parameter(model, "gamma_A", gamma_A, "molar_per_hour", has_constant_value=False)
	new_parameter(model, "gamma_BC", gamma_BC, "molar_per_hour")

	k_A 			= 2.7 		# h^-1
	k_BC			= 2.7 		# h^-1
	k_assemble		= 1e9 		# M^-1 h^-1
	beta_mRNA		= 0.2e-6	# M h^-1		# Michaelis Menten Vmax of active degradation
	K_mRNA			= 0.2e-6	# M 			# Michaelis Menten K of active degradation
	alpha_prot		= 0.03		# h^-1

	new_parameter(model, "k_A", k_A, "per_hour")
	new_parameter(model, "k_BC", k_BC, "per_hour")
	new_parameter(model, "k_assemble", k_assemble, "per_molar_per_hour")
	new_parameter(model, "beta_mRNA", beta_mRNA, "molar_per_hour")
	new_parameter(model, "K_mRNA", K_mRNA, "molar")
	new_parameter(model, "alpha_prot", alpha_prot, "per_hour")

	# ====================================
	# add species (13)		12 excluding EMPTY.
	# ====================================

	# Note: all species start at 0 concentration
	# Synthesis reactions build concentration over time

	# Usage:
	# new_species(model, species_name, initial_conc_molar, compartment_name, compartment_vol_litres)
	
	new_species(model, "A_mRNA", 0, "ecoli", vol0)
	new_species(model, "BC_mRNA", 0, "ecoli", vol0)
	new_species(model, "A_monomer", 0, "ecoli", vol0)
	new_species(model, "A", 0, "ecoli", vol0)	# KaiA dimer
	new_species(model, "B_monomer", 0, "ecoli", vol0)
	new_species(model, "B", 0, "ecoli", vol0) 	# KaiB dimer
	new_species(model, "C_monomer", 0, "ecoli", vol0)
	new_species(model, "C_dimer", 0, "ecoli", vol0)
	new_species(model, "C_trimer", 0, "ecoli", vol0)
	new_species(model, "C_4mer", 0, "ecoli", vol0)
	new_species(model, "C_5mer", 0, "ecoli", vol0)
	new_species(model, "C0", 0, "ecoli", vol0)	# unphosphorylated active KaiC heaxmer

	# just to catch degraded species -- not a state variable
	new_species(model, "EMPTY", 0, "ecoli", vol0)

	# ====================================
	# add reactions (23)
	# ====================================

	# mRNA synthesis
	new_synthesis_reaction_1product(model, "T1", "A_mRNA", "gamma_A", "ecoli")
	new_synthesis_reaction_1product(model, "T2", "BC_mRNA", "gamma_BC", "ecoli")

	# mRNA translation
	new_unimolecular_reaction_1product(model, "T3", "A_mRNA", "A_monomer", "k_A", "ecoli")
	new_unimolecular_reaction_2product(model, "T4", "BC_mRNA", "B_monomer", "C_monomer", "k_BC", "ecoli")

	# dimer and hexamer self-assembly
	new_bimolecular_reaction_1product(model, "T5", "A_monomer", "A_monomer", "A", "k_assemble", "ecoli")
	new_bimolecular_reaction_1product(model, "T6", "B_monomer", "B_monomer", "B", "k_assemble", "ecoli")

	new_bimolecular_reaction_1product(model, "T7_1", "C_monomer", "C_monomer", "C_dimer", "k_assemble", "ecoli")
	new_bimolecular_reaction_1product(model, "T7_2", "C_dimer", "C_monomer", "C_trimer", "k_assemble", "ecoli")
	new_bimolecular_reaction_1product(model, "T7_3", "C_trimer", "C_monomer", "C_4mer", "k_assemble", "ecoli")
	new_bimolecular_reaction_1product(model, "T7_4", "C_4mer", "C_monomer", "C_5mer", "k_assemble", "ecoli")	
	new_bimolecular_reaction_1product(model, "T7_5", "C_5mer", "C_monomer", "C0", "k_assemble", "ecoli")
	
	# catalysed degradation of mRNAs
	new_michaelis_menten_reaction(model, "T8_1", "A_mRNA", "EMPTY", "K_mRNA", "beta_mRNA", "ecoli")
	new_michaelis_menten_reaction(model, "T8_2", "BC_mRNA", "EMPTY", "K_mRNA", "beta_mRNA", "ecoli")

	# dilution of proteins (that are not clock proteins: the latter are degraded in Zwicker spec below)
	new_unimolecular_reaction_1product(model, "T9_1", "A_monomer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_2", "A", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_3", "B_monomer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_4", "B", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_5", "C_monomer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_6", "C_dimer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_7", "C_trimer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_8", "C_4mer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_9", "C_5mer", "EMPTY", "alpha_prot", "ecoli")
	new_unimolecular_reaction_1product(model, "T9_10", "C0", "EMPTY", "alpha_prot", "ecoli")




	
	########################################################################################
	#
	#
	# Zwicker PTO in-vitro model
	#
	#
	########################################################################################

	# ====================================
	# add parameters (77)
	# ====================================

	# Usage: 
	# new_parameter(model, parameter_name, parameter_value, parameter_unit)

	# 2.1 parameters table

	fi 			= [1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,10]						# h^-1
	bi 			= 100 														# h^-1
	ki_Af 		= 1.72e10 													# M^-1 h^-1
	ki_Ab 		= [1,3,9,27,81,243,729]										# h^-1
	k_pf 		= 1															# h^-1
	iki_Bf		= [1.72e9 * k for k in [0.001,0.1,1,1,1,1,1]] 				# M^-1 h^-1
	iki_Bf__2 	= [2 * k for k in iki_Bf] 									# M^-1 h^-1		[twice above]
	iki_Bb		= [10,1,1,1,1,1,1]											# h^-1
	iki_Bb__2   = [2 * k for k in iki_Bb]  									# h^-1			[twice above]
	iki_Af 		= [1.72e9 * k for k in [1e-2,1e3,1e3,1e3,1e2,1e-3,1e-4]] 	# M^-1 h^-1
	iki_Af__2   = [2 * k for k in iki_Af]									# M^-1 h^-1  	[twice above]
	iki_Ab		= [10,1,1,1,1,1,10]											# h^-1
	iki_Ab__2	= [2 * k for k in iki_Ab]									# h^-1			[twice above]
	k_ps		= 0.025 													# h^-1
	k_dps 		= 0.4 														# h^-1
	ik_ps		= 0.025 													# h^-1
	ik_dps		= 0.4 														# h^-1

		# NOTE: the k_dps and ik_dps values of 100 h^-1 in PNAS SI are INCORRECT
		# as confirmed by email from Pieter Rein Ten Wolde

	# 2.2 add single value rate constants to model
	
	new_parameter(model, "bi", bi, "per_hour")
	new_parameter(model, "ki_Af", ki_Af, "per_molar_per_hour")
	new_parameter(model, "k_pf", k_pf, "per_hour")
	new_parameter(model, "k_ps", k_ps, "per_hour")
	new_parameter(model, "k_dps", k_dps, "per_hour")
	new_parameter(model, "ik_ps", ik_ps, "per_hour")
	new_parameter(model, "ik_dps", ik_dps, "per_hour")

	# 2.3 add rate constants with an i-dependent value, to the model

	for i in range(0,6+1) :
		# fi
		new_parameter(model, "f%d" % (i), fi[i], "per_hour")
		
		# ki_Ab
		new_parameter(model, "k%d_Ab" % (i), ki_Ab[i], "per_hour")
		
		# iki_Bf
		new_parameter(model, "ik%d_Bf" % (i), iki_Bf[i], "per_molar_per_hour")
		new_parameter(model, "ik%d_Bf__2" % (i), iki_Bf__2[i], "per_molar_per_hour")
		# iki_Bb
		new_parameter(model, "ik%d_Bb" % (i), iki_Bb[i], "per_hour")
		new_parameter(model, "ik%d_Bb__2" % (i), iki_Bb__2[i], "per_hour")
		
		# iki_Af
		new_parameter(model, "ik%d_Af" % (i), iki_Af[i], "per_molar_per_hour")
		new_parameter(model, "ik%d_Af__2" % (i), iki_Af__2[i], "per_molar_per_hour")
		# iki_Ab
		new_parameter(model, "ik%d_Ab" % (i), iki_Ab[i], "per_hour")
		new_parameter(model, "ik%d_Ab__2" % (i), iki_Ab__2[i], "per_hour")


	# ====================================
	# add species (55)
	#				Note the A,B,C0 species were ADDED BEFORE in the gene expression part of the model
	#				..and so are not added again here. Alone, the PTO has 58 total species.
	# ====================================

	# Usage:
	# new_species(model, species_name, initial_conc_molar, compartment_name, compartment_vol_litres)

	
	# Note: in-vivo, ALL species start at 0 concentration. Gene synthesis reactions build conc over time, after the plasmid is inserted, and after external arabinose pulses.
	all_clock_species = set()

	# other species that depend on phosphorylation state i
	# there are 7 of each of these species
	for i in range(0,6+1) :

		if i > 0 : 	# don't add C0 species, it was added before
			# Ci
			species = "C%d" % (i)
			new_species(model, species, 0.0, "ecoli", vol0)
			all_clock_species.add(species)

		# iCi
		species = "iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)

		# ACi
		species = "AC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)

		# B1iCi
		species = "B1iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)

		# B2iCi
		species = "B2iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)

		# A1B1iCi
		species = "A1B1iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)

		# A1B2iCi
		species = "A1B2iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)		

		# A2B2iCi
		species = "A2B2iC%d" % (i)
		new_species(model, species, 0.0, "ecoli", vol0)	
		all_clock_species.add(species)


	# ====================================
	# add clock reactions (188)
	# ====================================

	# --- reaction 1
	for i in range(0,6+1) :
	
		# forwards
		# Ci --> iCi
		new_unimolecular_reaction_1product(model, "C1_%df" % (i), \
			"C%d" % (i), \
			"iC%d" % (i), \
			"f%d" % (i), "ecoli")

		# backwards
		# iCi --> Ci
		new_unimolecular_reaction_1product(model, "C1_%db" % (i), \
			"iC%d" % (i), \
			"C%d" % (i), \
			"bi", "ecoli")

	# --- reaction 2
	for i in range(0,6+1) :

		# forwards
		# Ci + A --> ACi
		new_bimolecular_reaction_1product(model, "C2_%df" % (i), \
			"C%d" % (i), "A", \
			"AC%d" % (i), \
			"ki_Af", "ecoli")

		# backwards
		# ACi --> Ci + A
		new_unimolecular_reaction_2product(model, "C2_%db" % (i), \
			"AC%d" % (i), \
			"C%d" % (i), "A", \
			"k%d_Ab" % (i), "ecoli")

		if i < 6 :
			# irreversible
			# ACi --> Ci+1 +  A
			new_unimolecular_reaction_2product(model, "C2_%dirrev" % (i), \
				"AC%d" % (i), \
				"C%d" % (i+1), "A", \
				"k_pf", "ecoli")

	# --- reaction 3
	for i in range(0,6+1) :

		# forwards
		# iCi + B --> B1iCi
		new_bimolecular_reaction_1product(model, "C3_%df" % (i), \
			"iC%d" % (i), "B", \
			"B1iC%d" % (i), \
			"ik%d_Bf__2" % (i), "ecoli")

		# backwards
		# B1iCi --> iCi + B
		new_unimolecular_reaction_2product(model, "C3_%db" % (i), \
			"B1iC%d" % (i), \
			"iC%d" % (i), "B", \
			"ik%d_Bb" % (i), "ecoli")

	# --- reaction 4
	for i in range(0,6+1) :

		# forwards
		# B1iCi + B --> B2iCi		
		new_bimolecular_reaction_1product(model, "C4_%df" % (i), \
			"B1iC%d" % (i), "B", \
			"B2iC%d" % (i), \
			"ik%d_Bf" % (i), "ecoli")

		# backwards
		# B2iCi --> B1iCi + B
		new_unimolecular_reaction_2product(model, "C4_%db" % (i), \
			"B2iC%d" % (i), \
			"B1iC%d" % (i), "B", \
			"ik%d_Bb__2" % (i), "ecoli")

	# --- reaction 5
	for i in range(0,6+1) :

		# forwards
		# B1iCi + A --> A1B1iCi
		new_bimolecular_reaction_1product(model, "C5_%df" % (i), \
			"B1iC%d" % (i), "A", \
			"A1B1iC%d" % (i), \
			"ik%d_Af" % (i), "ecoli")

		
		# backwards
		# A1B1iCi --> B1iCi + A
		new_unimolecular_reaction_2product(model, "C5_%db" % (i), \
			"A1B1iC%d" % (i), \
			"B1iC%d" % (i), "A", \
			"ik%d_Ab" % (i), "ecoli")

	# --- reaction 6
	for i in range(0,6+1) :
		
		# forwards
		# B2iCi + A --> A1B2iCi
		new_bimolecular_reaction_1product(model, "C6_%df" % (i), \
			"B2iC%d" % (i), "A", \
			"A1B2iC%d" % (i), \
			"ik%d_Af__2" % (i), "ecoli")

		# backwards
		# A1B2iCi --> B2iCi + A
		new_unimolecular_reaction_2product(model, "C6_%db" % (i), \
			"A1B2iC%d" % (i), \
			"B2iC%d" % (i), "A", \
			"ik%d_Ab" % (i), "ecoli")		

	# --- reaction 7
	for i in range(0,6+1) :

		# forwards
		# A1B2iCi + A --> A2B2iCi	
		new_bimolecular_reaction_1product(model, "C7_%df" % (i), \
			"A1B2iC%d" % (i), "A", \
			"A2B2iC%d" % (i), \
			"ik%d_Af" % (i), "ecoli")

		# backwards
		# A2B2iCi --> A1B2iCi + A
		new_unimolecular_reaction_2product(model, "C7_%db" % (i), \
			"A2B2iC%d" % (i), \
			"A1B2iC%d" % (i), "A", \
			"ik%d_Ab__2" % (i), "ecoli")			


	# simple phosphorylation and dephosphorylation reactions

	# --- reaction 8
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# Ci --> Ci+1
			new_unimolecular_reaction_1product(model, "C8_%df" % (i), \
				"C%d" % (i), \
				"C%d" % (i+1), \
				"k_ps", "ecoli")

			# backwards
			# Ci+1 --> Ci
			new_unimolecular_reaction_1product(model, "C8_%db" % (i), \
				"C%d" % (i+1), \
				"C%d" % (i), \
				"k_dps", "ecoli")			

	# --- reaction 9
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# iCi --> iCi+1
			new_unimolecular_reaction_1product(model, "C9_%df" % (i), \
				"iC%d" % (i), \
				"iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# iCi+1 --> iCi
			new_unimolecular_reaction_1product(model, "C9_%db" % (i), \
				"iC%d" % (i+1), \
				"iC%d" % (i), \
				"ik_dps", "ecoli")	

	# --- reaction 10
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# B1iCi --> B1iCi+1
			new_unimolecular_reaction_1product(model, "C10_%df" % (i), \
				"B1iC%d" % (i), \
				"B1iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# B1iCi+1 --> B1iCi
			new_unimolecular_reaction_1product(model, "C10_%db" % (i), \
				"B1iC%d" % (i+1), \
				"B1iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 11
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# B2iCi --> B2iCi+1
			new_unimolecular_reaction_1product(model, "C11_%df" % (i), \
				"B2iC%d" % (i), \
				"B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# B2iCi+1 --> B2iCi
			new_unimolecular_reaction_1product(model, "C11_%db" % (i), \
				"B2iC%d" % (i+1), \
				"B2iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 12
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A1B1iCi --> A1B1iCi+1
			new_unimolecular_reaction_1product(model, "C12_%df" % (i), \
				"A1B1iC%d" % (i), \
				"A1B1iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A1B1iCi+1 --> A1B1iCi
			new_unimolecular_reaction_1product(model, "C12_%db" % (i), \
				"A1B1iC%d" % (i+1), \
				"A1B1iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 13
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A1B2iCi --> A1B2iCi+1
			new_unimolecular_reaction_1product(model, "C13_%df" % (i), \
				"A1B2iC%d" % (i), \
				"A1B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A1B2iCi+1 --> A1B2iCi
			new_unimolecular_reaction_1product(model, "C13_%db" % (i), \
				"A1B2iC%d" % (i+1), \
				"A1B2iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 14
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A2B2iCi --> A2B2iCi+1
			new_unimolecular_reaction_1product(model, "C14_%df" % (i), \
				"A2B2iC%d" % (i), \
				"A2B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A2B2iCi+1 --> A2B2iCi
			new_unimolecular_reaction_1product(model, "C14_%db" % (i), \
				"A2B2iC%d" % (i+1), \
				"A2B2iC%d" % (i), \
				"ik_dps", "ecoli")


	# =========================================
	# add clock protein dilution reactions (55)
	# =========================================

	for i, species in enumerate(all_clock_species) :

		new_unimolecular_reaction_1product(model, "C15_%d" % (i), \
			"%s" % (species), \
			"EMPTY", \
			"alpha_prot", "ecoli")


	# ====================================
	# export SBML
	# ====================================	
	f = open(sbml_filename, "w")
	f.write(writeSBMLToString(document))
	f.close()






























#
#
# Execute
#
#

def generate(config, sbml_filename="epto0.xml") :

	create_epto0_SBML(config, sbml_filename)
		# config is a dictionary with gamma_A and gamma_BC settings



"""
Some helpful links I found on creating custom kinetic laws:

# createKineticLaw
https://synonym.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/classlibsbml_1_1_model.html#a89e1b2f1494214c0aeb6fcd617e6ac46

# kineticLaw object returned from createKineticLaw
https://synonym.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/classlibsbml_1_1_kinetic_law.html

	The example uses: 
		kinetic_law.setMath(math_ast)

	There is also
		kinetic_law.setFormula()
		https://synonym.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/classlibsbml_1_1_kinetic_law.html#a133aa3631a26eb95a7279c911d44b74a

	In both cases, you need to specify formula in AST format 
		(Abstract Syntax Tree)

		The API for this is here:
		http://model.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/libsbml-math.html

		Bottom of doc: you can read in MathML directly and make an AST from it.
		This is maybe easiest to do, just copy the COPASI mathML.

	In the kinetic law, you can name a function that you have specified as a function definition

# createFunctionDefinition
https://synonym.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/classlibsbml_1_1_model.html#aef2ebe33543f5bc6849cb729a248b156

# FunctionDefinition object returned from createFunctionDefinition
https://synonym.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/classlibsbml_1_1_function_definition.html

	It has a setMath() method, which takes an AST.
	simply make the AST from COPASI MathML of the reaction.

	Also, need to setId() and setName() of the function.
	Id is used in a rate law definition, Name is displayed in the COPASI function list.
"""


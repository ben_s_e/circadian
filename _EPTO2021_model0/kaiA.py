"""

Ben Shirt-Ediss
"""

import matplotlib.pyplot as plt
from scipy.optimize import leastsq
import math
import numpy


# ------------------------------------------------------------------------
# 1. Experimental tabulated data
# key: external arabinose conc (uM) 
# value: in-vivo KaiA concentration (in A.U.) after 4 hours

kaiA_AU_table = {	0:		5.1,
					0.01:	7.1,
					0.50:	5.4,
					1.00:	10.2,
					10.00:	21.7,
					20.00:	34.6,
					50.00:	45.2,
					100.00:	43.1	}


# 2. Calculated from maximal transcription model

kaiA_4hr_saturation_conc = 6.4e-6 		# 6.4uM

# 3. Transform kaiA_AU_table, so that 
# key: external arabinose (M)
# value: estimated in-vivo KaiA concentration (in M) after 4 hours

kaiA_M_table = {}
norm = kaiA_AU_table[50.00]
for ara_uM in kaiA_AU_table :
	kaiA_M_table[ara_uM/1e6] = (kaiA_AU_table[ara_uM] / norm) * kaiA_4hr_saturation_conc

# 4. Make ordered lists of ara conc (ara_M) and kaiA conc (kaiA_M), from the table above
ara_M = sorted(kaiA_M_table.keys())
kaiA_M = []
for a in ara_M :
	kaiA_M.append( kaiA_M_table[a] )
# ------------------------------------------------------------------------



def fitting_function(x, beta, gamma, k) :

	bracket = 1 - math.exp(-1 * k * x)

	return ((beta - gamma) * bracket) + gamma




def error_function(fitting_params, beta, gamma) :	
					# list of params to fit (i.e. k), followed by other static parameters beta and gamma
	
	k = fitting_params[0]
	
	model = [fitting_function(x, beta, gamma, k) for x in ara_M]
	experiment = kaiA_M

	# calc squared error between model and experiment at each arabinose concentration
	seat = [(x1 - x2)**2 for (x1, x2) in zip(model, experiment)]

	return seat









def plot_kaiA_graph() :
	"""Plot experimental data, with KaiA concentrations put into Molar
		and fitting function shown
	"""

	# perform fitting to experimental data
	k0 = 1e3
	fitting_params = [k0] 		# fitting parameter -- initial guess
	beta = kaiA_M_table[50e-6] 	# constant: maximum
	gamma = kaiA_M_table[0]		# constant: zero offset (important to maintain)	

	x, cov_x, infodict, mesg, ier = leastsq(error_function, fitting_params[:], args=(beta, gamma), full_output=1)
	k = float(x) # fitted answer

	
	# compute fitting function, for plotting
	x_list = list( numpy.linspace(0,100e-6, 1000) )
	fit = [fitting_function(x, beta, gamma, k) for x in x_list]

	
	# plot 1: normal x axis
	plt.figure()
	plt.subplot(2, 1, 1)
	plt.plot(ara_M, kaiA_M, "k.")
	plt.plot(x_list, fit, "r")
	plt.xlabel("External arabinose conc (M)")
	plt.ylabel("KaiA conc at 4 hours (M)")	
	
	
	# plot 2: fitting when x axis is squashed into log spacing
	plt.subplot(2, 1, 2)
	plt.semilogx(ara_M, kaiA_M, "k.")
	plt.semilogx(x_list, fit, "r")
	plt.xlabel("External arabinose conc (M) - log scale")
	plt.ylabel("KaiA conc at 4 hours (M)")	

	print("fitted k: %1.2f" % k)

	plt.show()




# Debugging fitting error: shows plot of fitting error ---------------------
def sse(list1, list2) :
	# sum squared error between two lists of floats

	ss = 0

	for i, val1 in enumerate(list1) :
		val2 = list2[i]
		ss += ((val1 - val2) ** 2)

	return ss




def debug_fitting_error() :

	k_list = list(numpy.linspace(0,1e6,1000))
	sse_list = []

	beta = kaiA_M_table[50e-6] 	# constant: maximum
	gamma = kaiA_M_table[0]		# constant: zero offset (important to maintain)	

	for k in k_list :
		model = [fitting_function(x, beta, gamma, k) for x in ara_M]
		experiment = kaiA_M

		# calculate the sum squared error between the 2 lists
		sse_list.append( sse(model, experiment) )

	plt.figure()
	plt.plot(k_list, sse_list)
	plt.xlabel("k")
	plt.ylabel("sum squared error")
	plt.show()
# ---------------------




if __name__ == "__main__" :
	
	#debug_fitting_error()
	
	plot_kaiA_graph()







#
#
# Simple heatmap
#
# Ben Shirt-Ediss

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches



def plot_heatmap(output_filename, mytitle, datamatrix, row_label, row_num, row_ticklabels, col_label, col_num, col_ticklabels, colourbar_min=None, colourbar_max=None, colourbar_shrink=1.0, colourbar_ticks=None, colourbar_orientation="vertical", colour_map="plasma", maskval=None) :

	# Different colour maps
	# https://matplotlib.org/3.1.1/tutorials/colors/colormaps.html
	# 
	# The perceptually uniform sequential colour maps are probably best to use for error surfaces
	# e.g., set colour_map to:
	#
	#	viridis		
	#	plasma
	#	inferno
	#	magma
	# 	cividis
	#
	# Also the sequential colour maps should be ok:
	#
	# 	Blues
	#	Greens
	#	Reds
	#	YlOrRd		(yellow, orange, red)	


	plt.figure()

	ax = plt.subplot(1,1,1)

	# set scale limits of the colourbar if required
	if (colourbar_min != None and colourbar_max != None) :
		plt.imshow(datamatrix, interpolation='nearest', vmin=colourbar_min, vmax=colourbar_max, cmap=colour_map)
	else :
		plt.imshow(datamatrix, interpolation='nearest', cmap=colour_map)
	
	cbar = plt.colorbar(orientation=colourbar_orientation, shrink=colourbar_shrink)
		# play with shrink percentage, to get vertical colour bar the same size as the figure
	
	if colourbar_ticks != None :   # colourbar ticks is a list integers, of where to display each tick on colour bar
		cbar.set_ticks(colourbar_ticks)
		tkl = [str(tk) for tk in colourbar_ticks]
		cbar.set_ticklabels(tkl)

	plt.title(mytitle)

	plt.xticks(range(col_num))
	ax.set_xticklabels(col_ticklabels, rotation=90)
	plt.xlabel(col_label)

	plt.yticks(range(row_num))
	ax.set_yticklabels(row_ticklabels)
	plt.ylabel(row_label)

	ax.tick_params(labelsize = 7.0) # font size of tick labels

	# mask squares whose data = maskval, by drawing white patches over top of heatmap
	# heatmap squares are 1 unit across, and there is a -0.5 unit offset in both x and y directions
	if maskval != None :

		maskcol = "xkcd:white"

		for y, row in enumerate(datamatrix) :
			for x, val in enumerate(row) :
				if val == maskval :
					ax.add_patch(
	          			mpatches.Rectangle(
	              			(x-0.5, y-0.5),     # (x,y)
	              			1,          # width
	              			1,          # height
	              			fill=True, 
	              			color=maskcol))	

	plt.savefig(output_filename, bbox_inches='tight', dpi=300)
	plt.close()





#
#
# DEMO
#
#
#


if __name__ == "__main__" :

	
	#Simple Usage Example: 
	datamatrix = [[1,2,3,7,5,1,2,4],[4,5,6,0.5,3,3,1,1],[3,2,1,9,0.3,0.5,1,2]]
	row_num = 3
	row_ticklabels = ["row1", "row2", "row3"]
	col_num = 8
	col_ticklabels = ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8"]
	
	plot_heatmap("test_heatmap.png", "DEMO", datamatrix, "ROWS", row_num, row_ticklabels, "COLS", col_num, col_ticklabels, colourbar_min=None, colourbar_max=None, colour_map="YlOrRd")





"""Creates a command line executable COPASI .cps model file, from SBML model spec

The .cps file produced from the SBML file is tweak to change the time course duration
(Time Course is not part of the SBML model itself, its only part of its simulation)

Ben Shirt-Ediss
"""


import subprocess


def generate(config, sbml_filename="epto0.xml", cps_filename="epto0.cps") :

	# First, tell COPASI to save the SBML model as a CPS copasi file (which is also XML)

	cmd = "%s --importSBML %s --save %s" % (config["COPASI_PATH"], sbml_filename, cps_filename)
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, universal_newlines=True)
	(output, err) = p.communicate()

	# Second, open the CPS copasi file (XML), and tweak the Time Course definition

	cps_file = open(cps_filename,"r")
	cps_filepath_executable = "%s_executable" % (cps_filename)
	cps_file_executable = open(cps_filepath_executable,"w")

	# (1) Mark model as executable

	m1 = "name=\"Time-Course\" type=\"timeCourse\" scheduled=\"false\""
	m1r = "name=\"Time-Course\" type=\"timeCourse\" scheduled=\"true\""  # change scheduled flag to true

	# (2) Specify an Trajectory output report, and output file to dump trajectory
	# note that default trajectory report is always Report_12

	m2 = "<Report reference=\"Report_12\" target=\"\" append=\"1\" confirmOverwrite=\"1\"/>"
	m2r = "<Report reference=\"Report_12\" target=\"%d.txt\" append=\"0\" confirmOverwrite=\"0\"/>" % (config["parameter_point_index"])

	# (3) Adding simulation duration, stepnumber and step size

	m3 = "<Parameter name=\"StepNumber\" type=\"unsignedInteger\" value=\"100\"/>"
	m3r = "<Parameter name=\"StepNumber\" type=\"unsignedInteger\" value=\"%d\"/>" % (config["sim_intervals"])
	m4 = "<Parameter name=\"StepSize\" type=\"float\" value=\"0.01\"/>" 
	m4r = "<Parameter name=\"StepSize\" type=\"float\" value=\"%.5e\"/>" % (config["sim_duration"] / config["sim_intervals"])
	m5 = "<Parameter name=\"Duration\" type=\"float\" value=\"1\"/>"
	m5r = "<Parameter name=\"Duration\" type=\"float\" value=\"%1.5f\"/>" % (config["sim_duration"])

	# (4) Change simulation method to Direct SSA (default is deterministic)
	m6 = "<Method name=\"Deterministic (LSODA)\" type=\"Deterministic(LSODA)\">"
	m6r = "<Method name=\"Stochastic (Direct method)\" type=\"Stochastic\">"

	# new parameters for SSA method, to add
	m7 = "        <Parameter name=\"Max Internal Steps\" type=\"integer\" value=\"1000000\"/>\n"
	m8 = "        <Parameter name=\"Use Random Seed\" type=\"bool\" value=\"0\"/>\n"
	m9 = "        <Parameter name=\"Random Seed\" type=\"unsignedInteger\" value=\"1\"/>\n"

	look_for_end_Method = False

	for line in cps_file :

		# this bit skips over all the LSODA parameters and does not copy them
		if look_for_end_Method :
			if "</Method>" not in line :
				continue
			else :
				look_for_end_Method = False  # found it

		if m1 in line :
			cps_file_executable.write(line.replace(m1, m1r))
		elif m2 in line :
			cps_file_executable.write(line.replace(m2, m2r))
		elif m3 in line :
			cps_file_executable.write(line.replace(m3, m3r))
		elif m4 in line :			
			cps_file_executable.write(line.replace(m4, m4r))
		elif m5 in line :			
			cps_file_executable.write(line.replace(m5, m5r))
		elif m6 in line :
			cps_file_executable.write(line.replace(m6, m6r))
			# write other parameter lines for stochastic method
			cps_file_executable.write(m7)
			cps_file_executable.write(m8)
			cps_file_executable.write(m9)
			look_for_end_Method = True
		else :
			cps_file_executable.write(line)		# leave the line alone		

	cps_file.close()
	cps_file_executable.close()

	# replace unmodified model file with modified model file
	cmd = "rm -rf %s; mv %s %s" % (cps_filename, cps_filepath_executable, cps_filename)
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, universal_newlines=True)
	(output, err) = p.communicate()




def execute(config, cps_filename) :

	cmd = "%s %s" % (config["COPASI_PATH"], cps_filename)
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, universal_newlines=True)
	(output, err) = p.communicate()




#
# ODE model of the full circadian rhythm model, Rust et al. 2007 Science paper.
# 
#
# Ben Shirt-Ediss
#
# May 2017

import numpy
import math
from scipy.integrate import ode
import matplotlib.pyplot as plt


# Model parameters from Table S2 of Supplementary Material

# ------------- Time Interval -----------

t_start = 0
t_end = 90			# hours!

num_steps = 10000

# ------------- Kinetic Rates -----------
# Unit: h^-1

# Basal rates

k_UT_0	=	0
k_TD_0 	=	0
k_SD_0 	= 	0
k_US_0 	= 	0
k_TU_0 	=	0.21
k_DT_0  =	0
k_DS_0  =	0.31
k_SU_0 	=	0.11

# Maximal rates, when KaiA is present

k_UT_A	=	0.479077
k_TD_A 	=	0.212923
k_SD_A 	= 	0.505692
k_US_A 	= 	0.0532308
k_TU_A 	=	0.0798462
k_DT_A  =	0.1730000
k_DS_A  =	-0.319385
k_SU_A 	=	-0.133077 		# xxx I have not seen negative rate constants before!
							# it effectively makes reaction one way, adding to the forward reaction rate

# ------------- Initial Concentrations -----------
# Unit: uM

# But unit does not matter, since all reactions are unimolecular with no
# concentration unit -- so conc can be in any unit.
# All we want is the percentage concs

T0 = 0.68
D0 = 1.36
S0 = 0.34


# ------------- Total Concentrations -----------
# Unit: uM

A_tot = 1.3		# total concentration of KaiA
C_tot = 3.4 	# total concentration of all forms of KaiC (conserved quantity)

K_half = 0.43	# Michaelis-Menten constant
				# Concentration of KaiA causing half-maximal effect on KaiC







def k(k_basal, k_maximal, p) :

	[S, A, K_half] = p

	return k_basal + ( (k_maximal * A) / (K_half + A) )
	





def ode_set(t, state, odeparams) :

	# unpack state variables
	T, D, S = state

	# unpack parameters
	[C_tot, A_tot, K_half, k_UT_0, k_TD_0, k_SD_0, k_US_0, k_TU_0, k_DT_0, k_DS_0, k_SU_0, k_UT_A, k_TD_A, k_SD_A, k_US_A, k_TU_A, k_DT_A, k_DS_A, k_SU_A] = odeparams

	# calc U conc
	U = C_tot - S - D - T

	# calc KaiA conc
	A = max([0, A_tot - (2 * S)])

	# package up constant variables to send into each rate function, to make derivatives below easier to read
	p = [S, A, K_half]

	# 3 derivatives
	dT__dt = ( k(k_UT_0, k_UT_A, p) * U ) + ( k(k_DT_0, k_DT_A, p) * D) - ( k(k_TU_0, k_TU_A, p) * T ) - ( k(k_TD_0, k_TD_A, p) * T )

	dD__dt = ( k(k_TD_0, k_TD_A, p) * T ) + ( k(k_SD_0, k_SD_A, p) * S ) - ( k(k_DT_0, k_DT_A, p) * D ) - ( k(k_DS_0, k_DS_A, p) * D )

	dS__dt = ( k(k_DS_0, k_DS_A, p) * D ) + ( k(k_US_0, k_US_A, p) * U ) - ( k(k_SD_0, k_SD_A, p) * S ) - ( k(k_SU_0, k_SU_A, p) * S )

	return [dT__dt, dD__dt, dS__dt]






def integrate(ode_set, state0, time, odeparams) :

	yy = []
	yy.append(state0)
	t0 = time[0]

	solver = ode(ode_set)
	solver.set_integrator("lsoda")  
	solver.set_initial_value(state0, t0)
	solver.set_f_params(odeparams)
	
	for t in time[1:] :
		yy.append(solver.integrate(t))
		if not solver.successful() :
			print("Warning. There are some integration errors.")

	return yy



# for getting columns out of yy
def get_state_variable(table, column_num) :
	"""
	If a table is represented as a list of equal sized row lists,
	this extracts a column of that table, as a list
	"""
	return [ row[column_num] for row in table ]




def run() :

	# time range
	time = numpy.linspace(t_start, t_end, num_steps)


	#
	# PART 1: Show that model works as described in Science paper, Fig 4B
	#
	#
	#


	plt.figure(1)

	# rate parameters
	odeparams = [C_tot, A_tot, K_half, k_UT_0, k_TD_0, k_SD_0, k_US_0, k_TU_0, k_DT_0, k_DS_0, k_SU_0, k_UT_A, k_TD_A, k_SD_A, k_US_A, k_TU_A, k_DT_A, k_DS_A, k_SU_A]

	# initial state
	state0 = [T0, D0, S0]

	# Euler
	yy = integrate(ode_set, state0, time, odeparams)

	# plot output
	T = get_state_variable(yy, 0)
	D = get_state_variable(yy, 1)
	S = get_state_variable(yy, 2)
	totTDS = []		# the overall conc of PHOSPHORYLATED FORMS of KaiC (e.g. T, D and S)

	for n in range(0, len(T)) :
		totTDS.append(T[n] + D[n] + S[n])

	# to get percentages of each phosphoform, rather than absolute concs,
	# T, D, S and totTDS need dividing by the TOTAL amount of KaiC including the U form, C_tot
	percentT = [ (t / C_tot) * 100 for t in T ] 
	percentD = [ (d / C_tot) * 100 for d in D ] 
	percentS = [ (s / C_tot) * 100 for s in S ] 
	percent_totTDS = [ (tds / C_tot) * 100 for tds in totTDS ]
	
	plt.plot(time, percent_totTDS, 'k--', linewidth=3)
	plt.plot(time, percentT, 'g', linewidth=3)
	plt.plot(time, percentD, 'b', linewidth=3)
	plt.plot(time, percentS, 'r', linewidth=3)


	plt.xlabel("Time, hours")
	plt.ylabel("% total KaiC conc")
	plt.xlim([t_start, t_end])
	plt.title("Fig 4B Rust 2007: Circadian Oscillator")








	#
	# PART 2: Show that a five-fold excess injection of KaiA at about 33 hours leads to the behaviour reported
	# in Figure 3B of Rust 2007
	#


	# time now split into 2 phases
	# in the model output, perturbation should be applied at about 30 hours (not 33 hours)
	# to match the same down part of the curve of the experimental result -- model oscillations run faster than experimental ones
	time1 = numpy.linspace(0, 30, num_steps)
	time2 = numpy.linspace(30, 40, num_steps)

	plt.figure(2)

	# ---- PHASE 1 ---- 
	# rate parameters
	odeparams = [C_tot, A_tot, K_half, k_UT_0, k_TD_0, k_SD_0, k_US_0, k_TU_0, k_DT_0, k_DS_0, k_SU_0, k_UT_A, k_TD_A, k_SD_A, k_US_A, k_TU_A, k_DT_A, k_DS_A, k_SU_A]

	# initial state
	state0 = [T0, D0, S0]

	# Euler to 34 hours
	yy1 = integrate(ode_set, state0, time1, odeparams)


	# ---- PHASE 2 ---- 
	# rate parameters, 5x excess of KaiA is added to the system
	odeparams = [C_tot, A_tot*5, K_half, k_UT_0, k_TD_0, k_SD_0, k_US_0, k_TU_0, k_DT_0, k_DS_0, k_SU_0, k_UT_A, k_TD_A, k_SD_A, k_US_A, k_TU_A, k_DT_A, k_DS_A, k_SU_A]

	# state0 is the same as end state of phase 1
	state0 = yy1[-1]

	# Euler to 40 hours
	yy2 = integrate(ode_set, state0, time2, odeparams)


	# plot output ------------------------
	T = get_state_variable(yy1, 0)
	D = get_state_variable(yy1, 1)
	S = get_state_variable(yy1, 2)
	totTDS = []		# the overall conc of PHOSPHORYLATED FORMS of KaiC (e.g. T, D and S)

	for n in range(0, len(T)) :
		totTDS.append(T[n] + D[n] + S[n])

	percent_totTDS = [ (tds / C_tot) * 100 for tds in totTDS ]
	
	plt.plot(time1, percent_totTDS, 'k--', linewidth=3)


	T = get_state_variable(yy2, 0)
	D = get_state_variable(yy2, 1)
	S = get_state_variable(yy2, 2)
	totTDS2 = []

	for n in range(0, len(T)) :
		totTDS2.append(T[n] + D[n] + S[n])

	percent_totTDS = [ (tds / C_tot) * 100 for tds in totTDS2 ]

	plt.plot(time2, percent_totTDS, 'r', linewidth=3)

	plt.xlabel("Time, hours")
	plt.ylabel("% total KaiC conc")
	plt.xlim([0, 40])
	plt.title("Fig 3B Rust 2007: Excess injection of KaiA")

	











	#
	# PART 3: Show that model this model has a problem: its sensitive to concentration scaling
	# (Fig S8B of Rust 2007 supplementary material)
	#
	#

	plt.figure(3)

	colours = ['k','b','g','r']
	i = 0

	for initial_conc_multiplier in [0.5, 1.0, 2.0, 4.0] :

		# Reproduces Fig S8b of Rust 2007 supplementary
		# when initial and total KaiC protein concs are multiplied by a constant, AND the KaiA total protein conc is multiplied by the same constant 

		# rate parameters
		odeparams = [C_tot * initial_conc_multiplier, A_tot * initial_conc_multiplier, K_half, k_UT_0, k_TD_0, k_SD_0, k_US_0, k_TU_0, k_DT_0, k_DS_0, k_SU_0, k_UT_A, k_TD_A, k_SD_A, k_US_A, k_TU_A, k_DT_A, k_DS_A, k_SU_A]

		# initial state
		state0 = [T0 * initial_conc_multiplier, D0 * initial_conc_multiplier, S0 * initial_conc_multiplier]

		# Euler
		yy = integrate(ode_set, state0, time, odeparams)

		# plot output
		T = get_state_variable(yy, 0)
		D = get_state_variable(yy, 1)
		S = get_state_variable(yy, 2)
		totTDS = []		# the overall conc of PHOSPHORYLATED FORMS of KaiC (e.g. T, D and S)

		for n in range(0, len(T)) :
			totTDS.append(T[n] + D[n] + S[n])

		percent_totTDS = [ ( tds / (C_tot * initial_conc_multiplier) ) * 100 for tds in totTDS ]
		
		plt.plot(time, percent_totTDS, colours[i], linewidth=3)
		i = i + 1

	plt.xlabel("Time, hours")
	plt.ylabel("% total KaiC conc")
	plt.xlim([t_start, t_end])
	plt.title("Fig S8B Rust 2007: Oscillator sensitive to absolute protein concs")

	plt.show()





if __name__ == "__main__" :
	run()



#
#
# ODE model of the linear phosphoform model in Fig 2c of Rust et al. 2007 Science paper.
#
# Ben Shirt-Ediss
#
# March 2017

import numpy
import math
from scipy.integrate import ode
import matplotlib.pyplot as plt



KaiA = True

	# set to False, will reproduce Figure 2B
	# where ALREADY phosphorylated KaiC de-phosphorylates (no KaiA or KaiB present)

	# set to True, will reproduce Figure 2A, 
	# where unphosphorylated KaiC phosphorylates in the presence of KaiA



# ------------- Time Interval -----------

t_start = 0
t_end = 21			# hours!

num_steps = 10000

# ------------- Kinetic Rates -----------
# Unit: h^-1

# default rates with no KaiA present

k__u_t = 0
k__t_u = 0.21

k__u_s = 0
k__s_u = 0.11

k__t_st = 0
k__st_t = 0

k__s_st = 0
k__st_s = 0.31


if KaiA == True :

	# 1.3uM KaiA present

	k__u_t = 0.36
	k__t_u = 0.27

	k__u_s = 0.04
	k__s_u = 0.01

	k__t_st = 0.16
	k__st_t = 0.13

	k__s_st = 0.38
	k__st_s = 0.07

# ------------- Initial Concentrations -----------
# Unit: M

# But does not matter, since all reactions are unimolecular with no
# concentration unit -- so conc can be in any unit.
# All we want is the percentage concs

# Fig 2.B. Graph starts with 70% KaiC phosphorylated,
# which is what the below numbers add up to
U0 =    0
S0 = 	0.15
T0 =    0.21
ST0 =   0.32

if KaiA == True :
	# Fig 2.A. Graph starts with 0% KaiC phosphorylated.
	# i.e. 100% of KaiC is unphosphorylated
	U0 = 	1
	S0 = 	0
	T0 = 	0
	ST0 = 	0














def ode_set(t, state, odeparams) :

	# unpack state variables
	U, S, T, N0 = state

	ST = N0 - U - S - T

	# unpack parameters
	k__u_t, k__t_u, k__u_s, k__s_u, k__t_st, k__st_t, k__s_st, k__st_s = odeparams

	# 3 derivatives
	dU__dt = -(k__u_t * U) + (k__t_u * T) - (k__u_s * U) + (k__s_u * S)

	dS__dt = (k__u_s * U) - (k__s_u * S) - (k__s_st * S) + (k__st_s * ST)

	dT__dt = (k__u_t * U) - (k__t_u * T) - (k__t_st * T) + (k__st_t * ST)

	dN0__dt = 0

	return [dU__dt, dS__dt, dT__dt, dN0__dt]




def integrate(ode_set, state0, time, odeparams) :

	yy = []
	yy.append(state0)
	t0 = time[0]

	solver = ode(ode_set)
	solver.set_integrator("lsoda")  
	solver.set_initial_value(state0, t0)
	solver.set_f_params(odeparams)
	
	for t in time[1:] :
		yy.append(solver.integrate(t))
		if not solver.successful() :
			print("Warning. There are some integration errors.")

	return yy



# for getting columns out of yy
def get_state_variable(table, column_num) :
	"""
	If a table is represented as a list of equal sized row lists,
	this extracts a column of that table, as a list
	"""
	return [ row[column_num] for row in table ]




def run() :

	# time range
	time = numpy.linspace(t_start, t_end, num_steps)

	# rate parameters
	odeparams = [k__u_t, k__t_u, k__u_s, k__s_u, k__t_st, k__st_t, k__s_st, k__st_s]

	# initial state
	N0 = U0 + S0 + T0 + ST0 		# total conserved concentration in the closed system
	state0 = [U0, S0, T0, N0]		# N0 is an unchanging state variable, so ST can be calculated in ode_set function

	# Euler
	yy = integrate(ode_set, state0, time, odeparams)

	# plot output
	U = get_state_variable(yy, 0)
	S = get_state_variable(yy, 1)
	T = get_state_variable(yy, 2)
	ST = []
	total = []		# the total conc of PHOSPHORYLATED FORMS of KaiC

	for n in range(0, len(U)) :
		ST.append(N0 - U[n] - S[n] - T[n])
		total.append(N0 - U[n])

	plt.figure()
	plt.plot(time, total, 'k')
	plt.plot(time, T, 'g')
	plt.plot(time, ST, 'b')
	plt.plot(time, S, 'r')
	plt.xlabel("Time, hours")
	plt.ylabel("% KaiC")
	plt.xlim([0, 21])

	if KaiA == True :
		plt.title("Fig 2a Rust 2007. PHOSPHORYLATION of KaiC in presence of KaiA")
	else :
		plt.title("Fig 2b Rust 2007. DE-PHOSPHORYLATION of KaiC (KaiA nor KaiB present)")


	plt.show()


if __name__ == "__main__" :
	run()



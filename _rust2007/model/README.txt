
To run the full model of circadian oscillations

	$ python3 full_model.py

		(generates 3 plots, reproducing different figures in Rust 2007 paper)


To run the partial model of KaiC phosphoforms inter-converting (does not oscillate, as there is no KaiA and KaiB)

	$ python3 linear_model.py
<?xml version="1.0" encoding="UTF-8"?>
<!-- generated with COPASI 4.29 (Build 228) (http://www.copasi.org) at 2021-02-04T18:42:26Z -->
<?oxygen RNGSchema="http://www.copasi.org/static/schema/CopasiML.rng" type="xml"?>
<COPASI xmlns="http://www.copasi.org/static/schema" versionMajor="4" versionMinor="29" versionDevel="228" copasiSourcesModified="0">
  <ListOfFunctions>
    <Function key="Function_13" name="Mass action (irreversible)" type="MassAction" reversible="false">
      <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
   <rdf:Description rdf:about="#Function_13">
   <CopasiMT:is rdf:resource="urn:miriam:obo.sbo:SBO:0000163" />
   </rdf:Description>
   </rdf:RDF>
      </MiriamAnnotation>
      <Comment>
        <body xmlns="http://www.w3.org/1999/xhtml">
<b>Mass action rate law for irreversible reactions</b>
<p>
Reaction scheme where the products are created from the reactants and the change of a product quantity is proportional to the product of reactant activities. The reaction scheme does not include any reverse process that creates the reactants from the products. The change of a product quantity is proportional to the quantity of one reactant.
</p>
</body>
      </Comment>
      <Expression>
        k1*PRODUCT&lt;substrate_i>
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_80" name="k1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_81" name="substrate" order="1" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
    <Function key="Function_14" name="Mass action (reversible)" type="MassAction" reversible="true">
      <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
   <rdf:Description rdf:about="#Function_14">
   <CopasiMT:is rdf:resource="urn:miriam:obo.sbo:SBO:0000042" />
   </rdf:Description>
   </rdf:RDF>
      </MiriamAnnotation>
      <Comment>
        <body xmlns="http://www.w3.org/1999/xhtml">
<b>Mass action rate law for reversible reactions</b>
<p>
Reaction scheme where the products are created from the reactants and the change of a product quantity is proportional to the product of reactant activities. The reaction scheme does include a reverse process that creates the reactants from the products.
</p>
</body>
      </Comment>
      <Expression>
        k1*PRODUCT&lt;substrate_i>-k2*PRODUCT&lt;product_j>
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_69" name="k1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_68" name="substrate" order="1" role="substrate"/>
        <ParameterDescription key="FunctionParameter_78" name="k2" order="2" role="constant"/>
        <ParameterDescription key="FunctionParameter_79" name="product" order="3" role="product"/>
      </ListOfParameterDescriptions>
    </Function>
  </ListOfFunctions>
  <Model key="Model_1" name="Van Zon 2007 PTO Model - Circadian Clock" simulationType="time" timeUnit="h" volumeUnit="l" areaUnit="m²" lengthUnit="m" quantityUnit="mol" type="deterministic" avogadroConstant="6.0221408570000002e+23">
    <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Model_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:31:56Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

    </MiriamAnnotation>
    <ListOfCompartments>
      <Compartment key="Compartment_0" name="compartment" simulationType="fixed" dimensionality="3" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Compartment_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:35:22Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Compartment>
    </ListOfCompartments>
    <ListOfMetabolites>
      <Metabolite key="Metabolite_0" name="C0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:35:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_1" name="iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:36:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_2" name="C1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:38:38Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_3" name="iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_1">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:38Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_4" name="C2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:38:44Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_5" name="iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_4">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:44Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_6" name="C3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_6">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:49Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_7" name="iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_6">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:49Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_8" name="C4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_8">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:54Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_9" name="iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_8">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:54Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_10" name="C5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_10">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:59Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_11" name="iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_10">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:38:59Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_12" name="C6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:39:03Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_13" name="iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T15:39:03Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_14" name="A" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:50:42Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_15" name="AC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_15">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:50:49Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_16" name="AC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:54:42Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_17" name="AC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:54:53Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_18" name="AC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_18">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:55:47Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_19" name="AC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:55:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_20" name="AC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:56:02Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_21" name="AC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_21">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T16:56:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_22" name="B2iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:24:04Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_23" name="B2iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:24:46Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_24" name="B2iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_24">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:02Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_25" name="B2iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_26" name="B2iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:36Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_27" name="B2iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_28" name="B2iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:26:19Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_29" name="B" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:28:03Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_30" name="A2B2iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_30">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T17:53:17Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_31" name="A2B2iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Metabolite_31">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-03T17:54:56Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_32" name="A2B2iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:55:01Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_33" name="A2B2iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_33">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:55:06Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_34" name="A2B2iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:55:11Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_35" name="A2B2iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_35">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:55:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_36" name="A2B2iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:55:23Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
    </ListOfMetabolites>
    <ListOfModelValues>
      <ModelValue key="ModelValue_0" name="f0" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_1" name="f1" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:47Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_2" name="f2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_3" name="f3" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:50Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_4" name="f4" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:51Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_5" name="f5" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:41:53Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_6" name="f6" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:42:05Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_7" name="bi" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:42:20Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_8" name="ki_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:55:05Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
      </ModelValue>
      <ModelValue key="ModelValue_9" name="k0_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_9">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:55:30Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_10" name="k1_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_10">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:14Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_11" name="k2_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_11">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:18Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_12" name="k3_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_12">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:20Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_13" name="k4_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_13">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:23Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_14" name="k5_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:26Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_15" name="k6_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_15">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:30Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_16" name="k_pf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:57:43Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_17" name="ik0_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:34:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_18" name="ik0_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_18">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:34:49Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_19" name="ik1_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:02Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_20" name="ik1_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:06Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_21" name="ik2_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_21">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:09Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_22" name="ik2_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_22">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:13Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_23" name="ik3_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_24" name="ik3_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_24">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:19Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_25" name="ik4_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_26" name="ik4_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_27" name="ik5_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:28Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_28" name="ik5_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_29" name="ik6_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:33Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_30" name="ik6_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_30">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:35:37Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_31" name="ik0_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_31">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:09Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_32" name="ik1_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:12Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_33" name="ik2_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_33">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:15Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_34" name="ik3_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_35" name="ik4_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_35">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:20Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_36" name="ik5_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:23Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_37" name="ik6_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_37">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:27Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l^2 / (mol^2 * h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_38" name="iki_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_38">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:58:38Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_39" name="k_ps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_39">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:15:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_40" name="k_dps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:15:30Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_41" name="ik_ps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_41">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:23:24Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_42" name="ik_dps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelValue_42">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:23:27Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
    </ListOfModelValues>
    <ListOfReactions>
      <Reaction key="Reaction_0" name="R1_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:34:21Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5366" name="k1" value="1e-05"/>
          <Constant key="Parameter_5355" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_2" name="R1_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:37:22Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5364" name="k1" value="1e-05"/>
          <Constant key="Parameter_5353" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_4" name="R1_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:37:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5362" name="k1" value="0.0001"/>
          <Constant key="Parameter_5357" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_6" name="R1_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:37:38Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5360" name="k1" value="0.001"/>
          <Constant key="Parameter_5359" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_8" name="R1_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:37:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5358" name="k1" value="0.01"/>
          <Constant key="Parameter_5361" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_10" name="R1_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_10">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:37:53Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5356" name="k1" value="0.1"/>
          <Constant key="Parameter_5363" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_12" name="R1_6" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_12">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:38:05Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_12" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5354" name="k1" value="10"/>
          <Constant key="Parameter_5365" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_12"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_14" name="R2_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:50:31Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5352" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5384" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_0"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_9"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_16" name="R2_0irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:51:35Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5675" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_17" name="R2_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:05Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5676" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5386" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_2"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_10"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_19" name="R2_1irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:11Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_4" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5677" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_20" name="R2_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5704" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5345" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_4"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_11"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_22" name="R2_2irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_22">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:23Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5703" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_23" name="R2_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5706" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5348" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_6"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_12"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_25" name="R2_3irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:41Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5689" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_26" name="R2_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5687" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5690" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_8"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_13"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_28" name="R2_4irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:52:55Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5350" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_29" name="R2_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:53:17Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5349" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5688" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_10"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_20"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_31" name="R2_5irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_31">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:53:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_12" stoichiometry="1"/>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5347" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_20"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_32" name="R2_6" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T15:53:37Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_12" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5346" name="k1" value="1.72e+08"/>
          <Constant key="Parameter_5705" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_12"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_15"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_21"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_34" name="R3_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:23:33Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5344" name="k1" value="2.97e+10"/>
          <Constant key="Parameter_5351" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_17"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_1"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_18"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_36" name="R3_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:24:36Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5342" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5674" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_19"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_3"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_20"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_38" name="R3_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_38">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:24:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5340" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5645" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_21"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_5"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_22"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_40" name="R3_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:09Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_7" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5338" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5644" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_23"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_7"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_24"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_42" name="R3_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_42">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:28Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_9" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5336" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5337" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_25"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_9"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_26"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_44" name="R3_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_44">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:25:45Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5643" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5335" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_27"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_11"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_28"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_46" name="R3_6" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_46">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:26:09Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_29" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5642" name="k1" value="2.97e+12"/>
          <Constant key="Parameter_5339" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_13"/>
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_30"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_48" name="R4_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_48">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:52:49Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5343" name="k2" value="0.1"/>
          <Constant key="Parameter_5334" name="k1" value="0"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_22"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_50" name="R4_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_50">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:53:28Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5332" name="k1" value="2.97e+18"/>
          <Constant key="Parameter_5321" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_32"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_23"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_52" name="R4_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_52">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:53:35Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5330" name="k1" value="2.97e+20"/>
          <Constant key="Parameter_5341" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_33"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_24"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_54" name="R4_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_54">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:53:41Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5328" name="k1" value="2.97e+20"/>
          <Constant key="Parameter_5323" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_34"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_25"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_56" name="R4_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_56">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:53:48Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5326" name="k1" value="2.97e+18"/>
          <Constant key="Parameter_5327" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_35"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_26"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_58" name="R4_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_58">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:53:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5324" name="k1" value="0"/>
          <Constant key="Parameter_5325" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_36"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_27"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_60" name="R4_6" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_60">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T17:54:01Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_28" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_14" stoichiometry="2"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5322" name="k1" value="0"/>
          <Constant key="Parameter_5329" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_37"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_28"/>
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_36"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_62" name="R5_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_62">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:10:52Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5320" name="k1" value="0.025"/>
          <Constant key="Parameter_5333" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_64" name="R5_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_64">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:11:04Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5318" name="k1" value="0.025"/>
          <Constant key="Parameter_5331" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_66" name="R5_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_66">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:11:09Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5316" name="k1" value="0.025"/>
          <Constant key="Parameter_5304" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_68" name="R5_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_68">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:11:14Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5314" name="k1" value="0.025"/>
          <Constant key="Parameter_5306" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_8"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_70" name="R5_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_70">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:11:19Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5307" name="k1" value="0.025"/>
          <Constant key="Parameter_5308" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_72" name="R5_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_72">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:11:25Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_12" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5305" name="k1" value="0.025"/>
          <Constant key="Parameter_5315" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_12"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_74" name="R6_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_74">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:20:34Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5303" name="k1" value="0.025"/>
          <Constant key="Parameter_5317" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_76" name="R6_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_76">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:20:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5301" name="k1" value="0.025"/>
          <Constant key="Parameter_5319" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_78" name="R6_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_78">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:20:45Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5299" name="k1" value="0.025"/>
          <Constant key="Parameter_5292" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_80" name="R6_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_80">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:20:50Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5297" name="k1" value="0.025"/>
          <Constant key="Parameter_5294" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_82" name="R6_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_82">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:21:04Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5295" name="k1" value="0.025"/>
          <Constant key="Parameter_5296" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_84" name="R6_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_84">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:21:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5293" name="k1" value="0.025"/>
          <Constant key="Parameter_5298" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_86" name="R7_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_86">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:28:47Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5291" name="k1" value="0.025"/>
          <Constant key="Parameter_5300" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_88" name="R7_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_88">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:30:43Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5289" name="k1" value="0.025"/>
          <Constant key="Parameter_5302" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_90" name="R7_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_90">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:30:58Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5287" name="k1" value="0.025"/>
          <Constant key="Parameter_5280" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_92" name="R7_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_92">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:31:03Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5285" name="k1" value="0.025"/>
          <Constant key="Parameter_5282" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_94" name="R7_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_94">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:31:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5283" name="k1" value="0.025"/>
          <Constant key="Parameter_5284" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_96" name="R7_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_96">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:31:16Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5281" name="k1" value="0.025"/>
          <Constant key="Parameter_5286" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_98" name="R8_0" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_98">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:37:23Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5279" name="k1" value="0.025"/>
          <Constant key="Parameter_5270" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_100" name="R8_1" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_100">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:37:32Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5277" name="k1" value="0.025"/>
          <Constant key="Parameter_5268" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_102" name="R8_2" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_102">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:37:40Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5275" name="k1" value="0.025"/>
          <Constant key="Parameter_5272" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_104" name="R8_3" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_104">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:37:50Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5273" name="k1" value="0.025"/>
          <Constant key="Parameter_5274" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_106" name="R8_4" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_106">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:37:56Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5271" name="k1" value="0.025"/>
          <Constant key="Parameter_5276" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_108" name="R8_5" reversible="true" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_108">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-03T18:38:04Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5269" name="k1" value="0.025"/>
          <Constant key="Parameter_5278" name="k2" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_14" unitType="Default" scalingCompartment="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_69">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_68">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_78">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_79">
              <SourceParameter reference="Metabolite_36"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
    </ListOfReactions>
    <ListOfModelParameterSets activeSet="ModelParameterSet_1">
      <ModelParameterSet key="ModelParameterSet_1" name="Initial State">
        <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelParameterSet_1">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:54Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]" value="1" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B]" value="1.0538746499750029e+18" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6]" value="1000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab]" value="30" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab]" value="90" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab]" value="270" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab]" value="810" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab]" value="2430" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab]" value="7290" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf]" value="29700000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb]" value="1000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k1" value="1000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k2" value="30" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k2" value="90" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k2" value="270" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k2" value="810" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k2" value="2430" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k2" value="7290" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k1" value="29700000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k2" value="1000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
      <ModelParameterSet key="ModelParameterSet_0" name="Original parameters">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelParameterSet_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-04T17:23:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]" value="1" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B]" value="1.0538746499750029e+18" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0]" value="1.0000000000000001e-05" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1]" value="1.0000000000000001e-05" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2]" value="0.0001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3]" value="0.001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab]" value="30" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab]" value="90" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab]" value="270" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab]" value="810" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab]" value="2430" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab]" value="7290" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf]" value="29700000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb]" value="1000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k1" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k1" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k1" value="0.0001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k1" value="0.001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k2" value="30" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k2" value="90" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k2" value="270" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k2" value="810" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k2" value="2430" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k2" value="7290" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k1" value="29700000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k2" value="1000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
      <ModelParameterSet key="ModelParameterSet_2" name="Increase fi flip rates">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelParameterSet_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-04T17:23:54Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment]" value="1" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A]" value="3.4928416970600326e+17" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B]" value="1.0538746499750029e+18" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6]" value="1000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab]" value="30" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab]" value="90" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab]" value="270" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab]" value="810" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab]" value="2430" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab]" value="7290" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf]" value="29700000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb]" value="1000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf]" value="2970000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af]" value="2.97e+20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af]" value="2.97e+18" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af]" value="0" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_0],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k1" value="1000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R1_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0],ParameterGroup=Parameters,Parameter=k2" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1],ParameterGroup=Parameters,Parameter=k2" value="30" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2],ParameterGroup=Parameters,Parameter=k2" value="90" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3],ParameterGroup=Parameters,Parameter=k2" value="270" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4],ParameterGroup=Parameters,Parameter=k2" value="810" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5],ParameterGroup=Parameters,Parameter=k2" value="2430" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R2_6],ParameterGroup=Parameters,Parameter=k2" value="7290" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k1" value="29700000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_0],ParameterGroup=Parameters,Parameter=k2" value="1000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k1" value="2970000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R3_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_0],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_1],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_2],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k1" value="2.97e+20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_3],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k1" value="2.97e+18" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_4],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_5],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k1" value="0" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R4_6],ParameterGroup=Parameters,Parameter=k2" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[iki_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R5_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R6_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R7_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_0],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_1],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_2],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_3],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_4],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
            <ModelParameter cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Reactions[R8_5],ParameterGroup=Parameters,Parameter=k2" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
    </ListOfModelParameterSets>
    <StateTemplate>
      <StateTemplateVariable objectReference="Model_1"/>
      <StateTemplateVariable objectReference="Metabolite_14"/>
      <StateTemplateVariable objectReference="Metabolite_29"/>
      <StateTemplateVariable objectReference="Metabolite_2"/>
      <StateTemplateVariable objectReference="Metabolite_6"/>
      <StateTemplateVariable objectReference="Metabolite_10"/>
      <StateTemplateVariable objectReference="Metabolite_8"/>
      <StateTemplateVariable objectReference="Metabolite_4"/>
      <StateTemplateVariable objectReference="Metabolite_23"/>
      <StateTemplateVariable objectReference="Metabolite_25"/>
      <StateTemplateVariable objectReference="Metabolite_27"/>
      <StateTemplateVariable objectReference="Metabolite_5"/>
      <StateTemplateVariable objectReference="Metabolite_9"/>
      <StateTemplateVariable objectReference="Metabolite_12"/>
      <StateTemplateVariable objectReference="Metabolite_3"/>
      <StateTemplateVariable objectReference="Metabolite_11"/>
      <StateTemplateVariable objectReference="Metabolite_32"/>
      <StateTemplateVariable objectReference="Metabolite_34"/>
      <StateTemplateVariable objectReference="Metabolite_0"/>
      <StateTemplateVariable objectReference="Metabolite_7"/>
      <StateTemplateVariable objectReference="Metabolite_35"/>
      <StateTemplateVariable objectReference="Metabolite_31"/>
      <StateTemplateVariable objectReference="Metabolite_22"/>
      <StateTemplateVariable objectReference="Metabolite_28"/>
      <StateTemplateVariable objectReference="Metabolite_24"/>
      <StateTemplateVariable objectReference="Metabolite_13"/>
      <StateTemplateVariable objectReference="Metabolite_16"/>
      <StateTemplateVariable objectReference="Metabolite_18"/>
      <StateTemplateVariable objectReference="Metabolite_33"/>
      <StateTemplateVariable objectReference="Metabolite_20"/>
      <StateTemplateVariable objectReference="Metabolite_17"/>
      <StateTemplateVariable objectReference="Metabolite_1"/>
      <StateTemplateVariable objectReference="Metabolite_19"/>
      <StateTemplateVariable objectReference="Metabolite_36"/>
      <StateTemplateVariable objectReference="Metabolite_15"/>
      <StateTemplateVariable objectReference="Metabolite_21"/>
      <StateTemplateVariable objectReference="Metabolite_30"/>
      <StateTemplateVariable objectReference="Metabolite_26"/>
      <StateTemplateVariable objectReference="Compartment_0"/>
      <StateTemplateVariable objectReference="ModelValue_0"/>
      <StateTemplateVariable objectReference="ModelValue_1"/>
      <StateTemplateVariable objectReference="ModelValue_2"/>
      <StateTemplateVariable objectReference="ModelValue_3"/>
      <StateTemplateVariable objectReference="ModelValue_4"/>
      <StateTemplateVariable objectReference="ModelValue_5"/>
      <StateTemplateVariable objectReference="ModelValue_6"/>
      <StateTemplateVariable objectReference="ModelValue_7"/>
      <StateTemplateVariable objectReference="ModelValue_8"/>
      <StateTemplateVariable objectReference="ModelValue_9"/>
      <StateTemplateVariable objectReference="ModelValue_10"/>
      <StateTemplateVariable objectReference="ModelValue_11"/>
      <StateTemplateVariable objectReference="ModelValue_12"/>
      <StateTemplateVariable objectReference="ModelValue_13"/>
      <StateTemplateVariable objectReference="ModelValue_14"/>
      <StateTemplateVariable objectReference="ModelValue_15"/>
      <StateTemplateVariable objectReference="ModelValue_16"/>
      <StateTemplateVariable objectReference="ModelValue_17"/>
      <StateTemplateVariable objectReference="ModelValue_18"/>
      <StateTemplateVariable objectReference="ModelValue_19"/>
      <StateTemplateVariable objectReference="ModelValue_20"/>
      <StateTemplateVariable objectReference="ModelValue_21"/>
      <StateTemplateVariable objectReference="ModelValue_22"/>
      <StateTemplateVariable objectReference="ModelValue_23"/>
      <StateTemplateVariable objectReference="ModelValue_24"/>
      <StateTemplateVariable objectReference="ModelValue_25"/>
      <StateTemplateVariable objectReference="ModelValue_26"/>
      <StateTemplateVariable objectReference="ModelValue_27"/>
      <StateTemplateVariable objectReference="ModelValue_28"/>
      <StateTemplateVariable objectReference="ModelValue_29"/>
      <StateTemplateVariable objectReference="ModelValue_30"/>
      <StateTemplateVariable objectReference="ModelValue_31"/>
      <StateTemplateVariable objectReference="ModelValue_32"/>
      <StateTemplateVariable objectReference="ModelValue_33"/>
      <StateTemplateVariable objectReference="ModelValue_34"/>
      <StateTemplateVariable objectReference="ModelValue_35"/>
      <StateTemplateVariable objectReference="ModelValue_36"/>
      <StateTemplateVariable objectReference="ModelValue_37"/>
      <StateTemplateVariable objectReference="ModelValue_38"/>
      <StateTemplateVariable objectReference="ModelValue_39"/>
      <StateTemplateVariable objectReference="ModelValue_40"/>
      <StateTemplateVariable objectReference="ModelValue_41"/>
      <StateTemplateVariable objectReference="ModelValue_42"/>
    </StateTemplate>
    <InitialState type="initialState">
      0 3.4928416970600326e+17 1.0538746499750029e+18 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 3.4928416970600326e+17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.01 0.01 0.10000000000000001 1 10 100 1000 10 172000000 10 30 90 270 810 2430 7290 1 29700000000 1000 2970000000000 100 2970000000000 100 2970000000000 100 2970000000000 100 2970000000000 100 2970000000000 100 0 2.97e+18 2.97e+20 2.97e+20 2.97e+18 0 0 100 0.025000000000000001 0.40000000000000002 0.025000000000000001 0.40000000000000002 
    </InitialState>
  </Model>
  <ListOfTasks>
    <Task key="Task_15" name="Steady-State" type="steadyState" scheduled="false" updateModel="false">
      <Report reference="Report_11" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="JacobianRequested" type="bool" value="1"/>
        <Parameter name="StabilityAnalysisRequested" type="bool" value="1"/>
      </Problem>
      <Method name="Enhanced Newton" type="EnhancedNewton">
        <Parameter name="Resolution" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Derivation Factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Use Newton" type="bool" value="1"/>
        <Parameter name="Use Integration" type="bool" value="1"/>
        <Parameter name="Use Back Integration" type="bool" value="0"/>
        <Parameter name="Accept Negative Concentrations" type="bool" value="0"/>
        <Parameter name="Iteration Limit" type="unsignedInteger" value="50"/>
        <Parameter name="Maximum duration for forward integration" type="unsignedFloat" value="1000000000"/>
        <Parameter name="Maximum duration for backward integration" type="unsignedFloat" value="1000000"/>
        <Parameter name="Target Criterion" type="string" value="Distance and Rate"/>
      </Method>
    </Task>
    <Task key="Task_16" name="Time-Course" type="timeCourse" scheduled="false" updateModel="false">
      <Report reference="Report_12" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100000"/>
        <Parameter name="StepSize" type="float" value="0.00072000000000000005"/>
        <Parameter name="Duration" type="float" value="72"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
      </Problem>
      <Method name="Deterministic (LSODA)" type="Deterministic(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="100000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
    <Task key="Task_17" name="Scan" type="scan" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="Subtask" type="unsignedInteger" value="1"/>
        <ParameterGroup name="ScanItems">
        </ParameterGroup>
        <Parameter name="Output in subtask" type="bool" value="1"/>
        <Parameter name="Adjust initial conditions" type="bool" value="0"/>
        <Parameter name="Continue on Error" type="bool" value="0"/>
      </Problem>
      <Method name="Scan Framework" type="ScanFramework">
      </Method>
    </Task>
    <Task key="Task_18" name="Elementary Flux Modes" type="fluxMode" scheduled="false" updateModel="false">
      <Report reference="Report_13" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="EFM Algorithm" type="EFMAlgorithm">
      </Method>
    </Task>
    <Task key="Task_19" name="Optimization" type="optimization" scheduled="false" updateModel="false">
      <Report reference="Report_14" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Subtask" type="cn" value="CN=Root,Vector=TaskList[Steady-State]"/>
        <ParameterText name="ObjectiveExpression" type="expression">
          
        </ParameterText>
        <Parameter name="Maximize" type="bool" value="0"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
      </Problem>
      <Method name="Random Search" type="RandomSearch">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Iterations" type="unsignedInteger" value="100000"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_20" name="Parameter Estimation" type="parameterFitting" scheduled="false" updateModel="false">
      <Report reference="Report_15" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Maximize" type="bool" value="0"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
        <Parameter name="Steady-State" type="cn" value="CN=Root,Vector=TaskList[Steady-State]"/>
        <Parameter name="Time-Course" type="cn" value="CN=Root,Vector=TaskList[Time-Course]"/>
        <Parameter name="Create Parameter Sets" type="bool" value="0"/>
        <Parameter name="Use Time Sens" type="bool" value="0"/>
        <Parameter name="Time-Sens" type="cn" value=""/>
        <ParameterGroup name="Experiment Set">
        </ParameterGroup>
        <ParameterGroup name="Validation Set">
          <Parameter name="Weight" type="unsignedFloat" value="1"/>
          <Parameter name="Threshold" type="unsignedInteger" value="5"/>
        </ParameterGroup>
      </Problem>
      <Method name="Evolutionary Programming" type="EvolutionaryProgram">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Generations" type="unsignedInteger" value="200"/>
        <Parameter name="Population Size" type="unsignedInteger" value="20"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
        <Parameter name="Stop after # Stalled Generations" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_21" name="Metabolic Control Analysis" type="metabolicControlAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_16" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="MCA Method (Reder)" type="MCAMethod(Reder)">
        <Parameter name="Modulation Factor" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Use Reder" type="bool" value="1"/>
        <Parameter name="Use Smallbone" type="bool" value="1"/>
      </Method>
    </Task>
    <Task key="Task_22" name="Lyapunov Exponents" type="lyapunovExponents" scheduled="false" updateModel="false">
      <Report reference="Report_17" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="ExponentNumber" type="unsignedInteger" value="3"/>
        <Parameter name="DivergenceRequested" type="bool" value="1"/>
        <Parameter name="TransientTime" type="float" value="0"/>
      </Problem>
      <Method name="Wolf Method" type="WolfMethod">
        <Parameter name="Orthonormalization Interval" type="unsignedFloat" value="1"/>
        <Parameter name="Overall time" type="unsignedFloat" value="1000"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
      </Method>
    </Task>
    <Task key="Task_23" name="Time Scale Separation Analysis" type="timeScaleSeparationAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_18" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
      </Problem>
      <Method name="ILDM (LSODA,Deuflhard)" type="TimeScaleSeparation(ILDM,Deuflhard)">
        <Parameter name="Deuflhard Tolerance" type="unsignedFloat" value="0.0001"/>
      </Method>
    </Task>
    <Task key="Task_24" name="Sensitivities" type="sensitivities" scheduled="false" updateModel="false">
      <Report reference="Report_19" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="SubtaskType" type="unsignedInteger" value="1"/>
        <ParameterGroup name="TargetFunctions">
          <Parameter name="SingleObject" type="cn" value=""/>
          <Parameter name="ObjectListType" type="unsignedInteger" value="7"/>
        </ParameterGroup>
        <ParameterGroup name="ListOfVariables">
          <ParameterGroup name="Variables">
            <Parameter name="SingleObject" type="cn" value=""/>
            <Parameter name="ObjectListType" type="unsignedInteger" value="41"/>
          </ParameterGroup>
          <ParameterGroup name="Variables">
            <Parameter name="SingleObject" type="cn" value=""/>
            <Parameter name="ObjectListType" type="unsignedInteger" value="0"/>
          </ParameterGroup>
        </ParameterGroup>
      </Problem>
      <Method name="Sensitivities Method" type="SensitivitiesMethod">
        <Parameter name="Delta factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Delta minimum" type="unsignedFloat" value="9.9999999999999998e-13"/>
      </Method>
    </Task>
    <Task key="Task_25" name="Moieties" type="moieties" scheduled="false" updateModel="false">
      <Report reference="Report_20" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="Householder Reduction" type="Householder">
      </Method>
    </Task>
    <Task key="Task_26" name="Cross Section" type="crosssection" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <Parameter name="LimitCrossings" type="bool" value="0"/>
        <Parameter name="NumCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitOutTime" type="bool" value="0"/>
        <Parameter name="LimitOutCrossings" type="bool" value="0"/>
        <Parameter name="PositiveDirection" type="bool" value="1"/>
        <Parameter name="NumOutCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitUntilConvergence" type="bool" value="0"/>
        <Parameter name="ConvergenceTolerance" type="float" value="9.9999999999999995e-07"/>
        <Parameter name="Threshold" type="float" value="0"/>
        <Parameter name="DelayOutputUntilConvergence" type="bool" value="0"/>
        <Parameter name="OutputConvergenceTolerance" type="float" value="9.9999999999999995e-07"/>
        <ParameterText name="TriggerExpression" type="expression">
          
        </ParameterText>
        <Parameter name="SingleVariable" type="cn" value=""/>
      </Problem>
      <Method name="Deterministic (LSODA)" type="Deterministic(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="100000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
    <Task key="Task_27" name="Linear Noise Approximation" type="linearNoiseApproximation" scheduled="false" updateModel="false">
      <Report reference="Report_21" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="Linear Noise Approximation" type="LinearNoiseApproximation">
      </Method>
    </Task>
    <Task key="Task_28" name="Time-Course Sensitivities" type="timeSensitivities" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <ParameterGroup name="ListOfParameters">
        </ParameterGroup>
        <ParameterGroup name="ListOfTargets">
        </ParameterGroup>
      </Problem>
      <Method name="LSODA Sensitivities" type="Sensitivities(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
  </ListOfTasks>
  <ListOfReports>
    <Report key="Report_11" name="Steady-State" taskType="steadyState" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Steady-State]"/>
      </Footer>
    </Report>
    <Report key="Report_12" name="Time-Course" taskType="timeCourse" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Time-Course],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Time-Course],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_13" name="Elementary Flux Modes" taskType="fluxMode" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Elementary Flux Modes],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_14" name="Optimization" taskType="optimization" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_15" name="Parameter Estimation" taskType="parameterFitting" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_16" name="Metabolic Control Analysis" taskType="metabolicControlAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_17" name="Lyapunov Exponents" taskType="lyapunovExponents" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_18" name="Time Scale Separation Analysis" taskType="timeScaleSeparationAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_19" name="Sensitivities" taskType="sensitivities" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_20" name="Moieties" taskType="moieties" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_21" name="Linear Noise Approximation" taskType="linearNoiseApproximation" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Result"/>
      </Footer>
    </Report>
  </ListOfReports>
  <ListOfPlots>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values 1" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values 2" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values 3" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values 4" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Van Zon 2007 PTO Model - Circadian Clock,Vector=Compartments[compartment],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
  </ListOfPlots>
  <GUI>
  </GUI>
  <ListOfUnitDefinitions>
    <UnitDefinition key="Unit_1" name="meter" symbol="m">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        m
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_5" name="second" symbol="s">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_4">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        s
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_13" name="Avogadro" symbol="Avogadro">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_15" name="dimensionless" symbol="1">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_14">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        1
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_17" name="item" symbol="#">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_16">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        #
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_35" name="liter" symbol="l">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_34">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        0.001*m^3
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_41" name="mole" symbol="mol">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_40">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro*#
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_67" name="hour" symbol="h">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_66">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-04T17:23:50Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        3600*s
      </Expression>
    </UnitDefinition>
  </ListOfUnitDefinitions>
</COPASI>

"""

Reproducing Fig 2 of van Zon

Note, the p fraction does not go down to 0.2 like theirs
But the free KaiA conc fits, and the oscillations are 24 hours.

Make the # Time  = #Time on the first line of .txt exported by COPASI

"""

import csv
import matplotlib.pyplot as plt


def read_csv() :

	variables = {}
	values = {}

	with open('da2.txt') as csv_file:
		csv_reader = csv.reader(csv_file, delimiter='\t')
		line_count = 0
		for row in csv_reader:
			if line_count == 0:
				# make row headings the keys of the dictionary
				for i, heading in enumerate(row) :
					variables[i] = str(heading)
					values[i] = []

				line_count += 1
			else:
				# add data to dictionary
				for i, val in enumerate(row) :
					if val == "" :
						values[i].append(0)
					else :
						values[i].append(float(val))

				line_count += 1

	return variables, values



def find_species_index(species, variables) :

	for v in variables :
		if variables[v] == species :
			return v

	return -1



def calc_A_div_AT(variables, values, AT) :

	Aconc = values[find_species_index("[A]", variables)]

	Aconc_normalised = []
	for A in Aconc :
		Aconc_normalised.append(A / AT)

	return Aconc_normalised




def calc_p(variables, values, CT) :

	# 1. perform element wise sums
	# https://stackoverflow.com/questions/19261747/sum-of-n-lists-element-wise-python

	# list1 is total conc of hexamers with 1 site phosphorylated at each time
	# list2 is total conc of hexamers with 2 sites phosphorylated at each time

	LISTS = {}

	for i in range(1,7) :
		
		Ci = values[find_species_index("[C%d]" % (i), variables)]
		iCi = values[find_species_index("[iC%d]" % (i), variables)]
		
		# Note: the ACi complexes are just transient states,
		# so not including them in totals does not make a lot of difference

		#ACi = values[find_species_index("[AC%d]" % (i), variables)]
		#LISTS["list%d" % (i)] = [sum(x) for x in zip(Ci,iCi,ACi)]

		LISTS["list%d" % (i)] = [sum(x) for x in zip(Ci,iCi)]

	# 2. multiple items in list 1 by 1, list 2 by 2, list 3 by 3 etc...

	for i in range(1,7) :

		LISTS["list%d" % (i)] = [x * i for x in LISTS["list%d" % (i)]]

	# 3. now make final list, which is numerator of p fraction at each time
	NUMERATOR = [sum(x) for x in zip(LISTS["list1"],LISTS["list2"],LISTS["list3"], \
		LISTS["list4"],LISTS["list5"],LISTS["list6"])]

	# 4. divide each element by 6CT, to get p
	p = [x / (6 * CT) for x in NUMERATOR]

	return p






if __name__ == "__main__" :

	# total concentrations
	AT = 0.012e-6  # M  = 0.012uM
	CT = 0.58e-6   # M  = 0.58uM

	# get traj data
	variables, values = read_csv()

	tidx = find_species_index("#Time", variables)
	time = values[tidx]

	Aconc_normalised = calc_A_div_AT(variables, values, AT)
	p = calc_p(variables, values, CT)

	plt.figure()
	plt.plot(time, Aconc_normalised)
	plt.plot(time, p)

	plt.xlabel("Time (hours)")
	plt.ylabel("p and [A]/[A]T")

	plt.xticks([0,12,24,36,48,60,72])
	plt.show()





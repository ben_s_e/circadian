<?xml version="1.0" encoding="UTF-8"?>
<!-- generated with COPASI 4.29 (Build 228) (http://www.copasi.org) at 2021-02-08T21:34:25Z -->
<?oxygen RNGSchema="http://www.copasi.org/static/schema/CopasiML.rng" type="xml"?>
<COPASI xmlns="http://www.copasi.org/static/schema" versionMajor="4" versionMinor="29" versionDevel="228" copasiSourcesModified="0">
  <ListOfFunctions>
    <Function key="Function_13" name="Mass action (irreversible)" type="MassAction" reversible="false">
      <MiriamAnnotation>
<rdf:RDF xmlns:CopasiMT="http://www.copasi.org/RDF/MiriamTerms#" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
   <rdf:Description rdf:about="#Function_13">
   <CopasiMT:is rdf:resource="urn:miriam:obo.sbo:SBO:0000163" />
   </rdf:Description>
   </rdf:RDF>
      </MiriamAnnotation>
      <Comment>
        <body xmlns="http://www.w3.org/1999/xhtml">
<b>Mass action rate law for irreversible reactions</b>
<p>
Reaction scheme where the products are created from the reactants and the change of a product quantity is proportional to the product of reactant activities. The reaction scheme does not include any reverse process that creates the reactants from the products. The change of a product quantity is proportional to the quantity of one reactant.
</p>
</body>
      </Comment>
      <Expression>
        k1*PRODUCT&lt;substrate_i>
      </Expression>
      <ListOfParameterDescriptions>
        <ParameterDescription key="FunctionParameter_80" name="k1" order="0" role="constant"/>
        <ParameterDescription key="FunctionParameter_81" name="substrate" order="1" role="substrate"/>
      </ListOfParameterDescriptions>
    </Function>
  </ListOfFunctions>
  <Model key="Model_1" name="Zwicker et al 2010 full PPC in vitro model" simulationType="time" timeUnit="h" volumeUnit="l" areaUnit="1" lengthUnit="1" quantityUnit="mol" type="deterministic" avogadroConstant="6.0221417899999999e+23">
    <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Model_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

    </MiriamAnnotation>
    <ListOfCompartments>
      <Compartment key="Compartment_0" name="ecoli" simulationType="fixed" dimensionality="3" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Compartment_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Compartment>
    </ListOfCompartments>
    <ListOfMetabolites>
      <Metabolite key="Metabolite_0" name="A" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_1" name="B" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_2" name="C0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_3" name="iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_4" name="AC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_5" name="B1iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_6" name="B2iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_7" name="A1B1iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_8" name="A1B2iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_9" name="A2B2iC0" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_9">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_10" name="C1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_10">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_11" name="iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_11">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_12" name="AC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_12">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_13" name="B1iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_13">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_14" name="B2iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_15" name="A1B1iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_15">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_16" name="A1B2iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_17" name="A2B2iC1" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_18" name="C2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_18">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_19" name="iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_20" name="AC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_21" name="B1iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_21">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_22" name="B2iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_22">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_23" name="A1B1iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_24" name="A1B2iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_24">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_25" name="A2B2iC2" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_26" name="C3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_27" name="iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_28" name="AC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_29" name="B1iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_30" name="B2iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_30">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_31" name="A1B1iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_31">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_32" name="A1B2iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_33" name="A2B2iC3" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_33">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_34" name="C4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_35" name="iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_35">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_36" name="AC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_37" name="B1iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_37">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_38" name="B2iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_38">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_39" name="A1B1iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_39">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_40" name="A1B2iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_41" name="A2B2iC4" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_41">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_42" name="C5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_42">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_43" name="iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_43">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_44" name="AC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_44">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_45" name="B1iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_45">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_46" name="B2iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_46">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_47" name="A1B1iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_47">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_48" name="A1B2iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_48">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_49" name="A2B2iC5" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_49">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_50" name="C6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_50">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_51" name="iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_51">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_52" name="AC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_52">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_53" name="B1iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_53">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_54" name="B2iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_54">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_55" name="A1B1iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_55">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_56" name="A1B2iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_56">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
      <Metabolite key="Metabolite_57" name="A2B2iC6" simulationType="reactions" compartment="Compartment_0" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Metabolite_57">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
      </Metabolite>
    </ListOfMetabolites>
    <ListOfModelValues>
      <ModelValue key="ModelValue_0" name="bi" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_1" name="ki_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_1">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_2" name="k_pf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_2">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_3" name="k_ps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_3">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_4" name="k_dps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_4">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_5" name="ik_ps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_5">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_6" name="ik_dps" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_6">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_7" name="f0" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_7">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_8" name="k0_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_8">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_9" name="ik0_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_9">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_10" name="ik0_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_10">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_11" name="ik0_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_11">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_12" name="ik0_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_13" name="ik0_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_13">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_14" name="ik0_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_14">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_15" name="ik0_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_15">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_16" name="ik0_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_16">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_17" name="f1" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_17">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_18" name="k1_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_18">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_19" name="ik1_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_19">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_20" name="ik1_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_20">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_21" name="ik1_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_21">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_22" name="ik1_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_22">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_23" name="ik1_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_23">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_24" name="ik1_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_24">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_25" name="ik1_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_25">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_26" name="ik1_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_26">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_27" name="f2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_27">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_28" name="k2_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_28">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_29" name="ik2_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_29">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_30" name="ik2_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_30">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_31" name="ik2_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_31">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_32" name="ik2_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_32">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_33" name="ik2_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_33">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_34" name="ik2_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_34">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_35" name="ik2_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_35">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_36" name="ik2_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_36">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_37" name="f3" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_37">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_38" name="k3_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_38">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_39" name="ik3_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_39">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_40" name="ik3_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_40">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_41" name="ik3_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_41">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_42" name="ik3_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_42">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_43" name="ik3_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_43">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_44" name="ik3_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_44">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_45" name="ik3_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_45">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_46" name="ik3_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_46">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_47" name="f4" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_47">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_48" name="k4_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_48">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_49" name="ik4_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_49">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_50" name="ik4_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_50">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_51" name="ik4_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_51">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_52" name="ik4_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_52">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_53" name="ik4_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_53">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_54" name="ik4_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_54">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_55" name="ik4_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_55">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_56" name="ik4_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_56">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_57" name="f5" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_57">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_58" name="k5_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_58">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_59" name="ik5_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_59">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_60" name="ik5_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_60">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_61" name="ik5_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_61">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_62" name="ik5_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_62">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_63" name="ik5_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_63">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_64" name="ik5_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_64">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_65" name="ik5_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_65">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_66" name="ik5_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_66">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_67" name="f6" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_67">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_68" name="k6_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_68">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_69" name="ik6_Bf" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_69">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_70" name="ik6_Bf__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_70">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_71" name="ik6_Bb" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_71">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_72" name="ik6_Bb__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_72">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_73" name="ik6_Af" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_73">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_74" name="ik6_Af__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_74">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          l/(mol*h)
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_75" name="ik6_Ab" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_75">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
      <ModelValue key="ModelValue_76" name="ik6_Ab__2" simulationType="fixed" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelValue_76">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <Unit>
          1/h
        </Unit>
      </ModelValue>
    </ListOfModelValues>
    <ListOfReactions>
      <Reaction key="Reaction_0" name="R1_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_0">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5376" name="k1" value="1e-06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_7"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_1" name="R1_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_1">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5375" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_2" name="R1_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_2">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5374" name="k1" value="1e-05"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_17"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_3" name="R1_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5373" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_4" name="R1_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_4">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5372" name="k1" value="0.0001"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_27"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_5" name="R1_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5371" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_6" name="R1_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_6">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5370" name="k1" value="0.001"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_37"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_7" name="R1_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_7">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5369" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_8" name="R1_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_8">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5368" name="k1" value="0.01"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_47"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_9" name="R1_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_9">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5367" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_10" name="R1_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_10">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5366" name="k1" value="0.1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_57"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_42"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_11" name="R1_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_11">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5365" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_43"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_12" name="R1_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_12">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_50" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_51" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5364" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_67"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_50"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_13" name="R1_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_13">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_51" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_50" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5363" name="k1" value="100"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_0"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_51"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_14" name="R2_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_14">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5362" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_2"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_15" name="R2_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_15">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5361" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_8"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_16" name="R2_0irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_16">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_4" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5360" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_4"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_17" name="R2_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_17">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_12" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5359" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_10"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_18" name="R2_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_18">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_12" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5358" name="k1" value="3"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_18"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_12"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_19" name="R2_1irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_19">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_12" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5357" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_12"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_20" name="R2_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_20">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5356" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_18"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_21" name="R2_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_21">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5680" name="k1" value="9"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_28"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_20"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_22" name="R2_2irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_22">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_20" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5681" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_20"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_23" name="R2_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_23">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5679" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_26"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_24" name="R2_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_24">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5682" name="k1" value="27"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_38"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_25" name="R2_3irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_25">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_28" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5709" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_28"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_26" name="R2_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_26">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_34" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5710" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_34"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_27" name="R2_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_27">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5708" name="k1" value="81"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_48"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_36"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_28" name="R2_4irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_28">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_36" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5711" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_36"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_29" name="R2_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_29">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_42" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_44" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5693" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_42"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_30" name="R2_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_30">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_44" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5694" name="k1" value="243"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_58"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_44"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_31" name="R2_5irrev" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_31">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_44" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_50" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5692" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_2"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_44"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_32" name="R2_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_32">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_50" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_52" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5695" name="k1" value="1.72e+10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_1"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_50"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_33" name="R2_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_33">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_52" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_50" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5355" name="k1" value="729"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_68"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_52"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_34" name="R3_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_34">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5354" name="k1" value="3.44e+06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_10"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_3"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_35" name="R3_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_35">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5353" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_11"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_36" name="R3_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_36">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5352" name="k1" value="3.44e+08"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_20"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_11"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_37" name="R3_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_37">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5351" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_21"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_38" name="R3_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_38">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5350" name="k1" value="3.44e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_30"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_19"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_39" name="R3_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_39">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_19" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5349" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_31"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_21"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_40" name="R3_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_40">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5348" name="k1" value="3.44e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_40"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_27"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_41" name="R3_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_41">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5347" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_41"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_42" name="R3_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_42">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5346" name="k1" value="3.44e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_50"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_35"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_43" name="R3_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_43">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5345" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_51"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_44" name="R3_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_44">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_43" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5344" name="k1" value="3.44e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_60"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_43"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_45" name="R3_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_45">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_43" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5343" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_61"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_45"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_46" name="R3_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_46">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_51" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_53" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5342" name="k1" value="3.44e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_70"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_51"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_47" name="R3_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_47">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_53" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_51" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5341" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_71"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_53"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_48" name="R4_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_48">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5340" name="k1" value="1.72e+06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_9"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_5"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_49" name="R4_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_49">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5648" name="k1" value="20"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_12"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_50" name="R4_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_50">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5649" name="k1" value="1.72e+08"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_19"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_13"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_51" name="R4_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_51">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5647" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_22"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_52" name="R4_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_52">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5650" name="k1" value="1.72e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_29"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_21"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_53" name="R4_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_53">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5339" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_32"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_54" name="R4_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_54">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5338" name="k1" value="1.72e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_39"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_55" name="R4_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_55">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_29" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5337" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_42"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_56" name="R4_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_56">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_37" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5336" name="k1" value="1.72e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_49"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_37"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_57" name="R4_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_57">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5335" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_52"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_38"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_58" name="R4_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_58">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_45" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5334" name="k1" value="1.72e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_59"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_45"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_59" name="R4_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_59">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5333" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_62"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_46"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_60" name="R4_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_60">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_53" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5332" name="k1" value="1.72e+09"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_69"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_53"/>
              <SourceParameter reference="Metabolite_1"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_61" name="R4_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_61">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_53" stoichiometry="1"/>
          <Product metabolite="Metabolite_1" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5331" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_72"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_54"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_62" name="R5_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_62">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5330" name="k1" value="1.72e+07"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_13"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_5"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_63" name="R5_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_63">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5329" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_15"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_64" name="R5_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_64">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5328" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_23"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_13"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_65" name="R5_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_65">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5327" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_25"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_66" name="R5_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_66">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5326" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_33"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_21"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_67" name="R5_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_67">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5325" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_35"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_68" name="R5_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_68">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5324" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_43"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_29"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_69" name="R5_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_69">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_29" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5323" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_45"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_70" name="R5_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_70">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_37" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5322" name="k1" value="1.72e+11"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_53"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_37"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_71" name="R5_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_71">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5321" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_55"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_39"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_72" name="R5_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_72">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_45" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5320" name="k1" value="1.72e+06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_63"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_45"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_73" name="R5_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_73">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5319" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_65"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_47"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_74" name="R5_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_74">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_53" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_55" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5313" name="k1" value="172000"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_73"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_53"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_75" name="R5_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_75">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_55" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_53" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5312" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_75"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_55"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_76" name="R6_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_76">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5311" name="k1" value="3.44e+07"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_14"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_6"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_77" name="R6_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_77">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5310" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_15"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_8"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_78" name="R6_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_78">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5309" name="k1" value="3.44e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_24"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_14"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_79" name="R6_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_79">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5308" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_25"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_80" name="R6_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_80">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5307" name="k1" value="3.44e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_34"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_22"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_81" name="R6_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_81">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_22" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5306" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_35"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_82" name="R6_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_82">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5305" name="k1" value="3.44e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_44"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_30"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_83" name="R6_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_83">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_30" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5304" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_45"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_84" name="R6_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_84">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_38" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5303" name="k1" value="3.44e+11"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_54"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_38"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_85" name="R6_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_85">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_38" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5302" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_55"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_86" name="R6_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_86">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_46" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5301" name="k1" value="3.44e+06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_64"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_46"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_87" name="R6_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_87">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_46" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5300" name="k1" value="1"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_65"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_48"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_88" name="R6_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_88">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_54" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5299" name="k1" value="344000"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_74"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_54"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_89" name="R6_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_89">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_54" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5298" name="k1" value="10"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_75"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_56"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_90" name="R7_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_90">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5297" name="k1" value="1.72e+07"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_13"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_8"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_91" name="R7_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_91">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5296" name="k1" value="20"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_16"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_92" name="R7_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_92">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5295" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_23"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_16"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_93" name="R7_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_93">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_16" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5294" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_26"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_94" name="R7_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_94">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5293" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_33"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_24"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_95" name="R7_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_95">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5292" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_36"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_96" name="R7_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_96">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5291" name="k1" value="1.72e+12"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_43"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_32"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_97" name="R7_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_97">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5290" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_46"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_98" name="R7_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_98">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_40" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5289" name="k1" value="1.72e+11"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_53"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_40"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_99" name="R7_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_99">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_40" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5288" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_56"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_41"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_100" name="R7_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_100">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_48" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5287" name="k1" value="1.72e+06"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_63"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_48"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_101" name="R7_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_101">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_48" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5286" name="k1" value="2"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_66"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_102" name="R7_6f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_102">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_56" stoichiometry="1"/>
          <Substrate metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_57" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5285" name="k1" value="172000"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_73"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_56"/>
              <SourceParameter reference="Metabolite_0"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_103" name="R7_6b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_103">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_57" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_56" stoichiometry="1"/>
          <Product metabolite="Metabolite_0" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5284" name="k1" value="20"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_76"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_57"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_104" name="R8_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_104">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5283" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_2"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_105" name="R8_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_105">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_2" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5282" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_106" name="R8_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_106">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5281" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_10"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_107" name="R8_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_107">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_10" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5280" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_108" name="R8_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_108">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5279" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_18"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_109" name="R8_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_109">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_18" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5278" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_110" name="R8_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_110">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5277" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_26"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_111" name="R8_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_111">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_26" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5276" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_112" name="R8_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_112">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5275" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_34"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_113" name="R8_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_113">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_34" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5274" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_42"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_114" name="R8_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_114">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_50" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5273" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_3"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_42"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_115" name="R8_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_115">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_50" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_42" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5272" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_4"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_50"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_116" name="R9_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_116">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5271" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_3"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_117" name="R9_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_117">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_3" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5270" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_118" name="R9_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_118">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5269" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_11"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_119" name="R9_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_119">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_11" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5268" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_120" name="R9_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_120">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5267" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_19"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_121" name="R9_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_121">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_19" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5266" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_122" name="R9_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_122">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5265" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_27"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_123" name="R9_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_123">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_27" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5264" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_124" name="R9_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_124">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5263" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_35"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_125" name="R9_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_125">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_35" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5262" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_43"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_126" name="R9_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_126">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_51" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5261" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_43"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_127" name="R9_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_127">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_51" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_43" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5260" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_51"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_128" name="R10_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_128">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5259" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_5"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_129" name="R10_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_129">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_5" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5258" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_130" name="R10_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_130">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5257" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_13"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_131" name="R10_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_131">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_13" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5256" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_21"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_132" name="R10_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_132">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5255" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_21"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_133" name="R10_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_133">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_21" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5254" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_134" name="R10_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_134">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5253" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_29"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_135" name="R10_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_135">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_29" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5252" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_136" name="R10_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_136">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5251" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_37"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_137" name="R10_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_137">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_37" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5250" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_45"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_138" name="R10_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_138">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_53" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5249" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_45"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_139" name="R10_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_139">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_53" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_45" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5248" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_53"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_140" name="R11_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_140">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5247" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_6"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_141" name="R11_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_141">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_6" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5246" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_142" name="R11_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_142">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5245" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_14"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_143" name="R11_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_143">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_14" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5244" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_144" name="R11_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_144">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5243" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_22"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_145" name="R11_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_145">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_22" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5242" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_146" name="R11_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_146">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5241" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_30"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_147" name="R11_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_147">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_30" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5240" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_38"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_148" name="R11_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_148">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5239" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_38"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_149" name="R11_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_149">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_38" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5238" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_46"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_150" name="R11_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_150">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5237" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_46"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_151" name="R11_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_151">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_54" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_46" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5236" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_54"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_152" name="R12_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_152">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5235" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_7"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_153" name="R12_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_153">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_7" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5234" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_154" name="R12_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_154">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5233" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_15"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_155" name="R12_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_155">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_15" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5232" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_156" name="R12_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_156">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5231" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_23"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_157" name="R12_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_157">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_23" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5230" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_158" name="R12_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_158">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5229" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_31"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_159" name="R12_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_159">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_31" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5228" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_39"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_160" name="R12_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_160">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5227" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_39"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_161" name="R12_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_161">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_39" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5226" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_47"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_162" name="R12_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_162">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_55" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5225" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_47"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_163" name="R12_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_163">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_55" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_47" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5224" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_55"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_164" name="R13_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_164">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5223" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_8"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_165" name="R13_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_165">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_8" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5222" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_166" name="R13_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_166">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5221" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_16"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_167" name="R13_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_167">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_16" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5220" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_168" name="R13_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_168">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5219" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_24"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_169" name="R13_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_169">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_24" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5218" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_170" name="R13_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_170">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5217" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_32"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_171" name="R13_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_171">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_32" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5216" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_172" name="R13_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_172">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5215" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_40"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_173" name="R13_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_173">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_40" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5214" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_48"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_174" name="R13_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_174">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5213" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_48"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_175" name="R13_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Reaction_175">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_56" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_48" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5212" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_56"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_176" name="R14_0f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_176">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5211" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_9"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_177" name="R14_0b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_177">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_9" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5210" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_178" name="R14_1f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_178">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5865" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_17"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_179" name="R14_1b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_179">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_17" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5866" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_180" name="R14_2f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_180">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5864" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_25"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_181" name="R14_2b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_181">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_25" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5867" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_182" name="R14_3f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_182">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5209" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_33"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_183" name="R14_3b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_183">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_33" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5208" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_41"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_184" name="R14_4f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_184">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5207" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_41"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_185" name="R14_4b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_185">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_41" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5206" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_186" name="R14_5f" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_186">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_57" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5205" name="k1" value="0.025"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_5"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_49"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
      <Reaction key="Reaction_187" name="R14_5b" reversible="false" fast="false" addNoise="false">
        <MiriamAnnotation>
<rdf:RDF xmlns:dcterms="http://purl.org/dc/terms/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#Reaction_187">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-07T22:55:08Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ListOfSubstrates>
          <Substrate metabolite="Metabolite_57" stoichiometry="1"/>
        </ListOfSubstrates>
        <ListOfProducts>
          <Product metabolite="Metabolite_49" stoichiometry="1"/>
        </ListOfProducts>
        <ListOfConstants>
          <Constant key="Parameter_5204" name="k1" value="0.4"/>
        </ListOfConstants>
        <KineticLaw function="Function_13" unitType="Default" scalingCompartment="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]">
          <ListOfCallParameters>
            <CallParameter functionParameter="FunctionParameter_80">
              <SourceParameter reference="ModelValue_6"/>
            </CallParameter>
            <CallParameter functionParameter="FunctionParameter_81">
              <SourceParameter reference="Metabolite_57"/>
            </CallParameter>
          </ListOfCallParameters>
        </KineticLaw>
      </Reaction>
    </ListOfReactions>
    <ListOfModelParameterSets activeSet="ModelParameterSet_1">
      <ModelParameterSet key="ModelParameterSet_1" name="Initial State">
        <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#ModelParameterSet_1">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:57Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]" value="1.0000000000000001e-15" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A]" value="349.28422382000019" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B]" value="1053.87481325" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C0]" value="349.28422382000008" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af]" value="17200000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0]" value="9.9999999999999995e-07" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af]" value="17200000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2]" value="34400000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1]" value="1.0000000000000001e-05" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab]" value="3" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2]" value="344000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2]" value="0.0001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab]" value="9" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3]" value="0.001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab]" value="27" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab]" value="81" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af]" value="172000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2]" value="344000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab]" value="243" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab]" value="729" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af]" value="172000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2]" value="344000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f],ParameterGroup=Parameters,Parameter=k1" value="9.9999999999999995e-07" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f],ParameterGroup=Parameters,Parameter=k1" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f],ParameterGroup=Parameters,Parameter=k1" value="0.0001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f],ParameterGroup=Parameters,Parameter=k1" value="0.001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b],ParameterGroup=Parameters,Parameter=k1" value="3" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b],ParameterGroup=Parameters,Parameter=k1" value="9" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b],ParameterGroup=Parameters,Parameter=k1" value="27" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b],ParameterGroup=Parameters,Parameter=k1" value="81" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b],ParameterGroup=Parameters,Parameter=k1" value="243" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b],ParameterGroup=Parameters,Parameter=k1" value="729" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f],ParameterGroup=Parameters,Parameter=k1" value="344000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f],ParameterGroup=Parameters,Parameter=k1" value="34400000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f],ParameterGroup=Parameters,Parameter=k1" value="344000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f],ParameterGroup=Parameters,Parameter=k1" value="344000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
      <ModelParameterSet key="ModelParameterSet_3" name="vol 1um3,  C0=0.58uM">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelParameterSet_3">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-08T13:59:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]" value="1.0000000000000001e-15" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A]" value="349.28422382000019" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B]" value="1053.87481325" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C0]" value="349.28422382000002" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af]" value="17200000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0]" value="9.9999999999999995e-07" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af]" value="17200000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2]" value="34400000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1]" value="1.0000000000000001e-05" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab]" value="3" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2]" value="344000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2]" value="0.0001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab]" value="9" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3]" value="0.001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab]" value="27" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab]" value="81" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af]" value="172000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2]" value="344000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab]" value="243" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab]" value="729" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af]" value="172000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2]" value="344000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f],ParameterGroup=Parameters,Parameter=k1" value="9.9999999999999995e-07" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f],ParameterGroup=Parameters,Parameter=k1" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f],ParameterGroup=Parameters,Parameter=k1" value="0.0001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f],ParameterGroup=Parameters,Parameter=k1" value="0.001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b],ParameterGroup=Parameters,Parameter=k1" value="3" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b],ParameterGroup=Parameters,Parameter=k1" value="9" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b],ParameterGroup=Parameters,Parameter=k1" value="27" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b],ParameterGroup=Parameters,Parameter=k1" value="81" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b],ParameterGroup=Parameters,Parameter=k1" value="243" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b],ParameterGroup=Parameters,Parameter=k1" value="729" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f],ParameterGroup=Parameters,Parameter=k1" value="344000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f],ParameterGroup=Parameters,Parameter=k1" value="34400000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f],ParameterGroup=Parameters,Parameter=k1" value="344000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f],ParameterGroup=Parameters,Parameter=k1" value="344000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
      <ModelParameterSet key="ModelParameterSet_5" name="vol 0.1um3, C0=0.58uM">
        <MiriamAnnotation>
<rdf:RDF
   xmlns:dcterms="http://purl.org/dc/terms/"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about="#ModelParameterSet_5">
    <dcterms:created>
      <rdf:Description>
        <dcterms:W3CDTF>2021-02-08T13:59:10Z</dcterms:W3CDTF>
      </rdf:Description>
    </dcterms:created>
  </rdf:Description>
</rdf:RDF>

        </MiriamAnnotation>
        <ModelParameterGroup cn="String=Initial Time" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model" value="0" type="Model" simulationType="time"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Compartment Sizes" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli]" value="9.9999999999999998e-17" type="Compartment" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Species Values" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A]" value="34.928422382000022" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B]" value="105.38748132499995" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C0]" value="34.928422381999994" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC0]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC1]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC2]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC3]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC4]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC5]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC6]" value="0" type="Species" simulationType="reactions"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC6]" value="0" type="Species" simulationType="reactions"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Initial Global Quantities" type="Group">
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi]" value="100" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af]" value="17200000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps]" value="0.025000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps]" value="0.40000000000000002" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0]" value="9.9999999999999995e-07" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af]" value="17200000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2]" value="34400000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1]" value="1.0000000000000001e-05" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab]" value="3" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf]" value="172000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2]" value="344000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2]" value="0.0001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab]" value="9" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3]" value="0.001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab]" value="27" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af]" value="1720000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2]" value="3440000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4]" value="0.01" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab]" value="81" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af]" value="172000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2]" value="344000000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5]" value="0.10000000000000001" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab]" value="243" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af]" value="1720000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2]" value="3440000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab]" value="729" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf]" value="1720000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2]" value="3440000000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb]" value="1" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2]" value="2" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af]" value="172000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2]" value="344000" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab]" value="10" type="ModelValue" simulationType="fixed"/>
          <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2]" value="20" type="ModelValue" simulationType="fixed"/>
        </ModelParameterGroup>
        <ModelParameterGroup cn="String=Kinetic Parameters" type="Group">
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0f],ParameterGroup=Parameters,Parameter=k1" value="9.9999999999999995e-07" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f0],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_0b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1f],ParameterGroup=Parameters,Parameter=k1" value="1.0000000000000001e-05" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f1],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_1b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2f],ParameterGroup=Parameters,Parameter=k1" value="0.0001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_2b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3f],ParameterGroup=Parameters,Parameter=k1" value="0.001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f3],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_3b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4f],ParameterGroup=Parameters,Parameter=k1" value="0.01" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f4],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_4b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5f],ParameterGroup=Parameters,Parameter=k1" value="0.10000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f5],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_5b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6f],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[f6],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R1_6b],ParameterGroup=Parameters,Parameter=k1" value="100" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[bi],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_0irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1b],ParameterGroup=Parameters,Parameter=k1" value="3" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_1irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2b],ParameterGroup=Parameters,Parameter=k1" value="9" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_2irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3b],ParameterGroup=Parameters,Parameter=k1" value="27" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_3irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4b],ParameterGroup=Parameters,Parameter=k1" value="81" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_4irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5b],ParameterGroup=Parameters,Parameter=k1" value="243" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_5irrev],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_pf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6f],ParameterGroup=Parameters,Parameter=k1" value="17200000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ki_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R2_6b],ParameterGroup=Parameters,Parameter=k1" value="729" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1f],ParameterGroup=Parameters,Parameter=k1" value="344000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6f],ParameterGroup=Parameters,Parameter=k1" value="3440000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R3_6b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1f],ParameterGroup=Parameters,Parameter=k1" value="172000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6f],ParameterGroup=Parameters,Parameter=k1" value="1720000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bf],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R4_6b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Bb__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R5_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0f],ParameterGroup=Parameters,Parameter=k1" value="34400000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_0b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_1b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_2b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3f],ParameterGroup=Parameters,Parameter=k1" value="3440000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_3b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4f],ParameterGroup=Parameters,Parameter=k1" value="344000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_4b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5f],ParameterGroup=Parameters,Parameter=k1" value="3440000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_5b],ParameterGroup=Parameters,Parameter=k1" value="1" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6f],ParameterGroup=Parameters,Parameter=k1" value="344000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R6_6b],ParameterGroup=Parameters,Parameter=k1" value="10" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0f],ParameterGroup=Parameters,Parameter=k1" value="17200000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_0b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik0_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_1b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik1_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_2b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik2_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3f],ParameterGroup=Parameters,Parameter=k1" value="1720000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_3b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik3_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4f],ParameterGroup=Parameters,Parameter=k1" value="172000000000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_4b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik4_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5f],ParameterGroup=Parameters,Parameter=k1" value="1720000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_5b],ParameterGroup=Parameters,Parameter=k1" value="2" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik5_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6f],ParameterGroup=Parameters,Parameter=k1" value="172000" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Af],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R7_6b],ParameterGroup=Parameters,Parameter=k1" value="20" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik6_Ab__2],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R8_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[k_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R9_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R10_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R11_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R12_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R13_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_0b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_1b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_2b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_3b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_4b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5f],ParameterGroup=Parameters,Parameter=k1" value="0.025000000000000001" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_ps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
          <ModelParameterGroup cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b]" type="Reaction">
            <ModelParameter cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Reactions[R14_5b],ParameterGroup=Parameters,Parameter=k1" value="0.40000000000000002" type="ReactionParameter" simulationType="assignment">
              <InitialExpression>
                &lt;CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Values[ik_dps],Reference=InitialValue>
              </InitialExpression>
            </ModelParameter>
          </ModelParameterGroup>
        </ModelParameterGroup>
      </ModelParameterSet>
    </ListOfModelParameterSets>
    <StateTemplate>
      <StateTemplateVariable objectReference="Model_1"/>
      <StateTemplateVariable objectReference="Metabolite_0"/>
      <StateTemplateVariable objectReference="Metabolite_1"/>
      <StateTemplateVariable objectReference="Metabolite_13"/>
      <StateTemplateVariable objectReference="Metabolite_29"/>
      <StateTemplateVariable objectReference="Metabolite_45"/>
      <StateTemplateVariable objectReference="Metabolite_21"/>
      <StateTemplateVariable objectReference="Metabolite_37"/>
      <StateTemplateVariable objectReference="Metabolite_10"/>
      <StateTemplateVariable objectReference="Metabolite_34"/>
      <StateTemplateVariable objectReference="Metabolite_42"/>
      <StateTemplateVariable objectReference="Metabolite_18"/>
      <StateTemplateVariable objectReference="Metabolite_16"/>
      <StateTemplateVariable objectReference="Metabolite_32"/>
      <StateTemplateVariable objectReference="Metabolite_48"/>
      <StateTemplateVariable objectReference="Metabolite_26"/>
      <StateTemplateVariable objectReference="Metabolite_5"/>
      <StateTemplateVariable objectReference="Metabolite_53"/>
      <StateTemplateVariable objectReference="Metabolite_22"/>
      <StateTemplateVariable objectReference="Metabolite_38"/>
      <StateTemplateVariable objectReference="Metabolite_43"/>
      <StateTemplateVariable objectReference="Metabolite_11"/>
      <StateTemplateVariable objectReference="Metabolite_27"/>
      <StateTemplateVariable objectReference="Metabolite_40"/>
      <StateTemplateVariable objectReference="Metabolite_24"/>
      <StateTemplateVariable objectReference="Metabolite_50"/>
      <StateTemplateVariable objectReference="Metabolite_14"/>
      <StateTemplateVariable objectReference="Metabolite_46"/>
      <StateTemplateVariable objectReference="Metabolite_31"/>
      <StateTemplateVariable objectReference="Metabolite_47"/>
      <StateTemplateVariable objectReference="Metabolite_15"/>
      <StateTemplateVariable objectReference="Metabolite_8"/>
      <StateTemplateVariable objectReference="Metabolite_56"/>
      <StateTemplateVariable objectReference="Metabolite_33"/>
      <StateTemplateVariable objectReference="Metabolite_49"/>
      <StateTemplateVariable objectReference="Metabolite_17"/>
      <StateTemplateVariable objectReference="Metabolite_2"/>
      <StateTemplateVariable objectReference="Metabolite_19"/>
      <StateTemplateVariable objectReference="Metabolite_35"/>
      <StateTemplateVariable objectReference="Metabolite_39"/>
      <StateTemplateVariable objectReference="Metabolite_23"/>
      <StateTemplateVariable objectReference="Metabolite_30"/>
      <StateTemplateVariable objectReference="Metabolite_41"/>
      <StateTemplateVariable objectReference="Metabolite_51"/>
      <StateTemplateVariable objectReference="Metabolite_3"/>
      <StateTemplateVariable objectReference="Metabolite_25"/>
      <StateTemplateVariable objectReference="Metabolite_55"/>
      <StateTemplateVariable objectReference="Metabolite_7"/>
      <StateTemplateVariable objectReference="Metabolite_28"/>
      <StateTemplateVariable objectReference="Metabolite_12"/>
      <StateTemplateVariable objectReference="Metabolite_44"/>
      <StateTemplateVariable objectReference="Metabolite_54"/>
      <StateTemplateVariable objectReference="Metabolite_20"/>
      <StateTemplateVariable objectReference="Metabolite_36"/>
      <StateTemplateVariable objectReference="Metabolite_57"/>
      <StateTemplateVariable objectReference="Metabolite_52"/>
      <StateTemplateVariable objectReference="Metabolite_4"/>
      <StateTemplateVariable objectReference="Metabolite_6"/>
      <StateTemplateVariable objectReference="Metabolite_9"/>
      <StateTemplateVariable objectReference="Compartment_0"/>
      <StateTemplateVariable objectReference="ModelValue_0"/>
      <StateTemplateVariable objectReference="ModelValue_1"/>
      <StateTemplateVariable objectReference="ModelValue_2"/>
      <StateTemplateVariable objectReference="ModelValue_3"/>
      <StateTemplateVariable objectReference="ModelValue_4"/>
      <StateTemplateVariable objectReference="ModelValue_5"/>
      <StateTemplateVariable objectReference="ModelValue_6"/>
      <StateTemplateVariable objectReference="ModelValue_7"/>
      <StateTemplateVariable objectReference="ModelValue_8"/>
      <StateTemplateVariable objectReference="ModelValue_9"/>
      <StateTemplateVariable objectReference="ModelValue_10"/>
      <StateTemplateVariable objectReference="ModelValue_11"/>
      <StateTemplateVariable objectReference="ModelValue_12"/>
      <StateTemplateVariable objectReference="ModelValue_13"/>
      <StateTemplateVariable objectReference="ModelValue_14"/>
      <StateTemplateVariable objectReference="ModelValue_15"/>
      <StateTemplateVariable objectReference="ModelValue_16"/>
      <StateTemplateVariable objectReference="ModelValue_17"/>
      <StateTemplateVariable objectReference="ModelValue_18"/>
      <StateTemplateVariable objectReference="ModelValue_19"/>
      <StateTemplateVariable objectReference="ModelValue_20"/>
      <StateTemplateVariable objectReference="ModelValue_21"/>
      <StateTemplateVariable objectReference="ModelValue_22"/>
      <StateTemplateVariable objectReference="ModelValue_23"/>
      <StateTemplateVariable objectReference="ModelValue_24"/>
      <StateTemplateVariable objectReference="ModelValue_25"/>
      <StateTemplateVariable objectReference="ModelValue_26"/>
      <StateTemplateVariable objectReference="ModelValue_27"/>
      <StateTemplateVariable objectReference="ModelValue_28"/>
      <StateTemplateVariable objectReference="ModelValue_29"/>
      <StateTemplateVariable objectReference="ModelValue_30"/>
      <StateTemplateVariable objectReference="ModelValue_31"/>
      <StateTemplateVariable objectReference="ModelValue_32"/>
      <StateTemplateVariable objectReference="ModelValue_33"/>
      <StateTemplateVariable objectReference="ModelValue_34"/>
      <StateTemplateVariable objectReference="ModelValue_35"/>
      <StateTemplateVariable objectReference="ModelValue_36"/>
      <StateTemplateVariable objectReference="ModelValue_37"/>
      <StateTemplateVariable objectReference="ModelValue_38"/>
      <StateTemplateVariable objectReference="ModelValue_39"/>
      <StateTemplateVariable objectReference="ModelValue_40"/>
      <StateTemplateVariable objectReference="ModelValue_41"/>
      <StateTemplateVariable objectReference="ModelValue_42"/>
      <StateTemplateVariable objectReference="ModelValue_43"/>
      <StateTemplateVariable objectReference="ModelValue_44"/>
      <StateTemplateVariable objectReference="ModelValue_45"/>
      <StateTemplateVariable objectReference="ModelValue_46"/>
      <StateTemplateVariable objectReference="ModelValue_47"/>
      <StateTemplateVariable objectReference="ModelValue_48"/>
      <StateTemplateVariable objectReference="ModelValue_49"/>
      <StateTemplateVariable objectReference="ModelValue_50"/>
      <StateTemplateVariable objectReference="ModelValue_51"/>
      <StateTemplateVariable objectReference="ModelValue_52"/>
      <StateTemplateVariable objectReference="ModelValue_53"/>
      <StateTemplateVariable objectReference="ModelValue_54"/>
      <StateTemplateVariable objectReference="ModelValue_55"/>
      <StateTemplateVariable objectReference="ModelValue_56"/>
      <StateTemplateVariable objectReference="ModelValue_57"/>
      <StateTemplateVariable objectReference="ModelValue_58"/>
      <StateTemplateVariable objectReference="ModelValue_59"/>
      <StateTemplateVariable objectReference="ModelValue_60"/>
      <StateTemplateVariable objectReference="ModelValue_61"/>
      <StateTemplateVariable objectReference="ModelValue_62"/>
      <StateTemplateVariable objectReference="ModelValue_63"/>
      <StateTemplateVariable objectReference="ModelValue_64"/>
      <StateTemplateVariable objectReference="ModelValue_65"/>
      <StateTemplateVariable objectReference="ModelValue_66"/>
      <StateTemplateVariable objectReference="ModelValue_67"/>
      <StateTemplateVariable objectReference="ModelValue_68"/>
      <StateTemplateVariable objectReference="ModelValue_69"/>
      <StateTemplateVariable objectReference="ModelValue_70"/>
      <StateTemplateVariable objectReference="ModelValue_71"/>
      <StateTemplateVariable objectReference="ModelValue_72"/>
      <StateTemplateVariable objectReference="ModelValue_73"/>
      <StateTemplateVariable objectReference="ModelValue_74"/>
      <StateTemplateVariable objectReference="ModelValue_75"/>
      <StateTemplateVariable objectReference="ModelValue_76"/>
    </StateTemplate>
    <InitialState type="initialState">
      0 349.28422382000019 1053.87481325 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 349.28422382000008 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1.0000000000000001e-15 100 17200000000 1 0.025000000000000001 0.40000000000000002 0.025000000000000001 0.40000000000000002 9.9999999999999995e-07 1 1720000 3440000 10 20 17200000 34400000 10 20 1.0000000000000001e-05 3 172000000 344000000 1 2 1720000000000 3440000000000 1 2 0.0001 9 1720000000 3440000000 1 2 1720000000000 3440000000000 1 2 0.001 27 1720000000 3440000000 1 2 1720000000000 3440000000000 1 2 0.01 81 1720000000 3440000000 1 2 172000000000 344000000000 1 2 0.10000000000000001 243 1720000000 3440000000 1 2 1720000 3440000 1 2 10 729 1720000000 3440000000 1 2 172000 344000 10 20 
    </InitialState>
  </Model>
  <ListOfTasks>
    <Task key="Task_15" name="Steady-State" type="steadyState" scheduled="false" updateModel="false">
      <Report reference="Report_11" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="JacobianRequested" type="bool" value="1"/>
        <Parameter name="StabilityAnalysisRequested" type="bool" value="1"/>
      </Problem>
      <Method name="Enhanced Newton" type="EnhancedNewton">
        <Parameter name="Resolution" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Derivation Factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Use Newton" type="bool" value="1"/>
        <Parameter name="Use Integration" type="bool" value="1"/>
        <Parameter name="Use Back Integration" type="bool" value="0"/>
        <Parameter name="Accept Negative Concentrations" type="bool" value="0"/>
        <Parameter name="Iteration Limit" type="unsignedInteger" value="50"/>
        <Parameter name="Maximum duration for forward integration" type="unsignedFloat" value="1000000000"/>
        <Parameter name="Maximum duration for backward integration" type="unsignedFloat" value="1000000"/>
        <Parameter name="Target Criterion" type="string" value="Distance and Rate"/>
      </Method>
    </Task>
    <Task key="Task_16" name="Time-Course" type="timeCourse" scheduled="false" updateModel="false">
      <Report reference="Report_12" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100000"/>
        <Parameter name="StepSize" type="float" value="0.00072000000000000005"/>
        <Parameter name="Duration" type="float" value="72"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
      </Problem>
      <Method name="Stochastic (Direct method)" type="Stochastic">
        <Parameter name="Max Internal Steps" type="integer" value="1000000"/>
        <Parameter name="Use Random Seed" type="bool" value="0"/>
        <Parameter name="Random Seed" type="unsignedInteger" value="1"/>
      </Method>
    </Task>
    <Task key="Task_17" name="Scan" type="scan" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="Subtask" type="unsignedInteger" value="1"/>
        <ParameterGroup name="ScanItems">
          <ParameterGroup name="ScanItem">
            <Parameter name="Number of steps" type="unsignedInteger" value="10"/>
            <Parameter name="Type" type="unsignedInteger" value="1"/>
            <Parameter name="Object" type="cn" value="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Reference=InitialVolume"/>
            <Parameter name="Minimum" type="float" value="5.0000000000000004e-16"/>
            <Parameter name="Maximum" type="float" value="2.0000000000000002e-15"/>
            <Parameter name="log" type="bool" value="0"/>
            <Parameter name="Values" type="string" value=""/>
            <Parameter name="Use Values" type="bool" value="0"/>
          </ParameterGroup>
        </ParameterGroup>
        <Parameter name="Output in subtask" type="bool" value="1"/>
        <Parameter name="Adjust initial conditions" type="bool" value="0"/>
        <Parameter name="Continue on Error" type="bool" value="0"/>
      </Problem>
      <Method name="Scan Framework" type="ScanFramework">
      </Method>
    </Task>
    <Task key="Task_18" name="Elementary Flux Modes" type="fluxMode" scheduled="false" updateModel="false">
      <Report reference="Report_13" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="EFM Algorithm" type="EFMAlgorithm">
      </Method>
    </Task>
    <Task key="Task_19" name="Optimization" type="optimization" scheduled="false" updateModel="false">
      <Report reference="Report_14" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Subtask" type="cn" value="CN=Root,Vector=TaskList[Steady-State]"/>
        <ParameterText name="ObjectiveExpression" type="expression">
          
        </ParameterText>
        <Parameter name="Maximize" type="bool" value="0"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
      </Problem>
      <Method name="Random Search" type="RandomSearch">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Iterations" type="unsignedInteger" value="100000"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_20" name="Parameter Estimation" type="parameterFitting" scheduled="false" updateModel="false">
      <Report reference="Report_15" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Maximize" type="bool" value="0"/>
        <Parameter name="Randomize Start Values" type="bool" value="0"/>
        <Parameter name="Calculate Statistics" type="bool" value="1"/>
        <ParameterGroup name="OptimizationItemList">
        </ParameterGroup>
        <ParameterGroup name="OptimizationConstraintList">
        </ParameterGroup>
        <Parameter name="Steady-State" type="cn" value="CN=Root,Vector=TaskList[Steady-State]"/>
        <Parameter name="Time-Course" type="cn" value="CN=Root,Vector=TaskList[Time-Course]"/>
        <Parameter name="Create Parameter Sets" type="bool" value="0"/>
        <Parameter name="Use Time Sens" type="bool" value="0"/>
        <Parameter name="Time-Sens" type="cn" value=""/>
        <ParameterGroup name="Experiment Set">
        </ParameterGroup>
        <ParameterGroup name="Validation Set">
          <Parameter name="Weight" type="unsignedFloat" value="1"/>
          <Parameter name="Threshold" type="unsignedInteger" value="5"/>
        </ParameterGroup>
      </Problem>
      <Method name="Evolutionary Programming" type="EvolutionaryProgram">
        <Parameter name="Log Verbosity" type="unsignedInteger" value="0"/>
        <Parameter name="Number of Generations" type="unsignedInteger" value="200"/>
        <Parameter name="Population Size" type="unsignedInteger" value="20"/>
        <Parameter name="Random Number Generator" type="unsignedInteger" value="1"/>
        <Parameter name="Seed" type="unsignedInteger" value="0"/>
        <Parameter name="Stop after # Stalled Generations" type="unsignedInteger" value="0"/>
      </Method>
    </Task>
    <Task key="Task_21" name="Metabolic Control Analysis" type="metabolicControlAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_16" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="MCA Method (Reder)" type="MCAMethod(Reder)">
        <Parameter name="Modulation Factor" type="unsignedFloat" value="1.0000000000000001e-09"/>
        <Parameter name="Use Reder" type="bool" value="1"/>
        <Parameter name="Use Smallbone" type="bool" value="1"/>
      </Method>
    </Task>
    <Task key="Task_22" name="Lyapunov Exponents" type="lyapunovExponents" scheduled="false" updateModel="false">
      <Report reference="Report_17" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="ExponentNumber" type="unsignedInteger" value="3"/>
        <Parameter name="DivergenceRequested" type="bool" value="1"/>
        <Parameter name="TransientTime" type="float" value="0"/>
      </Problem>
      <Method name="Wolf Method" type="WolfMethod">
        <Parameter name="Orthonormalization Interval" type="unsignedFloat" value="1"/>
        <Parameter name="Overall time" type="unsignedFloat" value="1000"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
      </Method>
    </Task>
    <Task key="Task_23" name="Time Scale Separation Analysis" type="timeScaleSeparationAnalysis" scheduled="false" updateModel="false">
      <Report reference="Report_18" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
      </Problem>
      <Method name="ILDM (LSODA,Deuflhard)" type="TimeScaleSeparation(ILDM,Deuflhard)">
        <Parameter name="Deuflhard Tolerance" type="unsignedFloat" value="0.0001"/>
      </Method>
    </Task>
    <Task key="Task_24" name="Sensitivities" type="sensitivities" scheduled="false" updateModel="false">
      <Report reference="Report_19" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="SubtaskType" type="unsignedInteger" value="1"/>
        <ParameterGroup name="TargetFunctions">
          <Parameter name="SingleObject" type="cn" value=""/>
          <Parameter name="ObjectListType" type="unsignedInteger" value="7"/>
        </ParameterGroup>
        <ParameterGroup name="ListOfVariables">
          <ParameterGroup name="Variables">
            <Parameter name="SingleObject" type="cn" value=""/>
            <Parameter name="ObjectListType" type="unsignedInteger" value="41"/>
          </ParameterGroup>
          <ParameterGroup name="Variables">
            <Parameter name="SingleObject" type="cn" value=""/>
            <Parameter name="ObjectListType" type="unsignedInteger" value="0"/>
          </ParameterGroup>
        </ParameterGroup>
      </Problem>
      <Method name="Sensitivities Method" type="SensitivitiesMethod">
        <Parameter name="Delta factor" type="unsignedFloat" value="0.001"/>
        <Parameter name="Delta minimum" type="unsignedFloat" value="9.9999999999999998e-13"/>
      </Method>
    </Task>
    <Task key="Task_25" name="Moieties" type="moieties" scheduled="false" updateModel="false">
      <Report reference="Report_20" target="" append="1" confirmOverwrite="1"/>
      <Problem>
      </Problem>
      <Method name="Householder Reduction" type="Householder">
      </Method>
    </Task>
    <Task key="Task_26" name="Cross Section" type="crosssection" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <Parameter name="LimitCrossings" type="bool" value="0"/>
        <Parameter name="NumCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitOutTime" type="bool" value="0"/>
        <Parameter name="LimitOutCrossings" type="bool" value="0"/>
        <Parameter name="PositiveDirection" type="bool" value="1"/>
        <Parameter name="NumOutCrossingsLimit" type="unsignedInteger" value="0"/>
        <Parameter name="LimitUntilConvergence" type="bool" value="0"/>
        <Parameter name="ConvergenceTolerance" type="float" value="9.9999999999999995e-07"/>
        <Parameter name="Threshold" type="float" value="0"/>
        <Parameter name="DelayOutputUntilConvergence" type="bool" value="0"/>
        <Parameter name="OutputConvergenceTolerance" type="float" value="9.9999999999999995e-07"/>
        <ParameterText name="TriggerExpression" type="expression">
          
        </ParameterText>
        <Parameter name="SingleVariable" type="cn" value=""/>
      </Problem>
      <Method name="Deterministic (LSODA)" type="Deterministic(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="100000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
    <Task key="Task_27" name="Linear Noise Approximation" type="linearNoiseApproximation" scheduled="false" updateModel="false">
      <Report reference="Report_21" target="" append="1" confirmOverwrite="1"/>
      <Problem>
        <Parameter name="Steady-State" type="key" value="Task_15"/>
      </Problem>
      <Method name="Linear Noise Approximation" type="LinearNoiseApproximation">
      </Method>
    </Task>
    <Task key="Task_28" name="Time-Course Sensitivities" type="timeSensitivities" scheduled="false" updateModel="false">
      <Problem>
        <Parameter name="AutomaticStepSize" type="bool" value="0"/>
        <Parameter name="StepNumber" type="unsignedInteger" value="100"/>
        <Parameter name="StepSize" type="float" value="0.01"/>
        <Parameter name="Duration" type="float" value="1"/>
        <Parameter name="TimeSeriesRequested" type="bool" value="1"/>
        <Parameter name="OutputStartTime" type="float" value="0"/>
        <Parameter name="Output Event" type="bool" value="0"/>
        <Parameter name="Start in Steady State" type="bool" value="0"/>
        <Parameter name="Use Values" type="bool" value="0"/>
        <Parameter name="Values" type="string" value=""/>
        <ParameterGroup name="ListOfParameters">
        </ParameterGroup>
        <ParameterGroup name="ListOfTargets">
        </ParameterGroup>
      </Problem>
      <Method name="LSODA Sensitivities" type="Sensitivities(LSODA)">
        <Parameter name="Integrate Reduced Model" type="bool" value="0"/>
        <Parameter name="Relative Tolerance" type="unsignedFloat" value="9.9999999999999995e-07"/>
        <Parameter name="Absolute Tolerance" type="unsignedFloat" value="9.9999999999999998e-13"/>
        <Parameter name="Max Internal Steps" type="unsignedInteger" value="10000"/>
        <Parameter name="Max Internal Step Size" type="unsignedFloat" value="0"/>
      </Method>
    </Task>
  </ListOfTasks>
  <ListOfReports>
    <Report key="Report_11" name="Steady-State" taskType="steadyState" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Steady-State]"/>
      </Footer>
    </Report>
    <Report key="Report_12" name="Time-Course" taskType="timeCourse" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Time-Course],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Time-Course],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_13" name="Elementary Flux Modes" taskType="fluxMode" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Footer>
        <Object cn="CN=Root,Vector=TaskList[Elementary Flux Modes],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_14" name="Optimization" taskType="optimization" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Problem=Optimization,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Optimization],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_15" name="Parameter Estimation" taskType="parameterFitting" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Description"/>
        <Object cn="String=\[Function Evaluations\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Value\]"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="String=\[Best Parameters\]"/>
      </Header>
      <Body>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Function Evaluations"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Value"/>
        <Object cn="Separator=&#x09;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Problem=Parameter Estimation,Reference=Best Parameters"/>
      </Body>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Parameter Estimation],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_16" name="Metabolic Control Analysis" taskType="metabolicControlAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Metabolic Control Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_17" name="Lyapunov Exponents" taskType="lyapunovExponents" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Lyapunov Exponents],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_18" name="Time Scale Separation Analysis" taskType="timeScaleSeparationAnalysis" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Time Scale Separation Analysis],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_19" name="Sensitivities" taskType="sensitivities" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Sensitivities],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_20" name="Moieties" taskType="moieties" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Moieties],Object=Result"/>
      </Footer>
    </Report>
    <Report key="Report_21" name="Linear Noise Approximation" taskType="linearNoiseApproximation" separator="&#x09;" precision="6">
      <Comment>
        Automatically generated report.
      </Comment>
      <Header>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Description"/>
      </Header>
      <Footer>
        <Object cn="String=&#x0a;"/>
        <Object cn="CN=Root,Vector=TaskList[Linear Noise Approximation],Object=Result"/>
      </Footer>
    </Report>
  </ListOfReports>
  <ListOfPlots>
    <PlotSpecification name="Concentrations, Volumes, and Global Quantity Values" type="Plot2D" active="1" taskTypes="">
      <Parameter name="log X" type="bool" value="0"/>
      <Parameter name="log Y" type="bool" value="0"/>
      <ListOfPlotItems>
        <PlotItem name="[A]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC0]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC0],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC1]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC1],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC2]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC2],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC3]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC3],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC4]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC4],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC5]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC5],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[C6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[C6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[AC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[AC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B1iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B1iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B1iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B1iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A1B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A1B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
        <PlotItem name="[A2B2iC6]" type="Curve2D">
          <Parameter name="Line type" type="unsignedInteger" value="0"/>
          <Parameter name="Line subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Line width" type="unsignedFloat" value="1"/>
          <Parameter name="Symbol subtype" type="unsignedInteger" value="0"/>
          <Parameter name="Color" type="string" value="auto"/>
          <Parameter name="Recording Activity" type="string" value="during"/>
          <ListOfChannels>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Reference=Time"/>
            <ChannelSpec cn="CN=Root,Model=Zwicker et al 2010 full PPC in vitro model,Vector=Compartments[ecoli],Vector=Metabolites[A2B2iC6],Reference=Concentration"/>
          </ListOfChannels>
        </PlotItem>
      </ListOfPlotItems>
    </PlotSpecification>
  </ListOfPlots>
  <GUI>
  </GUI>
  <SBMLReference file="zwicker.xml">
    <SBMLMap SBMLid="A" COPASIkey="Metabolite_0"/>
    <SBMLMap SBMLid="A1B1iC0" COPASIkey="Metabolite_7"/>
    <SBMLMap SBMLid="A1B1iC1" COPASIkey="Metabolite_15"/>
    <SBMLMap SBMLid="A1B1iC2" COPASIkey="Metabolite_23"/>
    <SBMLMap SBMLid="A1B1iC3" COPASIkey="Metabolite_31"/>
    <SBMLMap SBMLid="A1B1iC4" COPASIkey="Metabolite_39"/>
    <SBMLMap SBMLid="A1B1iC5" COPASIkey="Metabolite_47"/>
    <SBMLMap SBMLid="A1B1iC6" COPASIkey="Metabolite_55"/>
    <SBMLMap SBMLid="A1B2iC0" COPASIkey="Metabolite_8"/>
    <SBMLMap SBMLid="A1B2iC1" COPASIkey="Metabolite_16"/>
    <SBMLMap SBMLid="A1B2iC2" COPASIkey="Metabolite_24"/>
    <SBMLMap SBMLid="A1B2iC3" COPASIkey="Metabolite_32"/>
    <SBMLMap SBMLid="A1B2iC4" COPASIkey="Metabolite_40"/>
    <SBMLMap SBMLid="A1B2iC5" COPASIkey="Metabolite_48"/>
    <SBMLMap SBMLid="A1B2iC6" COPASIkey="Metabolite_56"/>
    <SBMLMap SBMLid="A2B2iC0" COPASIkey="Metabolite_9"/>
    <SBMLMap SBMLid="A2B2iC1" COPASIkey="Metabolite_17"/>
    <SBMLMap SBMLid="A2B2iC2" COPASIkey="Metabolite_25"/>
    <SBMLMap SBMLid="A2B2iC3" COPASIkey="Metabolite_33"/>
    <SBMLMap SBMLid="A2B2iC4" COPASIkey="Metabolite_41"/>
    <SBMLMap SBMLid="A2B2iC5" COPASIkey="Metabolite_49"/>
    <SBMLMap SBMLid="A2B2iC6" COPASIkey="Metabolite_57"/>
    <SBMLMap SBMLid="AC0" COPASIkey="Metabolite_4"/>
    <SBMLMap SBMLid="AC1" COPASIkey="Metabolite_12"/>
    <SBMLMap SBMLid="AC2" COPASIkey="Metabolite_20"/>
    <SBMLMap SBMLid="AC3" COPASIkey="Metabolite_28"/>
    <SBMLMap SBMLid="AC4" COPASIkey="Metabolite_36"/>
    <SBMLMap SBMLid="AC5" COPASIkey="Metabolite_44"/>
    <SBMLMap SBMLid="AC6" COPASIkey="Metabolite_52"/>
    <SBMLMap SBMLid="B" COPASIkey="Metabolite_1"/>
    <SBMLMap SBMLid="B1iC0" COPASIkey="Metabolite_5"/>
    <SBMLMap SBMLid="B1iC1" COPASIkey="Metabolite_13"/>
    <SBMLMap SBMLid="B1iC2" COPASIkey="Metabolite_21"/>
    <SBMLMap SBMLid="B1iC3" COPASIkey="Metabolite_29"/>
    <SBMLMap SBMLid="B1iC4" COPASIkey="Metabolite_37"/>
    <SBMLMap SBMLid="B1iC5" COPASIkey="Metabolite_45"/>
    <SBMLMap SBMLid="B1iC6" COPASIkey="Metabolite_53"/>
    <SBMLMap SBMLid="B2iC0" COPASIkey="Metabolite_6"/>
    <SBMLMap SBMLid="B2iC1" COPASIkey="Metabolite_14"/>
    <SBMLMap SBMLid="B2iC2" COPASIkey="Metabolite_22"/>
    <SBMLMap SBMLid="B2iC3" COPASIkey="Metabolite_30"/>
    <SBMLMap SBMLid="B2iC4" COPASIkey="Metabolite_38"/>
    <SBMLMap SBMLid="B2iC5" COPASIkey="Metabolite_46"/>
    <SBMLMap SBMLid="B2iC6" COPASIkey="Metabolite_54"/>
    <SBMLMap SBMLid="C0" COPASIkey="Metabolite_2"/>
    <SBMLMap SBMLid="C1" COPASIkey="Metabolite_10"/>
    <SBMLMap SBMLid="C2" COPASIkey="Metabolite_18"/>
    <SBMLMap SBMLid="C3" COPASIkey="Metabolite_26"/>
    <SBMLMap SBMLid="C4" COPASIkey="Metabolite_34"/>
    <SBMLMap SBMLid="C5" COPASIkey="Metabolite_42"/>
    <SBMLMap SBMLid="C6" COPASIkey="Metabolite_50"/>
    <SBMLMap SBMLid="R10_0b" COPASIkey="Reaction_129"/>
    <SBMLMap SBMLid="R10_0f" COPASIkey="Reaction_128"/>
    <SBMLMap SBMLid="R10_1b" COPASIkey="Reaction_131"/>
    <SBMLMap SBMLid="R10_1f" COPASIkey="Reaction_130"/>
    <SBMLMap SBMLid="R10_2b" COPASIkey="Reaction_133"/>
    <SBMLMap SBMLid="R10_2f" COPASIkey="Reaction_132"/>
    <SBMLMap SBMLid="R10_3b" COPASIkey="Reaction_135"/>
    <SBMLMap SBMLid="R10_3f" COPASIkey="Reaction_134"/>
    <SBMLMap SBMLid="R10_4b" COPASIkey="Reaction_137"/>
    <SBMLMap SBMLid="R10_4f" COPASIkey="Reaction_136"/>
    <SBMLMap SBMLid="R10_5b" COPASIkey="Reaction_139"/>
    <SBMLMap SBMLid="R10_5f" COPASIkey="Reaction_138"/>
    <SBMLMap SBMLid="R11_0b" COPASIkey="Reaction_141"/>
    <SBMLMap SBMLid="R11_0f" COPASIkey="Reaction_140"/>
    <SBMLMap SBMLid="R11_1b" COPASIkey="Reaction_143"/>
    <SBMLMap SBMLid="R11_1f" COPASIkey="Reaction_142"/>
    <SBMLMap SBMLid="R11_2b" COPASIkey="Reaction_145"/>
    <SBMLMap SBMLid="R11_2f" COPASIkey="Reaction_144"/>
    <SBMLMap SBMLid="R11_3b" COPASIkey="Reaction_147"/>
    <SBMLMap SBMLid="R11_3f" COPASIkey="Reaction_146"/>
    <SBMLMap SBMLid="R11_4b" COPASIkey="Reaction_149"/>
    <SBMLMap SBMLid="R11_4f" COPASIkey="Reaction_148"/>
    <SBMLMap SBMLid="R11_5b" COPASIkey="Reaction_151"/>
    <SBMLMap SBMLid="R11_5f" COPASIkey="Reaction_150"/>
    <SBMLMap SBMLid="R12_0b" COPASIkey="Reaction_153"/>
    <SBMLMap SBMLid="R12_0f" COPASIkey="Reaction_152"/>
    <SBMLMap SBMLid="R12_1b" COPASIkey="Reaction_155"/>
    <SBMLMap SBMLid="R12_1f" COPASIkey="Reaction_154"/>
    <SBMLMap SBMLid="R12_2b" COPASIkey="Reaction_157"/>
    <SBMLMap SBMLid="R12_2f" COPASIkey="Reaction_156"/>
    <SBMLMap SBMLid="R12_3b" COPASIkey="Reaction_159"/>
    <SBMLMap SBMLid="R12_3f" COPASIkey="Reaction_158"/>
    <SBMLMap SBMLid="R12_4b" COPASIkey="Reaction_161"/>
    <SBMLMap SBMLid="R12_4f" COPASIkey="Reaction_160"/>
    <SBMLMap SBMLid="R12_5b" COPASIkey="Reaction_163"/>
    <SBMLMap SBMLid="R12_5f" COPASIkey="Reaction_162"/>
    <SBMLMap SBMLid="R13_0b" COPASIkey="Reaction_165"/>
    <SBMLMap SBMLid="R13_0f" COPASIkey="Reaction_164"/>
    <SBMLMap SBMLid="R13_1b" COPASIkey="Reaction_167"/>
    <SBMLMap SBMLid="R13_1f" COPASIkey="Reaction_166"/>
    <SBMLMap SBMLid="R13_2b" COPASIkey="Reaction_169"/>
    <SBMLMap SBMLid="R13_2f" COPASIkey="Reaction_168"/>
    <SBMLMap SBMLid="R13_3b" COPASIkey="Reaction_171"/>
    <SBMLMap SBMLid="R13_3f" COPASIkey="Reaction_170"/>
    <SBMLMap SBMLid="R13_4b" COPASIkey="Reaction_173"/>
    <SBMLMap SBMLid="R13_4f" COPASIkey="Reaction_172"/>
    <SBMLMap SBMLid="R13_5b" COPASIkey="Reaction_175"/>
    <SBMLMap SBMLid="R13_5f" COPASIkey="Reaction_174"/>
    <SBMLMap SBMLid="R14_0b" COPASIkey="Reaction_177"/>
    <SBMLMap SBMLid="R14_0f" COPASIkey="Reaction_176"/>
    <SBMLMap SBMLid="R14_1b" COPASIkey="Reaction_179"/>
    <SBMLMap SBMLid="R14_1f" COPASIkey="Reaction_178"/>
    <SBMLMap SBMLid="R14_2b" COPASIkey="Reaction_181"/>
    <SBMLMap SBMLid="R14_2f" COPASIkey="Reaction_180"/>
    <SBMLMap SBMLid="R14_3b" COPASIkey="Reaction_183"/>
    <SBMLMap SBMLid="R14_3f" COPASIkey="Reaction_182"/>
    <SBMLMap SBMLid="R14_4b" COPASIkey="Reaction_185"/>
    <SBMLMap SBMLid="R14_4f" COPASIkey="Reaction_184"/>
    <SBMLMap SBMLid="R14_5b" COPASIkey="Reaction_187"/>
    <SBMLMap SBMLid="R14_5f" COPASIkey="Reaction_186"/>
    <SBMLMap SBMLid="R1_0b" COPASIkey="Reaction_1"/>
    <SBMLMap SBMLid="R1_0f" COPASIkey="Reaction_0"/>
    <SBMLMap SBMLid="R1_1b" COPASIkey="Reaction_3"/>
    <SBMLMap SBMLid="R1_1f" COPASIkey="Reaction_2"/>
    <SBMLMap SBMLid="R1_2b" COPASIkey="Reaction_5"/>
    <SBMLMap SBMLid="R1_2f" COPASIkey="Reaction_4"/>
    <SBMLMap SBMLid="R1_3b" COPASIkey="Reaction_7"/>
    <SBMLMap SBMLid="R1_3f" COPASIkey="Reaction_6"/>
    <SBMLMap SBMLid="R1_4b" COPASIkey="Reaction_9"/>
    <SBMLMap SBMLid="R1_4f" COPASIkey="Reaction_8"/>
    <SBMLMap SBMLid="R1_5b" COPASIkey="Reaction_11"/>
    <SBMLMap SBMLid="R1_5f" COPASIkey="Reaction_10"/>
    <SBMLMap SBMLid="R1_6b" COPASIkey="Reaction_13"/>
    <SBMLMap SBMLid="R1_6f" COPASIkey="Reaction_12"/>
    <SBMLMap SBMLid="R2_0b" COPASIkey="Reaction_15"/>
    <SBMLMap SBMLid="R2_0f" COPASIkey="Reaction_14"/>
    <SBMLMap SBMLid="R2_0irrev" COPASIkey="Reaction_16"/>
    <SBMLMap SBMLid="R2_1b" COPASIkey="Reaction_18"/>
    <SBMLMap SBMLid="R2_1f" COPASIkey="Reaction_17"/>
    <SBMLMap SBMLid="R2_1irrev" COPASIkey="Reaction_19"/>
    <SBMLMap SBMLid="R2_2b" COPASIkey="Reaction_21"/>
    <SBMLMap SBMLid="R2_2f" COPASIkey="Reaction_20"/>
    <SBMLMap SBMLid="R2_2irrev" COPASIkey="Reaction_22"/>
    <SBMLMap SBMLid="R2_3b" COPASIkey="Reaction_24"/>
    <SBMLMap SBMLid="R2_3f" COPASIkey="Reaction_23"/>
    <SBMLMap SBMLid="R2_3irrev" COPASIkey="Reaction_25"/>
    <SBMLMap SBMLid="R2_4b" COPASIkey="Reaction_27"/>
    <SBMLMap SBMLid="R2_4f" COPASIkey="Reaction_26"/>
    <SBMLMap SBMLid="R2_4irrev" COPASIkey="Reaction_28"/>
    <SBMLMap SBMLid="R2_5b" COPASIkey="Reaction_30"/>
    <SBMLMap SBMLid="R2_5f" COPASIkey="Reaction_29"/>
    <SBMLMap SBMLid="R2_5irrev" COPASIkey="Reaction_31"/>
    <SBMLMap SBMLid="R2_6b" COPASIkey="Reaction_33"/>
    <SBMLMap SBMLid="R2_6f" COPASIkey="Reaction_32"/>
    <SBMLMap SBMLid="R3_0b" COPASIkey="Reaction_35"/>
    <SBMLMap SBMLid="R3_0f" COPASIkey="Reaction_34"/>
    <SBMLMap SBMLid="R3_1b" COPASIkey="Reaction_37"/>
    <SBMLMap SBMLid="R3_1f" COPASIkey="Reaction_36"/>
    <SBMLMap SBMLid="R3_2b" COPASIkey="Reaction_39"/>
    <SBMLMap SBMLid="R3_2f" COPASIkey="Reaction_38"/>
    <SBMLMap SBMLid="R3_3b" COPASIkey="Reaction_41"/>
    <SBMLMap SBMLid="R3_3f" COPASIkey="Reaction_40"/>
    <SBMLMap SBMLid="R3_4b" COPASIkey="Reaction_43"/>
    <SBMLMap SBMLid="R3_4f" COPASIkey="Reaction_42"/>
    <SBMLMap SBMLid="R3_5b" COPASIkey="Reaction_45"/>
    <SBMLMap SBMLid="R3_5f" COPASIkey="Reaction_44"/>
    <SBMLMap SBMLid="R3_6b" COPASIkey="Reaction_47"/>
    <SBMLMap SBMLid="R3_6f" COPASIkey="Reaction_46"/>
    <SBMLMap SBMLid="R4_0b" COPASIkey="Reaction_49"/>
    <SBMLMap SBMLid="R4_0f" COPASIkey="Reaction_48"/>
    <SBMLMap SBMLid="R4_1b" COPASIkey="Reaction_51"/>
    <SBMLMap SBMLid="R4_1f" COPASIkey="Reaction_50"/>
    <SBMLMap SBMLid="R4_2b" COPASIkey="Reaction_53"/>
    <SBMLMap SBMLid="R4_2f" COPASIkey="Reaction_52"/>
    <SBMLMap SBMLid="R4_3b" COPASIkey="Reaction_55"/>
    <SBMLMap SBMLid="R4_3f" COPASIkey="Reaction_54"/>
    <SBMLMap SBMLid="R4_4b" COPASIkey="Reaction_57"/>
    <SBMLMap SBMLid="R4_4f" COPASIkey="Reaction_56"/>
    <SBMLMap SBMLid="R4_5b" COPASIkey="Reaction_59"/>
    <SBMLMap SBMLid="R4_5f" COPASIkey="Reaction_58"/>
    <SBMLMap SBMLid="R4_6b" COPASIkey="Reaction_61"/>
    <SBMLMap SBMLid="R4_6f" COPASIkey="Reaction_60"/>
    <SBMLMap SBMLid="R5_0b" COPASIkey="Reaction_63"/>
    <SBMLMap SBMLid="R5_0f" COPASIkey="Reaction_62"/>
    <SBMLMap SBMLid="R5_1b" COPASIkey="Reaction_65"/>
    <SBMLMap SBMLid="R5_1f" COPASIkey="Reaction_64"/>
    <SBMLMap SBMLid="R5_2b" COPASIkey="Reaction_67"/>
    <SBMLMap SBMLid="R5_2f" COPASIkey="Reaction_66"/>
    <SBMLMap SBMLid="R5_3b" COPASIkey="Reaction_69"/>
    <SBMLMap SBMLid="R5_3f" COPASIkey="Reaction_68"/>
    <SBMLMap SBMLid="R5_4b" COPASIkey="Reaction_71"/>
    <SBMLMap SBMLid="R5_4f" COPASIkey="Reaction_70"/>
    <SBMLMap SBMLid="R5_5b" COPASIkey="Reaction_73"/>
    <SBMLMap SBMLid="R5_5f" COPASIkey="Reaction_72"/>
    <SBMLMap SBMLid="R5_6b" COPASIkey="Reaction_75"/>
    <SBMLMap SBMLid="R5_6f" COPASIkey="Reaction_74"/>
    <SBMLMap SBMLid="R6_0b" COPASIkey="Reaction_77"/>
    <SBMLMap SBMLid="R6_0f" COPASIkey="Reaction_76"/>
    <SBMLMap SBMLid="R6_1b" COPASIkey="Reaction_79"/>
    <SBMLMap SBMLid="R6_1f" COPASIkey="Reaction_78"/>
    <SBMLMap SBMLid="R6_2b" COPASIkey="Reaction_81"/>
    <SBMLMap SBMLid="R6_2f" COPASIkey="Reaction_80"/>
    <SBMLMap SBMLid="R6_3b" COPASIkey="Reaction_83"/>
    <SBMLMap SBMLid="R6_3f" COPASIkey="Reaction_82"/>
    <SBMLMap SBMLid="R6_4b" COPASIkey="Reaction_85"/>
    <SBMLMap SBMLid="R6_4f" COPASIkey="Reaction_84"/>
    <SBMLMap SBMLid="R6_5b" COPASIkey="Reaction_87"/>
    <SBMLMap SBMLid="R6_5f" COPASIkey="Reaction_86"/>
    <SBMLMap SBMLid="R6_6b" COPASIkey="Reaction_89"/>
    <SBMLMap SBMLid="R6_6f" COPASIkey="Reaction_88"/>
    <SBMLMap SBMLid="R7_0b" COPASIkey="Reaction_91"/>
    <SBMLMap SBMLid="R7_0f" COPASIkey="Reaction_90"/>
    <SBMLMap SBMLid="R7_1b" COPASIkey="Reaction_93"/>
    <SBMLMap SBMLid="R7_1f" COPASIkey="Reaction_92"/>
    <SBMLMap SBMLid="R7_2b" COPASIkey="Reaction_95"/>
    <SBMLMap SBMLid="R7_2f" COPASIkey="Reaction_94"/>
    <SBMLMap SBMLid="R7_3b" COPASIkey="Reaction_97"/>
    <SBMLMap SBMLid="R7_3f" COPASIkey="Reaction_96"/>
    <SBMLMap SBMLid="R7_4b" COPASIkey="Reaction_99"/>
    <SBMLMap SBMLid="R7_4f" COPASIkey="Reaction_98"/>
    <SBMLMap SBMLid="R7_5b" COPASIkey="Reaction_101"/>
    <SBMLMap SBMLid="R7_5f" COPASIkey="Reaction_100"/>
    <SBMLMap SBMLid="R7_6b" COPASIkey="Reaction_103"/>
    <SBMLMap SBMLid="R7_6f" COPASIkey="Reaction_102"/>
    <SBMLMap SBMLid="R8_0b" COPASIkey="Reaction_105"/>
    <SBMLMap SBMLid="R8_0f" COPASIkey="Reaction_104"/>
    <SBMLMap SBMLid="R8_1b" COPASIkey="Reaction_107"/>
    <SBMLMap SBMLid="R8_1f" COPASIkey="Reaction_106"/>
    <SBMLMap SBMLid="R8_2b" COPASIkey="Reaction_109"/>
    <SBMLMap SBMLid="R8_2f" COPASIkey="Reaction_108"/>
    <SBMLMap SBMLid="R8_3b" COPASIkey="Reaction_111"/>
    <SBMLMap SBMLid="R8_3f" COPASIkey="Reaction_110"/>
    <SBMLMap SBMLid="R8_4b" COPASIkey="Reaction_113"/>
    <SBMLMap SBMLid="R8_4f" COPASIkey="Reaction_112"/>
    <SBMLMap SBMLid="R8_5b" COPASIkey="Reaction_115"/>
    <SBMLMap SBMLid="R8_5f" COPASIkey="Reaction_114"/>
    <SBMLMap SBMLid="R9_0b" COPASIkey="Reaction_117"/>
    <SBMLMap SBMLid="R9_0f" COPASIkey="Reaction_116"/>
    <SBMLMap SBMLid="R9_1b" COPASIkey="Reaction_119"/>
    <SBMLMap SBMLid="R9_1f" COPASIkey="Reaction_118"/>
    <SBMLMap SBMLid="R9_2b" COPASIkey="Reaction_121"/>
    <SBMLMap SBMLid="R9_2f" COPASIkey="Reaction_120"/>
    <SBMLMap SBMLid="R9_3b" COPASIkey="Reaction_123"/>
    <SBMLMap SBMLid="R9_3f" COPASIkey="Reaction_122"/>
    <SBMLMap SBMLid="R9_4b" COPASIkey="Reaction_125"/>
    <SBMLMap SBMLid="R9_4f" COPASIkey="Reaction_124"/>
    <SBMLMap SBMLid="R9_5b" COPASIkey="Reaction_127"/>
    <SBMLMap SBMLid="R9_5f" COPASIkey="Reaction_126"/>
    <SBMLMap SBMLid="bi" COPASIkey="ModelValue_0"/>
    <SBMLMap SBMLid="ecoli" COPASIkey="Compartment_0"/>
    <SBMLMap SBMLid="f0" COPASIkey="ModelValue_7"/>
    <SBMLMap SBMLid="f1" COPASIkey="ModelValue_17"/>
    <SBMLMap SBMLid="f2" COPASIkey="ModelValue_27"/>
    <SBMLMap SBMLid="f3" COPASIkey="ModelValue_37"/>
    <SBMLMap SBMLid="f4" COPASIkey="ModelValue_47"/>
    <SBMLMap SBMLid="f5" COPASIkey="ModelValue_57"/>
    <SBMLMap SBMLid="f6" COPASIkey="ModelValue_67"/>
    <SBMLMap SBMLid="iC0" COPASIkey="Metabolite_3"/>
    <SBMLMap SBMLid="iC1" COPASIkey="Metabolite_11"/>
    <SBMLMap SBMLid="iC2" COPASIkey="Metabolite_19"/>
    <SBMLMap SBMLid="iC3" COPASIkey="Metabolite_27"/>
    <SBMLMap SBMLid="iC4" COPASIkey="Metabolite_35"/>
    <SBMLMap SBMLid="iC5" COPASIkey="Metabolite_43"/>
    <SBMLMap SBMLid="iC6" COPASIkey="Metabolite_51"/>
    <SBMLMap SBMLid="ik0_Ab" COPASIkey="ModelValue_15"/>
    <SBMLMap SBMLid="ik0_Ab__2" COPASIkey="ModelValue_16"/>
    <SBMLMap SBMLid="ik0_Af" COPASIkey="ModelValue_13"/>
    <SBMLMap SBMLid="ik0_Af__2" COPASIkey="ModelValue_14"/>
    <SBMLMap SBMLid="ik0_Bb" COPASIkey="ModelValue_11"/>
    <SBMLMap SBMLid="ik0_Bb__2" COPASIkey="ModelValue_12"/>
    <SBMLMap SBMLid="ik0_Bf" COPASIkey="ModelValue_9"/>
    <SBMLMap SBMLid="ik0_Bf__2" COPASIkey="ModelValue_10"/>
    <SBMLMap SBMLid="ik1_Ab" COPASIkey="ModelValue_25"/>
    <SBMLMap SBMLid="ik1_Ab__2" COPASIkey="ModelValue_26"/>
    <SBMLMap SBMLid="ik1_Af" COPASIkey="ModelValue_23"/>
    <SBMLMap SBMLid="ik1_Af__2" COPASIkey="ModelValue_24"/>
    <SBMLMap SBMLid="ik1_Bb" COPASIkey="ModelValue_21"/>
    <SBMLMap SBMLid="ik1_Bb__2" COPASIkey="ModelValue_22"/>
    <SBMLMap SBMLid="ik1_Bf" COPASIkey="ModelValue_19"/>
    <SBMLMap SBMLid="ik1_Bf__2" COPASIkey="ModelValue_20"/>
    <SBMLMap SBMLid="ik2_Ab" COPASIkey="ModelValue_35"/>
    <SBMLMap SBMLid="ik2_Ab__2" COPASIkey="ModelValue_36"/>
    <SBMLMap SBMLid="ik2_Af" COPASIkey="ModelValue_33"/>
    <SBMLMap SBMLid="ik2_Af__2" COPASIkey="ModelValue_34"/>
    <SBMLMap SBMLid="ik2_Bb" COPASIkey="ModelValue_31"/>
    <SBMLMap SBMLid="ik2_Bb__2" COPASIkey="ModelValue_32"/>
    <SBMLMap SBMLid="ik2_Bf" COPASIkey="ModelValue_29"/>
    <SBMLMap SBMLid="ik2_Bf__2" COPASIkey="ModelValue_30"/>
    <SBMLMap SBMLid="ik3_Ab" COPASIkey="ModelValue_45"/>
    <SBMLMap SBMLid="ik3_Ab__2" COPASIkey="ModelValue_46"/>
    <SBMLMap SBMLid="ik3_Af" COPASIkey="ModelValue_43"/>
    <SBMLMap SBMLid="ik3_Af__2" COPASIkey="ModelValue_44"/>
    <SBMLMap SBMLid="ik3_Bb" COPASIkey="ModelValue_41"/>
    <SBMLMap SBMLid="ik3_Bb__2" COPASIkey="ModelValue_42"/>
    <SBMLMap SBMLid="ik3_Bf" COPASIkey="ModelValue_39"/>
    <SBMLMap SBMLid="ik3_Bf__2" COPASIkey="ModelValue_40"/>
    <SBMLMap SBMLid="ik4_Ab" COPASIkey="ModelValue_55"/>
    <SBMLMap SBMLid="ik4_Ab__2" COPASIkey="ModelValue_56"/>
    <SBMLMap SBMLid="ik4_Af" COPASIkey="ModelValue_53"/>
    <SBMLMap SBMLid="ik4_Af__2" COPASIkey="ModelValue_54"/>
    <SBMLMap SBMLid="ik4_Bb" COPASIkey="ModelValue_51"/>
    <SBMLMap SBMLid="ik4_Bb__2" COPASIkey="ModelValue_52"/>
    <SBMLMap SBMLid="ik4_Bf" COPASIkey="ModelValue_49"/>
    <SBMLMap SBMLid="ik4_Bf__2" COPASIkey="ModelValue_50"/>
    <SBMLMap SBMLid="ik5_Ab" COPASIkey="ModelValue_65"/>
    <SBMLMap SBMLid="ik5_Ab__2" COPASIkey="ModelValue_66"/>
    <SBMLMap SBMLid="ik5_Af" COPASIkey="ModelValue_63"/>
    <SBMLMap SBMLid="ik5_Af__2" COPASIkey="ModelValue_64"/>
    <SBMLMap SBMLid="ik5_Bb" COPASIkey="ModelValue_61"/>
    <SBMLMap SBMLid="ik5_Bb__2" COPASIkey="ModelValue_62"/>
    <SBMLMap SBMLid="ik5_Bf" COPASIkey="ModelValue_59"/>
    <SBMLMap SBMLid="ik5_Bf__2" COPASIkey="ModelValue_60"/>
    <SBMLMap SBMLid="ik6_Ab" COPASIkey="ModelValue_75"/>
    <SBMLMap SBMLid="ik6_Ab__2" COPASIkey="ModelValue_76"/>
    <SBMLMap SBMLid="ik6_Af" COPASIkey="ModelValue_73"/>
    <SBMLMap SBMLid="ik6_Af__2" COPASIkey="ModelValue_74"/>
    <SBMLMap SBMLid="ik6_Bb" COPASIkey="ModelValue_71"/>
    <SBMLMap SBMLid="ik6_Bb__2" COPASIkey="ModelValue_72"/>
    <SBMLMap SBMLid="ik6_Bf" COPASIkey="ModelValue_69"/>
    <SBMLMap SBMLid="ik6_Bf__2" COPASIkey="ModelValue_70"/>
    <SBMLMap SBMLid="ik_dps" COPASIkey="ModelValue_6"/>
    <SBMLMap SBMLid="ik_ps" COPASIkey="ModelValue_5"/>
    <SBMLMap SBMLid="k0_Ab" COPASIkey="ModelValue_8"/>
    <SBMLMap SBMLid="k1_Ab" COPASIkey="ModelValue_18"/>
    <SBMLMap SBMLid="k2_Ab" COPASIkey="ModelValue_28"/>
    <SBMLMap SBMLid="k3_Ab" COPASIkey="ModelValue_38"/>
    <SBMLMap SBMLid="k4_Ab" COPASIkey="ModelValue_48"/>
    <SBMLMap SBMLid="k5_Ab" COPASIkey="ModelValue_58"/>
    <SBMLMap SBMLid="k6_Ab" COPASIkey="ModelValue_68"/>
    <SBMLMap SBMLid="k_dps" COPASIkey="ModelValue_4"/>
    <SBMLMap SBMLid="k_pf" COPASIkey="ModelValue_2"/>
    <SBMLMap SBMLid="k_ps" COPASIkey="ModelValue_3"/>
    <SBMLMap SBMLid="ki_Af" COPASIkey="ModelValue_1"/>
  </SBMLReference>
  <ListOfUnitDefinitions>
    <UnitDefinition key="Unit_1" name="meter" symbol="m">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_0">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        m
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_5" name="second" symbol="s">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_4">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        s
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_13" name="Avogadro" symbol="Avogadro">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_12">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_15" name="dimensionless" symbol="1">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_14">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        1
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_17" name="item" symbol="#">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_16">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        #
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_35" name="liter" symbol="l">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_34">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        0.001*m^3
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_41" name="mole" symbol="mol">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_40">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        Avogadro*#
      </Expression>
    </UnitDefinition>
    <UnitDefinition key="Unit_67" name="hour" symbol="h">
      <MiriamAnnotation>
<rdf:RDF
xmlns:dcterms="http://purl.org/dc/terms/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<rdf:Description rdf:about="#Unit_66">
<dcterms:created>
<rdf:Description>
<dcterms:W3CDTF>2021-02-08T21:31:52Z</dcterms:W3CDTF>
</rdf:Description>
</dcterms:created>
</rdf:Description>
</rdf:RDF>
      </MiriamAnnotation>
      <Expression>
        3600*s
      </Expression>
    </UnitDefinition>
  </ListOfUnitDefinitions>
</COPASI>

README

How to use the Zwicker 2010 PPC in vitro model
to recreate Fig. S1A from the paper

1. Build the SBML model

python3 fullPPC.py

-- note that the python environment needs libsbml and matplotlib installed

2. Open the SBML model in COPASI

File --> Import SBML --> fullPPC.xml

3. Set hour time scale in COPASI

In the tree on left hand side:
COPASI - Model - Time Unit  set to h

4. Set compartment volume in COPASI

COPASI - Model - Biochemical - Compartments - Initial Size Column
	1e-15 (litres) for a 1um^3 volume compartment
	1e-16 (litres) for a 0.1um^3 volume compartment

5. Save the COPASI model file (.cps)
	(_fullPPC.cps is one I made earlier!)

6. Run the simulation in COPASI

COPASI - Tasks - Time course
	Duration [h] set to 72
	Intervals set to 100000
	Method combo set to: Stochastic (Direct Method)
	Output Assistant Button (bottom right), Choose concentrations... as the template, Create
	Run

7. Save the data
	On the trajectory window, press "Save Data" button above species

	Save as volume1.txt, if the compartment volume is 1e-15 litres
	Save as volume01.txt, if the compartment volume is 1e-16 litres

8. Plot FigS1A from the Zwicker paper

open volume1.txt, ans edit first line so # Time becomes #Time with no space
open volume01.txt, ans edit first line so # Time becomes #Time with no space

python3 figS1A.py









	



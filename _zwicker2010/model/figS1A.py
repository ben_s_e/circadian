"""

Reproducing Fig S1A of Zwicker 2010

NOTE: Make the # Time  = #Time on the first line of .txt exported by COPASI

Expects two data file from COPASI in this directory:
	volume1.txt			(sim volume is 1um3 = 1e-15 litres)
	volume01.txt		(sim volume is 0.1um3 = 1e-16 litres)

"""

import csv
import matplotlib.pyplot as plt


def read_csv(filename) :

	variables = {}
	values = {}

	with open(filename) as csv_file:
		csv_reader = csv.reader(csv_file, delimiter='\t')
		line_count = 0
		for row in csv_reader:
			if line_count == 0:
				# make row headings the keys of the dictionary
				for i, heading in enumerate(row) :
					variables[i] = str(heading)
					values[i] = []

				line_count += 1
			else:
				# add data to dictionary
				for i, val in enumerate(row) :
					if val == "" :
						values[i].append(0)
					else :
						values[i].append(float(val))

				line_count += 1

	return variables, values



def find_species_index(species, variables) :

	for v in variables :
		if variables[v] == species :
			return v

	return -1



def calc_p(variables, values, CT) :

	# 1. get total conc of each Ci species
	LISTS = {}

	for i in range(1,6+1) :		# i = 1,2,3,4,5,6  (NOT 0!!)

		# get all species that have i stores phosphorylated
		# and add their concentration trajectories, element by element
		# (so at each time, the total conc of species with i sites phosphorylated is known)
		
		L = None

		for v in variables :

			species = variables[v]

			if ("C%d" % (i)) in species :	# species is a KaiC hexamer, in some configuration, with i sites phosphorylated

				# get list of concs, for this species
				concs = values[find_species_index(species, variables)]

				# add this list, element by element, to the total list
				if L == None :
					L = concs[:] 	# copy by value
				else :
					L = [sum(x) for x in zip(concs,L)]
						# add concs to existing cumulative list
						# element wise sum of two lists
						# https://stackoverflow.com/questions/19261747/sum-of-n-lists-element-wise-python

		LISTS["list%d" % (i)] = L

	# 2. multiply items in list 1 by 1, items in list 2 by 2, items in list 3 by 3 etc...

	for i in range(1,6+1) :

		LISTS["list%d" % (i)] = [x * i for x in LISTS["list%d" % (i)]]

	# 3. now make final list, which is numerator of p fraction at each time
	NUMERATOR = [sum(x) for x in zip(LISTS["list1"],LISTS["list2"],LISTS["list3"], \
		LISTS["list4"],LISTS["list5"],LISTS["list6"])]

	# 4. divide each element by 6CT, to get p
	p = [x / (6 * CT) for x in NUMERATOR]

	return p






if __name__ == "__main__" :

	# total KaiC hexamer concentration
	CT = 0.58e-6   # M  = 0.58uM

	# get traj data
	variables, values = read_csv("volume1.txt")
	time = values[find_species_index("#Time", variables)]
	p_vol1 = calc_p(variables, values, CT)

	variables, values = read_csv("volume01.txt")
	time = values[find_species_index("#Time", variables)]
	p_vol01 = calc_p(variables, values, CT)

	# plot

	plt.figure()
	plt.plot(time, p_vol1, label="Vol = 1um3", color="xkcd:red")
	plt.plot(time, p_vol01, label="Vol = 0.1um3", color="xkcd:green")

	plt.xlabel("Time (hours)")
	plt.ylabel("p")

	plt.xticks([0,6,12,18,24,30,36,42,48,54,60,66,72])
	plt.legend()
	plt.show()





"""Generates SBML model of the Zwicker in vitro PPC

The Zwicker in vitro PPC contains too many reactions to enter manually in COPASI, 
so I algorithmically generate the SBML file here.

Reversible reactions are added as two separate irreversible reactions, as
its hardly documented how to make reversible reactions with python libsbml

This file follows template
http://model.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/create_simple_model_8py-example.html
http://model.caltech.edu/software/libsbml/5.18.0/docs/formatted/python-api/libsbml-python-creating-model.html

Requirements: 
	pip install python-libsbml

Ben Shirt-Ediss, Feb 2021
"""

import sys
from libsbml import *




#
#
# SBML model operations
#
#

def check(value, message) :

	if value == None:
		raise SystemExit('LibSBML returned a null value trying to ' + message + '.')
	elif type(value) is int:
		if value == LIBSBML_OPERATION_SUCCESS:
			return
		else:
			err_msg = 'Error encountered trying to ' + message + '.' \
					+ 'LibSBML returned error code ' + str(value) + ': "' \
					+ OperationReturnValue_toString(value).strip() + '"'
		raise SystemExit(err_msg)
	else:
		return



def new_base_model(document) :
	"""
	Returns base model, to which compartments, species, reactions, events are added
	"""
 
	# base units 
	# (note that time is always in seconds, and then when units are defined, seconds are multiplied by 3600
	# to get an hour time unit)
	model = document.createModel()
	check(model,                              				'create model')
	check(model.setTimeUnits("second"),       				'set model-wide time units')
	check(model.setExtentUnits("mole"),       				'set model units of extent')
	check(model.setSubstanceUnits('mole'),    				'set model substance units')
	check(model.setVolumeUnits('litre'),      				'set model volume units')
	check(model.setLengthUnits('dimensionless'),    		'set model length units')
	check(model.setAreaUnits('dimensionless'),				'set model area units')	

	# s^-1 unit definition (uni molecular reactions)
	per_second = model.createUnitDefinition()
	check(per_second,                         				'create unit definition')
	check(per_second.setId('per_second'),     				'set unit definition id')
	unit = per_second.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_SECOND),     				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# h^-1 unit definition (uni molecular reactions)
	per_hour = model.createUnitDefinition()
	check(per_hour,                         				'create unit definition')
	check(per_hour.setId('per_hour'),     					'set unit definition id')
	unit = per_hour.createUnit()
	check(unit,                               				'create unit on per_hour')
	check(unit.setKind(UNIT_KIND_SECOND),     				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')

	# M^-1 s^-1 unit definition (bi-molecular reactions)
	per_molar_per_second = model.createUnitDefinition()
	check(per_molar_per_second,               				'create unit definition')
	check(per_molar_per_second.setId('per_molar_per_second'), 'set unit definition id')
	unit = per_molar_per_second.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_second.createUnit()
	check(unit,                              		 		'create unit on per_second')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_second.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')

	# M^-1 hr^-1 unit definition (bi-molecular reactions)	
	per_molar_per_hour = model.createUnitDefinition()
	check(per_molar_per_hour,               						'create unit definition')
	check(per_molar_per_hour.setId('per_molar_per_hour'), 'set unit definition id')
	unit = per_molar_per_hour.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_MOLE),       				'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_hour.createUnit()
	check(unit,                              		 		'create unit on per_second')
	check(unit.setKind(UNIT_KIND_LITRE),      				'set unit kind')
	check(unit.setExponent(1),                				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(1),              				'set unit multiplier')
	unit = per_molar_per_hour.createUnit()
	check(unit,                               				'create unit on per_second')
	check(unit.setKind(UNIT_KIND_SECOND),       			'set unit kind')
	check(unit.setExponent(-1),               				'set unit exponent')
	check(unit.setScale(0),                   				'set unit scale')
	check(unit.setMultiplier(3600),              			'set unit multiplier')

	return model



def new_parameter(model, parameter_name, parameter_value, parameter_unit) :

	k = model.createParameter()
	check(k,                                  				'create parameter k')
	check(k.setId(parameter_name),                       	'set parameter k id')
	check(k.setConstant(True),                				'set parameter k constant')
	check(k.setValue(parameter_value),                     	'set parameter k value')
	check(k.setUnits(parameter_unit),          				'set parameter k units')



def new_compartment(model, compartment_name, compartment_vol_litres) :

	c = model.createCompartment()
	check(c,                                 				'create compartment')
	check(c.setId(compartment_name),                		'set compartment id')
	check(c.setConstant(False),              				'set compartment variable size') 	# this must be False if the volume changes -- i.e. if an event changes the volume.
	check(c.setSize(compartment_vol_litres),        		'set compartment size')
	check(c.setSpatialDimensions(3),         				'set compartment dimensions')
	check(c.setUnits('litre'),               				'set compartment size units')



def new_species(model, species_name, initial_conc_molar, compartment_name, compartment_vol_litres) :

	species_moles = initial_conc_molar * compartment_vol_litres

	s = model.createSpecies()
	check(s,                                 				'create species s')
	check(s.setId(species_name),             				'set species s id')
	check(s.setCompartment(compartment_name),				'set species s compartment')
	check(s.setConstant(False),              				'set "constant" attribute on s')
	check(s.setInitialAmount(species_moles),    			'set initial amount for s')
	check(s.setSubstanceUnits('mole'),       				'set substance units for s')
	check(s.setBoundaryCondition(False),     				'set "boundaryCondition" on s')
	check(s.setHasOnlySubstanceUnits(False), 				'set "hasOnlySubstanceUnits" on s')



def new_unimolecular_reaction_1product(model, reaction_name, reactant_name, product_name, rate_constant_name, compartment_name) :
	"""
	A --> B
	"""

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant = r.createReactant()
	check(reactant,                       					'create reactant')
	check(reactant.setSpecies(reactant_name),      			'assign reactant species')
	check(reactant.setConstant(True),     					'set "constant" on reactant')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')
 
	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI
	
	formula = '%s * %s * %s' % (rate_constant_name, reactant_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')

	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')




def new_unimolecular_reaction_2product(model, reaction_name, reactant_name, product1_name, product2_name, rate_constant_name, compartment_name) :
	"""
	A --> B + C

	(can be used as reverse of bimolecular reaction below)
	"""

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant = r.createReactant()
	check(reactant,                       					'create reactant')
	check(reactant.setSpecies(reactant_name),      			'assign reactant species')
	check(reactant.setConstant(True),     					'set "constant" on reactant')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product1_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product1')
 
	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product2_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product2')

	# NOTE THAT MULTIPLICATION BY COMPARTMENT VOLUME is for some reason required, in the rate equation
	# in order to make the reaction rule work properly (map to kinetic rate k) in COPASI
	
	formula = '%s * %s * %s' % (rate_constant_name, reactant_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')
 
	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')



def new_bimolecular_reaction_1product(model, reaction_name, reactant1_name, reactant2_name, product_name, rate_constant_name, compartment_name) :
	"""
	A + B --> C
	"""	

	r = model.createReaction()
	check(r,                                 				'create reaction')
	check(r.setId(reaction_name),                     		'set reaction id')
	check(r.setReversible(False),            				'set reaction reversibility flag')
	check(r.setFast(False),                  				'set reaction "fast" attribute')

	reactant1 = r.createReactant()
	check(reactant1,                       					'create reactant')
	check(reactant1.setSpecies(reactant1_name),      		'assign reactant species')
	check(reactant1.setConstant(True),     					'set "constant" on reactant 1')

	reactant2 = r.createReactant()
	check(reactant2,                       					'create reactant')
	check(reactant2.setSpecies(reactant2_name),      		'assign reactant species')
	check(reactant2.setConstant(True),     					'set "constant" on reactant 2')

	product = r.createProduct()
	check(product,                       					'create product')
	check(product.setSpecies(product_name),      			'assign product species')
	check(product.setConstant(True),     					'set "constant" on product')

	formula = '%s * %s * %s * %s' % (rate_constant_name, reactant1_name, reactant2_name, compartment_name)
	math_ast = parseL3Formula(formula)
	check(math_ast,                           				'create AST for rate expression')

	kinetic_law = r.createKineticLaw()
	check(kinetic_law,                        				'create kinetic law')
	check(kinetic_law.setMath(math_ast),      				'set math on kinetic law')




#
#
# Create SBML model of PPC in-vitro model (full model)
#
#

def create_fullPPC_SBML() :

	try:
		document = SBMLDocument(3, 1)
	except ValueError:
		raise SystemExit("Could not create SBMLDocument object")

	# ====================================
	# 1. configure base model
	# ====================================

	vol0 = 1e-15 	# litres. Volume of a bacterium is approx 1 um^3, which is 1e-15 litres
					# (Not relevant for deterministic simulation)

	model = new_base_model(document)
	new_compartment(model, "ecoli", vol0)

	# ====================================
	# 2. add parameters
	# ====================================

	# Usage: 
	# new_parameter(model, parameter_name, parameter_value, parameter_unit)

	# 2.1 parameters table

	fi 			= [1e-6,1e-5,1e-4,1e-3,1e-2,1e-1,10]						# h^-1
	bi 			= 100 														# h^-1
	ki_Af 		= 1.72e10 													# M^-1 h^-1
	ki_Ab 		= [1,3,9,27,81,243,729]										# h^-1
	k_pf 		= 1															# h^-1
	iki_Bf		= [1.72e9 * k for k in [0.001,0.1,1,1,1,1,1]] 				# M^-1 h^-1
	iki_Bf__2 	= [2 * k for k in iki_Bf] 									# M^-1 h^-1		[twice above]
	iki_Bb		= [10,1,1,1,1,1,1]											# h^-1
	iki_Bb__2   = [2 * k for k in iki_Bb]  									# h^-1			[twice above]
	iki_Af 		= [1.72e9 * k for k in [1e-2,1e3,1e3,1e3,1e2,1e-3,1e-4]] 	# M^-1 h^-1
	iki_Af__2   = [2 * k for k in iki_Af]									# M^-1 h^-1  	[twice above]
	iki_Ab		= [10,1,1,1,1,1,10]											# h^-1
	iki_Ab__2	= [2 * k for k in iki_Ab]									# h^-1			[twice above]
	k_ps		= 0.025 													# h^-1
	k_dps 		= 0.4 														# h^-1
	ik_ps		= 0.025 													# h^-1
	ik_dps		= 0.4 														# h^-1

		# NOTE: the k_dps and ik_dps values of 100 h^-1 in PNAS SI are INCORRECT
		# as confirmed by email from Pieter Rein Ten Wolde

	# 2.2 add single value rate constants to model
	
	new_parameter(model, "bi", bi, "per_hour")
	new_parameter(model, "ki_Af", ki_Af, "per_molar_per_hour")
	new_parameter(model, "k_pf", k_pf, "per_hour")
	new_parameter(model, "k_ps", k_ps, "per_hour")
	new_parameter(model, "k_dps", k_dps, "per_hour")
	new_parameter(model, "ik_ps", ik_ps, "per_hour")
	new_parameter(model, "ik_dps", ik_dps, "per_hour")

	# 2.3 add rate constants with an i-dependent value, to the model

	for i in range(0,6+1) :
		# fi
		new_parameter(model, "f%d" % (i), fi[i], "per_hour")
		
		# ki_Ab
		new_parameter(model, "k%d_Ab" % (i), ki_Ab[i], "per_hour")
		
		# iki_Bf
		new_parameter(model, "ik%d_Bf" % (i), iki_Bf[i], "per_molar_per_hour")
		new_parameter(model, "ik%d_Bf__2" % (i), iki_Bf__2[i], "per_molar_per_hour")
		# iki_Bb
		new_parameter(model, "ik%d_Bb" % (i), iki_Bb[i], "per_hour")
		new_parameter(model, "ik%d_Bb__2" % (i), iki_Bb__2[i], "per_hour")
		
		# iki_Af
		new_parameter(model, "ik%d_Af" % (i), iki_Af[i], "per_molar_per_hour")
		new_parameter(model, "ik%d_Af__2" % (i), iki_Af__2[i], "per_molar_per_hour")
		# iki_Ab
		new_parameter(model, "ik%d_Ab" % (i), iki_Ab[i], "per_hour")
		new_parameter(model, "ik%d_Ab__2" % (i), iki_Ab__2[i], "per_hour")


	# ====================================
	# 3. add species
	# ====================================

	# Usage:
	# new_species(model, species_name, initial_conc_molar, compartment_name, compartment_vol_litres)

	# specify the species that have an initial conc
	initial_concs = {}
	initial_concs["A"] = 0.58e-6 		# M
	initial_concs["B"] = 1.75e-6 		# M
	initial_concs["C0"] = 0.58e-6 		# M

	# A species (1)
	species = "A"
	conc = initial_concs.get(species, 0)
	new_species(model, species, conc, "ecoli", vol0)	

	# B species (1)
	species = "B"
	conc = initial_concs.get(species, 0)
	new_species(model, species, conc, "ecoli", vol0)	

	# other species that depend on phosphorylation state i
	# there are 7 of each of these species
	for i in range(0,6+1) :

		# Ci
		species = "C%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# iCi
		species = "iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# ACi
		species = "AC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# B1iCi
		species = "B1iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# B2iCi
		species = "B2iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# A1B1iCi
		species = "A1B1iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# A1B2iCi
		species = "A1B2iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	

		# A2B2iCi
		species = "A2B2iC%d" % (i)
		conc = initial_concs.get(species, 0)
		new_species(model, species, conc, "ecoli", vol0)	


	# ====================================
	# 4. add reactions
	# ====================================

	# --- reaction 1
	for i in range(0,6+1) :
	
		# forwards
		# Ci --> iCi
		new_unimolecular_reaction_1product(model, "R1_%df" % (i), \
			"C%d" % (i), \
			"iC%d" % (i), \
			"f%d" % (i), "ecoli")

		# backwards
		# iCi --> Ci
		new_unimolecular_reaction_1product(model, "R1_%db" % (i), \
			"iC%d" % (i), \
			"C%d" % (i), \
			"bi", "ecoli")

	# --- reaction 2
	for i in range(0,6+1) :

		# forwards
		# Ci + A --> ACi
		new_bimolecular_reaction_1product(model, "R2_%df" % (i), \
			"C%d" % (i), "A", \
			"AC%d" % (i), \
			"ki_Af", "ecoli")

		# backwards
		# ACi --> Ci + A
		new_unimolecular_reaction_2product(model, "R2_%db" % (i), \
			"AC%d" % (i), \
			"C%d" % (i), "A", \
			"k%d_Ab" % (i), "ecoli")

		if i < 6 :
			# irreversible
			# ACi --> Ci+1 +  A
			new_unimolecular_reaction_2product(model, "R2_%dirrev" % (i), \
				"AC%d" % (i), \
				"C%d" % (i+1), "A", \
				"k_pf", "ecoli")

	# --- reaction 3
	for i in range(0,6+1) :

		# forwards
		# iCi + B --> B1iCi
		new_bimolecular_reaction_1product(model, "R3_%df" % (i), \
			"iC%d" % (i), "B", \
			"B1iC%d" % (i), \
			"ik%d_Bf__2" % (i), "ecoli")

		# backwards
		# B1iCi --> iCi + B
		new_unimolecular_reaction_2product(model, "R3_%db" % (i), \
			"B1iC%d" % (i), \
			"iC%d" % (i), "B", \
			"ik%d_Bb" % (i), "ecoli")

	# --- reaction 4
	for i in range(0,6+1) :

		# forwards
		# B1iCi + B --> B2iCi		
		new_bimolecular_reaction_1product(model, "R4_%df" % (i), \
			"B1iC%d" % (i), "B", \
			"B2iC%d" % (i), \
			"ik%d_Bf" % (i), "ecoli")

		# backwards
		# B2iCi --> B1iCi + B
		new_unimolecular_reaction_2product(model, "R4_%db" % (i), \
			"B2iC%d" % (i), \
			"B1iC%d" % (i), "B", \
			"ik%d_Bb__2" % (i), "ecoli")

	# --- reaction 5
	for i in range(0,6+1) :

		# forwards
		# B1iCi + A --> A1B1iCi
		new_bimolecular_reaction_1product(model, "R5_%df" % (i), \
			"B1iC%d" % (i), "A", \
			"A1B1iC%d" % (i), \
			"ik%d_Af" % (i), "ecoli")

		
		# backwards
		# A1B1iCi --> B1iCi + A
		new_unimolecular_reaction_2product(model, "R5_%db" % (i), \
			"A1B1iC%d" % (i), \
			"B1iC%d" % (i), "A", \
			"ik%d_Ab" % (i), "ecoli")

	# --- reaction 6
	for i in range(0,6+1) :
		
		# forwards
		# B2iCi + A --> A1B2iCi
		new_bimolecular_reaction_1product(model, "R6_%df" % (i), \
			"B2iC%d" % (i), "A", \
			"A1B2iC%d" % (i), \
			"ik%d_Af__2" % (i), "ecoli")

		# backwards
		# A1B2iCi --> B2iCi + A
		new_unimolecular_reaction_2product(model, "R6_%db" % (i), \
			"A1B2iC%d" % (i), \
			"B2iC%d" % (i), "A", \
			"ik%d_Ab" % (i), "ecoli")		

	# --- reaction 7
	for i in range(0,6+1) :

		# forwards
		# A1B2iCi + A --> A2B2iCi	
		new_bimolecular_reaction_1product(model, "R7_%df" % (i), \
			"A1B2iC%d" % (i), "A", \
			"A2B2iC%d" % (i), \
			"ik%d_Af" % (i), "ecoli")

		# backwards
		# A2B2iCi --> A1B2iCi + A
		new_unimolecular_reaction_2product(model, "R7_%db" % (i), \
			"A2B2iC%d" % (i), \
			"A1B2iC%d" % (i), "A", \
			"ik%d_Ab__2" % (i), "ecoli")			


	# simple phosphorylation and dephosphorylation reactions

	# --- reaction 8
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# Ci --> Ci+1
			new_unimolecular_reaction_1product(model, "R8_%df" % (i), \
				"C%d" % (i), \
				"C%d" % (i+1), \
				"k_ps", "ecoli")

			# backwards
			# Ci+1 --> Ci
			new_unimolecular_reaction_1product(model, "R8_%db" % (i), \
				"C%d" % (i+1), \
				"C%d" % (i), \
				"k_dps", "ecoli")			

	# --- reaction 9
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# iCi --> iCi+1
			new_unimolecular_reaction_1product(model, "R9_%df" % (i), \
				"iC%d" % (i), \
				"iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# iCi+1 --> iCi
			new_unimolecular_reaction_1product(model, "R9_%db" % (i), \
				"iC%d" % (i+1), \
				"iC%d" % (i), \
				"ik_dps", "ecoli")	

	# --- reaction 10
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# B1iCi --> B1iCi+1
			new_unimolecular_reaction_1product(model, "R10_%df" % (i), \
				"B1iC%d" % (i), \
				"B1iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# B1iCi+1 --> B1iCi
			new_unimolecular_reaction_1product(model, "R10_%db" % (i), \
				"B1iC%d" % (i+1), \
				"B1iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 11
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# B2iCi --> B2iCi+1
			new_unimolecular_reaction_1product(model, "R11_%df" % (i), \
				"B2iC%d" % (i), \
				"B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# B2iCi+1 --> B2iCi
			new_unimolecular_reaction_1product(model, "R11_%db" % (i), \
				"B2iC%d" % (i+1), \
				"B2iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 12
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A1B1iCi --> A1B1iCi+1
			new_unimolecular_reaction_1product(model, "R12_%df" % (i), \
				"A1B1iC%d" % (i), \
				"A1B1iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A1B1iCi+1 --> A1B1iCi
			new_unimolecular_reaction_1product(model, "R12_%db" % (i), \
				"A1B1iC%d" % (i+1), \
				"A1B1iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 13
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A1B2iCi --> A1B2iCi+1
			new_unimolecular_reaction_1product(model, "R13_%df" % (i), \
				"A1B2iC%d" % (i), \
				"A1B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A1B2iCi+1 --> A1B2iCi
			new_unimolecular_reaction_1product(model, "R13_%db" % (i), \
				"A1B2iC%d" % (i+1), \
				"A1B2iC%d" % (i), \
				"ik_dps", "ecoli")

	# --- reaction 14
	for i in range(0,6+1) :

		if i < 6 :
			# forwards
			# A2B2iCi --> A2B2iCi+1
			new_unimolecular_reaction_1product(model, "R14_%df" % (i), \
				"A2B2iC%d" % (i), \
				"A2B2iC%d" % (i+1), \
				"ik_ps", "ecoli")

			# backwards
			# A2B2iCi+1 --> A2B2iCi
			new_unimolecular_reaction_1product(model, "R14_%db" % (i), \
				"A2B2iC%d" % (i+1), \
				"A2B2iC%d" % (i), \
				"ik_dps", "ecoli")



	# ====================================
	# 5. export SBML
	# ====================================	
	f = open("fullPPC.xml", "w")
	f.write(writeSBMLToString(document))
	f.close()





if __name__ == "__main__" :

	create_fullPPC_SBML() 

